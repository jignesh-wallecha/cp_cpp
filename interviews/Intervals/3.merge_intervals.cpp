#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

/**
* Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.

Example 1:

Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
 

Constraints:

1 <= intervals.length <= 104
intervals[i].length == 2
0 <= starti <= endi <= 104


Logic:

|----|  |----|

Their is a gap between this interval so we'll not merge the intervals. If intervals are like this, 
|-----|
	|-----|

Then we're going to merge it. So we'll take the start of 1st interval & end of 2 interval, (as interval[start][last] < interval[start+1][last+1], therefore we will take bigger one) and then merge it.

Conditions to figure out:
1. Take vector
2. add first interval directly into vector
3. If second interval is not overlapping to first then add second interval directly into vector

4. Agar if second interval is overlapping with first then merge the first & second interval and take the start of first interval & end of second interval as it is greater then first's end. And merge them and add to vector.

(As we don't need to look the start as it is sorted, we have to only focus on end of bigger interval, end woo lehna hai joh max ho).

**/

ostream& operator<<(ostream& outs, vector<int>& v) {
	return outs << v[0] << "  "  << v[1];
}

vector<vector<int>> merge(vector<vector<int>>& intervals) {
        
    vector<vector<int>> nonOverlapping;
      
    sort(intervals.begin(), intervals.end());
    /**
    * two case to add interval if vector is empty or their is no overlapping between two intervals 
    **/

    for(const auto& interval : intervals) {
        if(nonOverlapping.empty() || interval[0] > nonOverlapping.back()[1]) {
            nonOverlapping.emplace_back(interval);
        } else {
        	//merge joh nonOverlapping mai last rahega uske end ko change krna hai
        	nonOverlapping.back()[1] = std::max(nonOverlapping.back()[1], interval[1]);
        }

    }
        
  	return nonOverlapping;
}

void print(vector<vector<int>>& intervals) {
	for(const auto& interval : intervals) {
		for(const auto& e : interval) {
			cout << e <<"  ";
		}
		cout << '\n';
	}
}

int main() {

	vector<vector<int>> intervals = {
		{1, 3},
		{2, 6},
		{8, 10},
		{15, 18}
	};

	vector<vector<int>> res = merge(intervals);
	print(res);
}

