#include <vector>
#include <iostream>
using namespace std;

/**
* Insert Interval:

Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

You may assume that the intervals were initially sorted according to their start times.
Example 1:
Input: intervals = [[1,3],[6,9]], newInterval = [2,5]
Output: [[1,5],[6,9]]

Example 2:
Input: intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]], newInterval = [4,8]
Output: [[1,2],[3,10],[12,16]]
Explanation: Because the new interval [4,8] overlaps with [3,5],[6,7],[8,10].

Example 3:
Input: intervals = [], newInterval = [5,7]
Output: [[5,7]]

**/

vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval)
{
		vector<vector<int>> nonOverlapping;
		//if intervals vector is empty then direct push newInterval in result vector
		if(intervals.empty()) {
			nonOverlapping.emplace_back(newInterval);
			return nonOverlapping;
		}
		int i = 0, sz_ = intervals.size();

		while (i < sz_ and intervals[i][1] < newInterval[0]) {
			nonOverlapping.emplace_back(intervals[i]);
			++i;
		}
		cout << "i: " << i << endl;
		//newInterval overlapping with intervals 
		while (i < sz_ and intervals[i][0] <= newInterval[1]) {
			newInterval[0] = std::min(intervals[i][0], newInterval[0]);
			newInterval[1] = std::max(intervals[i][1], newInterval[1]);
			//cout << newInterval[0] << ", " << newInterval[1] << endl;
			++i;
		}
	//	cout << "newInterval: " << newInterval[0] << " " << newInterval[1] << endl;
		nonOverlapping.emplace_back(newInterval);
		while (i < sz_) {
			nonOverlapping.emplace_back(intervals[i++]);
		}
		return nonOverlapping;
}

int main() 
{
	vector<vector<int>> intervals = {
		{1, 2},
		{3, 5},
		{6, 7},
		{8, 10},
		{12, 16}
	};

	vector<int> newInterval = {4, 8};

	vector<vector<int>> result = insert(intervals, newInterval);
	for (const auto& v : result) {
		for (auto& e : v) {
			cout << e <<" ";
		}
		cout <<"\n";
	}
	cout << "\n";

	
	return 0;
}
