#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

/**
* Meeting Rooms:
* Given an array of meeting time intervals where intervals[i] = [starti, endi],
  determine if a person could attend all meetings.

* Ex1:
I/P: intervals = [[0, 30], [5, 10], [15, 20]]
O/P: false

* Ex2:
I/P: intervals = [[7, 10], [2, 4]]
O/P: true

Logic:

Sort this list according to first number. 

 [[0, 30], [5, 10], [15, 20]]

after sorting, check whether interval[0][1] is overlapping to interval[1][0] i.e 30 endtime is overlapping to starttime of 5. 30 > 5 i.e means 30 ka end time badme hai aur 5 start time kam hai. This means that i can sit/attend only meeting one from both. (yeh dono mai se mai sirf ekk hi attend krr pahunga).

Toh we have to tell ki mai sarre mettings attend krr pahunga ki nhi, kitne attend krr pahunga woo consideration mai nhi hai. So above ex, returns false becuase it is overlapping. (30 is overlapping to 5).

2. [[7, 10], [2, 4]]
 
sort - [[2, 4], [7, 10]]

timming is not overlapping i.e means i can attend all meetings. 

**/

bool canAttendMeetings(vector<vector<int>>& intervals) {

	//empty hai toh i can attend all
	if(intervals.empty()) return true;

	sort(intervals.begin(), intervals.end()); 
	//by defaultly yeh first value ke sabh se sort krta hai, pair rahenge n toh bhi yeh first value ke sabh se sort krta hai, aur agar first value match hota hai n toh woo second value check krta hai, otherwise yeh second value nhi dekhta hai.

	//constraints mai range diya hai n toh that range is good, then we have to use size_t type in c++ otherwise use int

/*	int end = intervals[0][1];
	for(size_t i = 1; i<intervals.size(); ++i) {
		if(intervals[i][0] < end) {
			return false;
		}
		end = std::max(end, intervals[i][1]);
	}
*/
	for(size_t i = 1; i<intervals.size(); ++i) {
		if(intervals[i][0] < intervals[i-1][1]) {
			return false;
		}
	}
	return true;
}

int main() {

	cout << std::boolalpha;

	vector<vector<int>> intervals = {
		{7, 10},
		{2, 4},
	};

	bool res = canAttendMeetings(intervals);
	cout << res << endl;
	//true

	intervals = {
		{0, 30},
		{5, 10},
		{15, 20}
	};

	res = canAttendMeetings(intervals);
	cout << res << endl;
	//false
}