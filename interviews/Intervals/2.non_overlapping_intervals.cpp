#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

/**
* Given a collection of intervals, find the minimum number of intervals you need to remove to make the rest of the intervals non-overlapping.

Example 1:

Input: [[1,2],[2,3],[3,4],[1,3]]
Output: 1
Explanation: [1,3] can be removed and the rest of intervals are non-overlapping.
Example 2:

Input: [[1,2],[1,2],[1,2]]
Output: 2
Explanation: You need to remove two [1,2] to make the rest of intervals non-overlapping.
Example 3:

Input: [[1,2],[2,3]]
Output: 0
Explanation: You don't need to remove any of the intervals since they're already non-overlapping.

Note:

You may assume the interval's end point is always bigger than its start point.
Intervals like [1,2] and [2,3] have borders "touching" but they don't overlap each other.

LOGIC:

[1, 100], [2, 5], [5, 10], [20, 45]

if we keep [1, 100], then we have to remove rest 3. As they are overlapping with [1, 100]. 

2nd soln:
[1, 100], [2, 5], [5, 10], [20, 45]

remove [1, 100], as rest are not overlapping as question says to remove the minimum interval.

Both above soln are correct but questions says to remove the minimum interval.

How to do this ?


**/