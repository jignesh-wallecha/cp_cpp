#include<iostream>
#include<vector>
using namespace std;

/**
* Interval List Intersections:
ou are given two lists of closed intervals, firstList and secondList, where firstList[i] = [starti, endi] and secondList[j] = [startj, endj]. Each list of intervals is pairwise disjoint and in sorted order.

Return the intersection of these two interval lists.

A closed interval [a, b] (with a < b) denotes the set of real numbers x with a <= x <= b.

The intersection of two closed intervals is a set of real numbers that are either empty or represented as a closed interval. For example, the intersection of [1, 3] and [2, 4] is [2, 3].

Example 1:
Input: firstList = [[0,2],[5,10],[13,23],[24,25]], 
	   secondList = [[1,5],[8,12],[15,24],[25,26]]
Output: [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]


Logic:
We need to get intersection of two pair of vector and put that intersection vector in result vector.

for ex:
[0, 2] [1, 5]

0 	1 	2 	4	5
|-------|
    |-----------|

So intersection is [1, 2]

To get this intersection common point , we have to take max of both lists first value & min of both lists second value.

It's like merge sort, take two array, and run until both array has something. Agar dono mai se konsa array bhi ghatam hogaya, then we have to leave rest of it, we do not have to go further. Agar dono mai kuch rahega toh ham intersection dundte jayenge. 

**/

/**
* Used data structure of merge sort (merging)
* Complexity:
* Time: O(n + m) n -> length of first list & m -> len of second list , this 		is max complexity.
 
**/
vector<vector<int>> intervalIntersection(vector<vector<int>>& firstList, vector<vector<int>>& secondList) {
        
    vector<vector<int>> res;
        
    int i, j;
    i = 0, j = 0;
    while(i < firstList.size() && j < secondList.size()) {
        int low = std::max(firstList[i][0], secondList[j][0]);
        int high = std::min(firstList[i][1], secondList[j][1]);
            
        if(low <= high) {
            res.push_back({low, high});
        }
        //here ham wohi pointer ko shift karenge jis list ka end small rahega.
        firstList[i][1] < secondList[j][1]  ? ++i : ++j;
    }
    return res;
}

int main() {

	vector<vector<int>> firstList = {
		{0, 2},
		{5, 10},
		{13, 23},
		{24, 25}
	};

	vector<vector<int>> secondList = {
		{1, 5},
		{8, 12},
		{15, 24},
		{25, 26}
	};

	vector<vector<int>> res = intervalIntersection(firstList, secondList);
	for(auto& arr : res) {
		for(auto& e : arr) {
			cout << e <<"  ";
		}
		cout << '\n';
	}
	
}