#include<iostream>
#include<vector>
#include<algorithm>
#include<queue>
#include<functional> //for greater 
using namespace std;

/**
* Meeting rooms 2:
* Given an array of meeting time intervals where intervals[i] = [starti, endi], return the minimum num of conference rooms required.

Ex1: 
I/p: intervals = [[0, 30], [5, 10], [15, 20]]
O/P: 2

Ex2:
I/p: intervals = [[7, 10], [2, 4]]
O/P: 1


LOGIC:
Suppose, we have meeting rooms infinte:

[[0, 30], [5, 10], [7,8], [15, 20]]

We do not need to concern ki kabh meeting start ho raha hai, we have to see when the meeting is ending.

So, [0, 30] is assigned to room num 1. 
Now, [5, 10] this will be assigned to room 2 becuase till now [0, 30] has not finished, As it is starting at 5 and room 1 end time is 30 so it is still their in room 1.So, we have to give [5, 10] to room 2.


rooms:
1. [] - [0, 30]
2. [] - [5, 10] 
3. []


Now, point is from [0, 30] & [5, 10] which room is getting empty early ?
So, [5, 10] will be finished first 



in_built priority_queue class in queue container / header is by default max-heap. (Descending Priority Queue). As it is enable std::less function object/functors, it internally keeps in ascending order but tops point to last element of ascending priority queue, so top element is returned so it returns largest element from queue. So to enable min-heap, we have to enable std::greater function object which will keep queue in descending priority queue and will returning the smallest element from queue. 
**/

int minMeetingRoomsRequired(vector<vector<int>>& intervals) {
	if(intervals.empty()) return 0; 

	//min heaps or ascending priority queue
	priority_queue<int, vector<int>, greater<int>> pq;

	sort(intervals.begin(), intervals.end());

	pq.emplace(intervals[0][1]);

	for(int i = 1; i<intervals.size(); ++i) {

		if(intervals[i][0] >= pq.top()) {
			//in this, interval start is greater/equal to end time of room, so their is no need of second room, so we will assign that same room to new interval pair.
			pq.pop();
		} 
		pq.emplace(intervals[i][1]);
	}

	return pq.size();
}

int main() {

	vector<vector<int>> intervals  = {
		{0, 30},
		{5, 10},
		{7, 8},
		{15, 20}
	};


	int res = minMeetingRoomsRequired(intervals);
	cout << res << endl;
	// 3

	intervals = {
		{7, 10},
		{2, 4}
	};

	res = minMeetingRoomsRequired(intervals);
	cout << res << endl;
	// 1
}