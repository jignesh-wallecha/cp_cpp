#include <iostream>
#include <vector>
using namespace std;
	
vector<int> power(15);

int findPower (int num) {

	cout << "num: " << num << endl;
	if (num == 1) {
		return 0;
	}
	power[num] = 1 + (num % 2 == 0 ? findPower(num/2) : findPower(num * 3 + 1));
	cout << num  << " : "<< power[num] << endl;
	return power[num];
}

int fibo (int n ) {
	cout << "n: " << n << endl;
	if (n == 0 || n == 1) {
		return 1;
	}  
	return fibo (n - 1);
}

int main() {

	int num = 12;
//		cout << "res: " << findPower(num) << endl;

	cout << "fibo: " << fibo(3);
	return 0;

}