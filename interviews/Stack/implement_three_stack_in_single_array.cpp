#include<iostream>
#include<exception>

using namespace std;

/**
* Design to Use Single Array implement Three Stacks:
*
* Array is divided into two parts 
* 1. Fixed Division
* 2. Flexible Division

* 1. Fixed Division of Array for Stacks i.e each stack will have equal amount 
* 	 of elements in it. 
	
*	So to do, so we'll keep stack_top ptr of each stack in array through,
*	division of arraySize/noOfStacks.

* We can take input in two arrays, i.e we can take array's size or we'll take
* Stack size.

* ArraySize = SizeOfStack * noOfStack = SizeofArr

* SizeOfStack = ArraySize/noOfStack

* Program Design:

* We'll take Stack size as i/p from user so that array size will be calculated.
* Keep noOfStack by default 3 or can also take from user.
* So arr = sizeOfStack * noOfStack

* Now, we'll keep track for much elements are inserted in each stack.
* So we'll keep counter track of elements for every stack.

* So we'll make counters array in which their are noOfStacks in it. and count
* of every stack, i.e how much elements are inserted in partiuclar stack.
* So their are n counters.

* if counters[n] == sizeOfStack 
	then that stack size is full 
	
* Stack Nums logic:

* 0---------3--------6--------|
  -----> 	 ------>  ------>
1.  stack1	 stack2	  stack3 
  			 or
2.  stack0 	 stack1	  stack2

There're two approaches that we can use either we can take stackNums as 
(1, 2, 3) or (0, 1, 2).

(0, 1, 2): for this it easy to make program 
(1, 2, 3): for this i'will tell interviewer that as this data structure
		   will be used by programmer only and it will not be used by end user
		   as the programmer we're comfortable using (0, 1, 2) as the counters.
So we're taking numberings as (0, 1, 2) for stackNums.

WE HAVE TO JUSTIFY EVERYTHNG WHILE DOING IT TO INTERVIEWER.

So now have to check that for given StackNum in counters arrays that stackNums
counter has reached to sizeOfStack. Agar it if full then we'll return it.

-> indexOfTop(int stackNum) logic:

Now we'need to get the top element of stack.

StackTop:

Arr = 0----------6---------12---------|

(0, 6, 12) from these indexes we're going to start for stack.

The array is blank, so counters of stackNum is also zero, as nothing is inserted.
If it is inserted in arr, we'll increment the counter for that stackNum.

To insert element in stack their is a logic, As we insert element into that 
particular stack, then that element is stackTop of that stack, so to insert new
upcomming element we need to insert it after that stackTop+1 pos, if it is 
not equal to sizeOfStack for that stack.

Arr = 0	 1--------6---------12---------|
	  10 20
	  |----------|----------|---------|	

If we're inserting in stack 0 then counter of that stackNum = 0 is incremented
counter0 = 1.

So now to insert into 1st idx in arr, it is matching with counter0 value
i.e (1). 
	
But if we insert into stackNum = 1, then it is giving problem.

So, we'll derive something from this by not keeping extra variables of 
stack_top for every stackNum.

* If we have stackSize & a stackNum then we can get Base address from it.

* StackSize * StackNum = Base Address.

* Plus we want offset that will tell at which next position we can insert 
next element.

* So for this, offset will be derived from counters array of StackNum which has 
count of every stackNum. 

* formula = StackSize * StackNum = Base + offset.

* So for ex, StackNum = 0, if we want to insert val/ele in this stack then,
 the calculations are, Assume StackSize is of 6.
	Base = 6 * 0 
	Base = 0.

 As, in stackNum = 0 stack their is no element in it yet. we'll increment count
 of stack0 which will be 1 now, and now offset = 1.

	indexOfTop = Base + offset 

So, now next Element will be inserted (+1), i.e indexOfTop + 1.



**/

struct StackUnderFlowException : public exception {
	
	string msg;
public:
	StackUnderFlowException(const string& _msg) : msg(_msg) {}

	const char* what() const noexcept {
		return msg.c_str();
	} 
};

struct StackOverFlowException : public exception {
	
	string msg;
public:
	StackOverFlowException(const string& _msg) : msg(_msg) {}

	const char* what() const noexcept {
		return msg.c_str();
	} 
};


class FixedMultiStack {

private:
	int noOfStacks;
	int sizeOfStack; //sizeof each Stack, so arraySize will be calculated.
	int *values;
	int *counters;

	void init(int noOfStacks, int sizeOfStack) {
		this->noOfStacks = noOfStacks;
		this->sizeOfStack = sizeOfStack;
		this->values = new int[sizeOfStack * noOfStacks];
		this->counters = new int[noOfStacks];
	}

	bool isFull(int stackNum) {
		return counters[stackNum] == sizeOfStack;
	}

	bool isEmpty(int stackNum) {
		return counters[stackNum] == 0;
	}

	int indexOfTop(int stackNum) {
		int base = sizeOfStack * stackNum;
		int offset = counters[stackNum];
		return base + offset - 1;
	}

public:

	FixedMultiStack(int sizeOfStack) {
		init(3, sizeOfStack);
	}

	FixedMultiStack(int noOfStacks, int sizeOfStack) {
		init(noOfStacks, sizeOfStack);
	}

	int peek(int stackNum) {
		if(isEmpty(stackNum)) {
			throw StackUnderFlowException("Stack is Underflow");
		}
		return values[indexOfTop(stackNum)];
	}

	void push(int stackNum, int value) {
		if(isFull(stackNum)) {
			throw StackOverFlowException("Stack is Overflow");
		}
		//actual insertion in arrays
	/*	values[indexOfTop(stackNum)+1] = value;
		counters[stackNum]++;
	*/
		counters[stackNum]++;
		values[indexOfTop(stackNum)] = value;
	}

	int pop(int stackNum) {
		if(isEmpty(stackNum)) {
			throw StackUnderFlowException("Stack is Underflow");
		}
		int top = indexOfTop(stackNum);
		int value = values[top];
		values[top] = 0;
		counters[stackNum]--;
 		return value;
	}

	void display(int stackNum) {
		if(isEmpty(stackNum)) {
			throw StackUnderFlowException("Stack is Underflow");
		}
		for(int i=indexOfTop(stackNum); i>=0; --i) {
			cout << values[i] <<"  ";
		}
		cout << '\n';
	}	

	void countersDisp() {
		int n = sizeof(counters)/sizeof(int);
		for(int i=0; i<n; ++i) {
			cout << counters[i] <<"  "; 
		}
		cout << '\n';
	}
};

int main() {

	FixedMultiStack fixedMultiStack(4);
	fixedMultiStack.push(1, 10);
	fixedMultiStack.push(1, 20);
	fixedMultiStack.push(1, 30);
	fixedMultiStack.push(1, 40);
	fixedMultiStack.display(1);
	fixedMultiStack.pop(1);
	fixedMultiStack.display(1);

	fixedMultiStack.countersDisp();
	return 0;
}

