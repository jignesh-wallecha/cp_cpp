#include<iostream>
#include<exception>
#include<vector>

using namespace std;

template <typename T>
struct Stack : vector<T> {

public:
    Stack() = default;

    int sizeOfStack() {
        return this->size();
    }

    bool isEmpty() {
        return this->size() == 0;
    }
    
    T topElement() {
        if(isEmpty()) throw std::runtime_error("Stack is underflow");
        return (*this)[this->size()-1];
    }
    void push(T x) {
        this->emplace_back(x);
    }  

    void clearAll() {
        this->clear();
    }

    void pop() {
        this->pop_back();
    }

    void display() {
        if(isEmpty()) return;
        cout << "Stack elements are:\n";
        for(auto &t : *this) {
            cout << t <<"  "; 
        }
        cout << '\n';
    }

};

int main()
{
    Stack<int> s;
    s.push(1);
    s.push(3);
    s.push(4);
    s.push(5);   
    s.display();

    s.pop();
    s.display();
    return 0;
}
