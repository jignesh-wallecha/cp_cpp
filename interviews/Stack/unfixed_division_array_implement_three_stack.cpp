#include<iostream>
#include<vector>

using namespace std;

/**
* Unfixed Division of Array for Stacks:

**/

struct StackUnderFlowException : public exception {
	
	string msg;
public:
	StackUnderFlowException(const string& _msg) : msg(_msg) {}

	const char* what() const noexcept {
		return msg.c_str();
	} 
};

struct StackOverFlowException : public exception {
	
	string msg;
public:
	StackOverFlowException(const string& _msg) : msg(_msg) {}

	const char* what() const noexcept {
		return msg.c_str();
	} 
};

class UnFixedMultiStack {

private:
	int noOfStacks;
	int* start;
	vector<int> values;
	int* counters;
	int sizeOfStack;

	void init(int noOfStacks, int sizeOfStack, vector<int> values_) {
		this->noOfStacks = noOfStacks;
		this->sizeOfStack = sizeOfStack;
		this->start = new int[noOfStacks];
		this->values = values_;
		this->counters = new int[noOfStacks];
	}

	bool isEmpty(int stackNum) {
		return counters[stackNum] == 0;
	}

	bool isFull(int stackNum) {
		return counters[stackNum] == sizeOfStack;
	}

	int indexOfTop(int stackNum) {
		int base = sizeOfStack * stackNum;
		int offset = counters[stackNum];
		return base + offset - 1;
	}

public:

	UnFixedMultiStack(int sizeOfStack, vector<int> values_) {
		init(3, sizeOfStack, values_);
	}

	UnFixedMultiStack(int noOfStacks, int sizeOfStack, vector<int>& values_) {
		init(noOfStacks, sizeOfStack, values_);
	}

	void push(int stackNum, int value) {
		if(isFull(stackNum)) {
			throw StackOverFlowException("Stack is Overflow");
		}
		counters[stackNum]++;
		int idxOfTop = indexOfTop(stackNum);
		values[idxOfTop] = value;
		start[stackNum] = idxOfTop;
	}

	void displayValuesArr() {
		int n = values.size();
		cout << "n: " << n << endl;
	}
};	

int main() {
	vector<int> values;
	UnFixedMultiStack unfixedMultiStack(4, values);
	unfixedMultiStack.push(0, 10);
	unfixedMultiStack.push(0, 20);
	unfixedMultiStack.push(0, 30);

	unfixedMultiStack.displayValuesArr();
}