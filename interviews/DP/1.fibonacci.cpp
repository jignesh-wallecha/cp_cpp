#include <iostream>
using namespace std;

/**
 * optimal substructure way (dnc) 
 * time complexity: O(2^n)
 * space: O(n) 
*/

int fibonacci(int n) {

    //base condn if n == 0 return 0 or n == 1 return 1;
    if (n == 0 || n == 1) {
        return n;
    }

    return fibonacci(n-1) + fibonacci(n-2); 
    //it is binary recursion(same function is called twice) and tail recursion tail because, it will do recursion at end not at start.
}

/**
 * DP (Top-down)
 * time: O(n) & Space: O(2n): one O(n) of stack and one O(n) of maintaining 
 * array/table in memory (memoization), so total space: O(n) 
*/ 

int fibonacciTopDownDP(int n, int dp[]) {

    //base condn
    if (n == 0 || n == 1) {
        dp[n] = n;
        return n;
    }

    //lookup condition (agar table mai fibo(n) hai toh recursive call dekhe compute mat krr sidha return from array only, because it is already pre-computed for that value, so this will get fibo(num) in O(1)).

    if (dp[n] != 0) {
        //means array is not empty
        return dp[n];
    }

    //otherwise we're computing first time so, compute and maintain sub-solution to sub-problem in array/memory.

    dp[n] = fibonacciTopDownDP(n-1, dp) + fibonacciTopDownDP(n-2, dp);
    return dp[n];
}

/**
 * DP (Bottom-Up): through tabulation, table is maintain and it contains soln of subproblem which will give solution of main problem 
 * 
 * time: O(n) & space: O(n)
*/ 
int fibonacciBottomUpDP(int n) {

    int dp[n+1] = {0};
    
    //base condition
    dp[0] = 0;
    dp[1] = 1;

    for (int i = 2; i <= n; ++i) {
        dp[i] = dp[i-1] + dp[i-2];
    }

    return dp[n];
}

int main() 
{
    int dp[100] = {0};
    int res = fibonacciTopDownDP(7, dp);
    cout << res << endl;

    res = fibonacciBottomUpDP(6);
    cout << res << endl;

    return 0;
}



