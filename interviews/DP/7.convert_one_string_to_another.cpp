#include <iostream>
using namespace std;

int minOperationToMakeStringSame(string s1, string s2, int index1, int index2) 
{
    //base condn 
    if (index1 == s1.length()) {
        return s2.length() - index2;
    }
    if (index2 == s2.length()) {
        return s1.length() - index1;
    }

    //if both character same then increment both the ptrs in string
    if (s1.at(index1) == s2.at(index2)) {
        return minOperationToMakeStringSame(s1, s2, index1+1, index2+1);
    }

    int cost1 = 1 + minOperationToMakeStringSame(s1, s2, index1+1, index2); //insert
    int cost2 = 1 + minOperationToMakeStringSame(s1, s2, index1, index2+1); //delete
    int cost3 = 1 + minOperationToMakeStringSame(s1, s2, index1+1, index2+1); //replace

    return min(min(cost1, cost2), cost3);
}

int main() 
{
    string s1 = "table";
    string s2 = "tbres";
    int res = minOperationToMakeStringSame(s1, s2, 0, 0);
    cout << res << endl;
    return 0;
}