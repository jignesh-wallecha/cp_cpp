#include <iostream>
using namespace std;

int waysToGetN(int n, int dp[]) 
{
    if (n == 0) return 0;
    if (n == 1) return 1; // {(1)}
    if (n == 2) return 1; // {(1,1)}
    if (n == 3) return 2; // {(1,1,1), (3)} 

    if (dp[n] != 0) {
        return dp[n];
    }

    int subtract1 = waysToGetN(n-1, dp);
    int subtract3 = waysToGetN(n-3, dp);
    int subtract4 = waysToGetN(n-4, dp);

    dp[n] = subtract1 + subtract3 + subtract4;
    return dp[n];
}

int main() 
{   
    int n = 5;
    int dp[n+1] = {0};

    int res = waysToGetN(n, dp);
    cout << res << endl;

    for (int i = 0; i < 6; ++i) {
        cout << i << "->";
        cout << dp[i] << endl;
    }
    return 0;
}
