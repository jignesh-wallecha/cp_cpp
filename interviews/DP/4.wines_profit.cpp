#include <iostream>
#include <vector>
using namespace std;

int profit(int wines[], int left, int right, int year, int dp[][100]) {

    //base condn
    if (left > right) {
        return 0;
    }

    //lookup condn
    if (dp[left][right] != 0) {
        return dp[left][right];
    }

    int sellLeft = wines[left] * year + profit(wines, left+1, right, year+1, dp);
    int sellRight = wines[right] * year + profit(wines, left, right-1, year+1, dp);

    int maxProfit = max(sellLeft, sellRight);
    dp[left][right] = maxProfit;

    return maxProfit;
}

int profitBottomUpDP(vector<int> wines) {

    int dp[][wines.size()] = {0};

    int left = 0, right = wines.size();

    //base condn
    dp[left][right]
}

int main() 
{
    int wines[] = {2, 4, 6, 2, 5};
    int dp[100][100] = {0};

    int res = profit(wines, 0, 4, 1, dp);
    cout << "max profit: " << res << endl;


    return 0;
}