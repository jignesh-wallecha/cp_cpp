#include <iostream>
#include <climits>

using namespace std;

/**
 * DP (top-down): 
 * time: O(n) & Space: O(n)
*/
int min_steps(int n, int dp[]) {

    //base condn
    if (n == 1) {
        return 0; //0 steps required to reach 1 as n=1.
    }

    //lookup condn
    if (dp[n] != 0) {
        return dp[n]; 
    }

    //calculate 3 paths and get min of that three paths;
    int path1, path2, path3;

    path1 = path2 = path3 = INT_MAX;

    if (n % 3 == 0) {
        path1 = min_steps(n/3, dp);
    }
    if (n % 2 == 0) {
        path2 = min_steps(n/2, dp);
    }
    path3 = min_steps(n-1, dp);

    int soln = std::min(std::min(path1, path2), path3) + 1;
    //dp[n] = soln;
    return dp[n] = soln;
}

int minStepsBottomUpDP(int n) {

    int dp[n + 1] = {0};

    dp[0] = 0;
    dp[1] = 0;
    for (int i = 2; i <= n; ++i) {
        int path1, path2, path3;
        path1 = path2 = path3 = INT_MAX;
        if (n % 3 == 0) {
            path1 = dp[i/3];
        }
        if (n % 2 == 0) {
            path2 = dp[i/2];
        }
        path3 = dp[i-1];

        dp[i] = std::min(std::min(path1, path2), path3) + 1;
    }
    return dp[n];
}

int main() 
{
    int res = minStepsBottomUpDP(10);
    cout << "min steps: " << res << endl;

    //top-down
    int dp[100] = {0};
    res = min_steps(6, dp);
    cout << "min steps top-down: " << res << endl;
}

