#include <iostream>
#include <vector>
using namespace std;

/**
 * This problem can be solved through greedy choice, but it will not give that max profit to thief which can be achieve through DP
 * for ex: [1, 2, 3, 1] = 
 * as through greedy i will take 2 and then 3 = 6, but through dp we get 4.
 * 
 * DP logic: This problem can be divided into substructure, their will be decision by theif either to rob that currentHouse or leave that house and take adjacent house of it. 
 * 
 * Ex: nums = [2, 7, 9, 3, 1]
 *                          Robbery
 *                           /  \  
 *                          2   0
 *                         / \  /\
 *                        9  0 7 0
 *                       / \  /\
 *                      1  0 3 0
 *                           \
 *                           0
 * 
 * Recurrence eqn: max(nums[currentHouse] + fn(nums, currentHouse+2), fn(nums, currentHouse+1))
 * The path is decided either to rob that house or not
*/

int robTopDownDP(vector<int> nums, int currentHouse, int dp[])
{
    int n = nums.size();

    //base condn
    if (currentHouse >= n) {
        return 0;
    }

    //lookup condn
    if (dp[currentHouse] != 0) {
        return dp[currentHouse];
    }
    // int robCurrentHouse = nums[currentHouse] + robTopDownDP(nums, currentHouse+2, dp);
    // int skipCurrentHouse =  robTopDownDP(nums, currentHouse+1, dp);
    //return dp[currentHouse] = std::max(robCurrentHouse, skipCurrentHouse);
    return dp[currentHouse] = std::max(nums[currentHouse] + robTopDownDP(nums, currentHouse+2, dp), robTopDownDP(nums, currentHouse+1, dp));
}
/**
 * Complexity: 
 * time: O(1) for base condn
 *       O(n)
 * space: O(n): maintain array
*/ 
int robBottomUpDP(vector<int> nums) 
{   
    int n = nums.size();
     //base codn
    if (n == 1) {
        //means their is only one house in row
        return nums[n-1];
    } else if (n == 2) {
        //their are two house in row take max of both houses money
        return std::max(nums[n-2], nums[n-1]);
    }
    
    int dp[100] = {0};

    dp[n-1] = nums[n-1];
    dp[n-2] = std::max(nums[n-1], nums[n-2]);
    for (int i = n-3; i >=0; --i) {
        dp[i] = std::max(nums[i] + dp[i+2], dp[i+1]);
    }
    return dp[0];
}

int main() 
{
    int dp[10] = {0};

    vector<int> nums = {2, 7, 9, 3, 1};
    int res = robTopDownDP(nums, 0, dp);
    cout << "max profit: " << res << endl;

    // nums = {1, 2, 3, 1};
    // res = robTopDownDP(nums, 0, dp);
    // cout << "max profit: " << res << endl;
 
    // for (int i = 0; i < 5; ++i) {
    //     cout << i << " -> ";
    //     cout << dp[i] << endl; 
    // }

    cout << "bottom up:\n";
    res = robBottomUpDP(nums);
    cout << res << endl;
    return 0;   
}