#include <iostream>
#include <climits>
using namespace std;

/**
 * n - is currency value
 * coins[]  - coins arr from which to get change 
 * T: num of coins in coin array
 * dp[]: memoization
*/

int minCoinsTopDownDP(int n, int coins[], int T, int dp[]) {

    //base condn
    if (n == 0) {
        return 0; //0 banane ke liye koi bhi coins nhi lagega
    }

    //lookup condn
    if (dp[n] != 0) {
        return dp[n];
    }

    //main problem soln
    int solution = INT_MAX;
    for (int i = 0; i < T; ++i) {
        if (n-coins[i] >= 0) {
            int subSolution = minCoinsTopDownDP(n-coins[i], coins, T, dp);
            solution = std::min(solution, subSolution+1);
        }
    }
   //  dp[n] = solution;
    return dp[n] = solution;
}

int minCoinsBottomUpDP(int n, int coins[], int T) {

    int dp[n+1] = {0};
    //base condn
    dp[0] = 0;

    //lookup condn
    if (dp[n] != 0) {
        return dp[n];
    }

    for (int i = 1; i <= n; ++i) {
        dp[i] = INT_MAX;
        for (int j = 0; j < T; ++j) {
            if (i - coins[j] >= 0) {
                int subSolution = dp[i - coins[j]];
                dp[i] = std::min(dp[i], subSolution+1);
            }
        }
    }
    return dp[n];
} 

int main() 
{
    int dp[20] = {0};
    int coins[] = {1, 7, 10};
    int res = minCoinsTopDownDP(15, coins, 3, dp);
    cout << res << endl;

    cout << "bottom up..\n";
    res = minCoinsBottomUpDP(15, coins, 3);
    cout << res << endl;

    return 0;
}