#include<vector>
#include<iostream>
#include<algorithm>

using namespace std;
/**
int maxSubArray(vector<int>& nums) {

	int n = nums.size();

	int currentElement = nums[0];
	int result = nums[0];
	for(int i=1; i<n; i++) {
		currentElement = max(currentElement + nums[i], nums[i]);
		result = max(result, currentElement);
	}
	return result;
}
*/

int maxSubArray(int arr[], int n) {

	int currentElement = arr[0];
	int result = arr[0];

	for(int i=1; i<n; ++i) {
		if(arr[i] > currentElement + arr[i]) {
			currentElement = arr[i];
		} else {
			currentElement = currentElement + arr[i];
		}
		if(currentElement > result) {
			result = currentElement;
		}
	}
	return result;
}

int main() {

	vector<int> nums = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
	int arr[] = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
	int n = sizeof(arr) / sizeof(arr[0]);	
	// int sum = maxSubArray(nums);
	// cout << "Max Sum = " << sum << endl;

	int sum = maxSubArray(arr, n);
	cout <<"Max Sum = "<< sum << endl;
}