#include<forward_list>
#include<iostream>
#include<iterator>

using namespace std;

//Complexity: O(n)
int lenOfList(forward_list<int>& list1) {
	int len = 0;
	for(auto i=list1.begin(); i != list1.end(); ++i){
		len++;
	}
	return len;
}
	
forward_list<int>::iterator intersectionLove(forward_list<int>& list1, forward_list<int>& list2) {

	auto low1 = list1.begin();
	auto low2 = list2.begin();
	int len1 = lenOfList(list1);
	int len2 = lenOfList(list2);

	if(len1 > len2) {
		int diff = len1 - len2;	
		for(auto i = 0; i<diff; ++i) {
			low1 = std::next(low1);
		}
	} else {
		int diff = len2 - len1;
		for(auto i = 0; i<diff; ++i) {
			low2 = std::next(low1);
		}
	}

	// cout << *low1 << endl;
	// cout << *low2 << endl;

	// while(low1 != list1.end()) {
	// 	if(low1 == low2) {
	// 		return *low1;
	// 	}
	// 	low1 = std::next(low1);
	// 	low2 = std::next(low2);
	// }
	// return -1;

	while(low1 != low2) {
		low1 = std::next(low1);
		low2 = std::next(low2);
	}
	return low1;
}

ostream& operator<<(ostream& out, forward_list<int>& node) {
	out << node.front();
	return out;
}

int main() {

	forward_list<int> list1 {1, 3, 5, 7, 9, 11};
	forward_list<int> list2 {2, 4, 9, 11};

	cout << "Len of list1: " << lenOfList(list1) << endl;
	cout << "Len of list2: " << lenOfList(list2) << endl;

	auto res  = intersectionLove(list1, list2);	
	//intersectionLove(list1, list2);	

	cout << res << endl;

	return 0;
}

/**

	|------|  |-----|	|------|   |------|	  |-----|	|------|
	|--1---|->|---3-|-->|---5--|-->|---7--|-->|--9--|-->|--11--|
											  ^
											 /	
						|------|   |------|-/
						|---2--|-->|---4--|


*/	