#include<iostream>
#include<climits>
#include<vector>
#include<numeric>

using namespace std;

/**
* Peek Logic: 
* Time Complexity: O(n)
* Space : O(n).
**/
vector<int> minCandies(vector<int> ratings) {
	vector<int> candies(ratings.size(), 1);

	int i=1, sz_ = ratings.size();
	while(i < sz_) {
		bool isSmallestPeek = (ratings[i-1] > ratings[i] && ratings[i] < ratings[i+1]);
		if(!isSmallestPeek) {
			i++;
			continue;
		}
		//if we found out the peek then
		//expanding towards left
		int leftIdx = i-1;
		while((leftIdx >=0) && ratings[leftIdx] > ratings[leftIdx+1]) {
			candies[leftIdx] = max(candies[leftIdx], candies[leftIdx+1] + 1);
			--leftIdx;
		}
		//expanding towards right
		int rightIdx = i+1;
		while((rightIdx < sz_) && ratings[rightIdx] > ratings[rightIdx-1]) {
			candies[rightIdx] = candies[rightIdx-1] + 1;
			++rightIdx;
		} 
		//cout << rightIdx << endl;
		i = rightIdx;
	}
	return candies;
}

/**
* Two Pass Approach:
* Time: O(n)
* Space: O(n)
**/
/*
int minCandies(vector<int>& rewards) {

	vector<int> candies(rewards.size(), 1);

	int sz_ = rewards.size();

	//Expanding towards right;
	for(int i=1; i<sz_; ++i) {
		if(rewards[i] > rewards[i-1]) {
			candies[i] = candies[i-1]  + 1;
		}
	}

	//Expanding towards left
	for(int i=sz_-2; i>=0; --i) {
		if(rewards[i] > rewards[i+1]) {
			candies[i] = max(candies[i], candies[i+1]+1);
		}
	}
	int initial_sum = 0;
	return accumulate(begin(candies), end(candies), initial_sum);
}
*/
int main() {

	vector<int> rewards = {8, 4, 2, 1, 3, 6, 7, 9, 5, 3, 4};
	// int sum = minCandies(rewards);
	// cout << "Sum = " << sum << endl;
	rewards = {1, 2, 2};
	vector<int> candies = minCandies(rewards);
	for(auto& candy : candies) {
		cout << candy <<"  ";
	}
	cout << "\n";
	return 0;
	 
}