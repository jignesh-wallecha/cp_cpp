#include<iostream>
#include<vector>
using namespace std;

int longestPeak(vector<int>& A) {

	//min length 3 hona chaiye to find the peek
	if(A.size() < 3) {
		return 0;
	}

	int longestPeakLength = 0; // Agar koi peek mila nhi i.e array(accending/desceding order) mai ho toh return 0, Agar INT_MIN rakhoge n toh ekk test case fail ho jayega, agar array ascending order yaha descending order mai rahega toh woo INT_MIN return hoga jabh ki koi peek mila nhi toh return 0 hona chaiya tha.

	int i = 1;
	//don't use for loop kyuki usko jaha tak peek mil raha hai waha tak jaha raha hai toh ham manually i ko change krr sakte hai. We need to change 'i' in some special case. Jaha prr special case ke liye 'i' ko change krna raheta hai toh don't use for loop use while loop.

	while(i < A.size()-1) {
		bool isPeek = (A[i-1] < A[i] && A[i] > A[i+1]);
		if(!isPeek) {
			i++;
			continue;
		}

		/**
		 * We need to find left edge
		 * left pointer will go till left<=0 otherwise left[i-1] > left[i]; 
		*/
		int leftIndex = i - 2;
		while(leftIndex >= 0 && A[leftIndex] < A[leftIndex+1])  {
			leftIndex--;
		}
		//cout <<"Left Index: "<< leftIndex << endl;

		/**
		*	We need to find the right edge
		*/
		int rightIndex = i + 2;
		while(rightIndex < A.size() && A[rightIndex] < A[rightIndex-1]) {
			rightIndex++;
		}
		//cout <<"Right Index: "<< rightIndex << endl;

		/**
			We need to find the length of peek
		*/
		int currentLength = rightIndex - leftIndex - 1;
		//cout << currentLength << endl;
		longestPeakLength = std::max(longestPeakLength, currentLength);

		/**
			to search next index is peek or not,
			move currentIndex to right index for peek search
		*/
		i = rightIndex;
	}

	return longestPeakLength;
}

int main() {

	vector<int> nums = {1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3};
	int lengthOfPeek = longestPeak(nums);
	cout << "Peek = " << lengthOfPeek << endl;

	nums = {2, 1, 4, 7, 3, 2, 5};
	lengthOfPeek = longestPeak(nums);
	cout << "Peek = " << lengthOfPeek << endl;

	nums = {2, 2};
	lengthOfPeek = longestPeak(nums);
	cout << "Peek = " << lengthOfPeek << endl;

	nums = {1, 2, 3, 4, 7, 10};
	lengthOfPeek = longestPeak(nums);
	cout << "Peek = " << lengthOfPeek << endl;

	return 0;
}