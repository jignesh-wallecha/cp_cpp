#include<iostream>
#include<vector>

using namespace std;

bool isIncreasing(vector<int>& A) {
	for(int i=0; i<A.size()-1; ++i) {
		if(A[i] > A[i+1])
			return false;
	}
	return true;
}

bool isDecreasing(vector<int>& A) {
	for(int i=0; i<A.size()-1; i++) {
		if(A[i] < A[i+1])
			return false;
	}
	return true;
} 

bool isMonotonic(vector<int>& A) {
	return isIncreasing(A) || isDecreasing(A);
}

/*
bool isMonotonic(vector<int>&  A) {
	bool isIncr = true;
	bool isDecr = true;

	for(int i=0; i<A.size()-1; ++i) {
		if(A[i] > A[i+1]){
			isIncr = false;
		}
		if(A[i] < A[i+1])
			isDecr = false;
	} 
	return isIncr || isDecr;
}
*/

int main() {

	vector<int> nums = {1, 2, 2, 3};
	cout << isMonotonic(nums) << endl;

	nums = {6, 5, 5, 4};
	cout << isMonotonic(nums) << endl;

	nums = {-1, -5, -10, -1100, 1101, -1102, -9001};
	cout << isMonotonic(nums) << endl;

	nums = {1, 2, 2, 1};
	cout << isMonotonic(nums) << endl;

	nums = {5, 5 ,5 ,5};
	cout << isMonotonic(nums) << endl;
	return 0;
}