#include<iostream>
#include<vector>
using namespace std;

/**
		Question: Move Element At End.
		Expln: Given an array 'nums' and an element 'e', write a function to move all 'e' to the end of array without maintaining the relative order of other elements. (i.e altering other elements are allowed)

		Ex: I/P: [2, 1, 2, 2, 2, 3, 4, 2] e = 2
			O/P: [4, 3, 1, 2, 2, 2, 2, 2] or [1, 4, 3, 2, 2, 2, 2, 2] and so on.
	*/

	/**
		By Sorting: their would be O(nlogn) complexity.

		By Counting: their would be O(n) to count elements O(n) to create new array and then O(n) loop to put elements and their would be O(n) space complexity. So, O(n) + O(n) + O(n) = O(n) time & O(n) Space.

		Two pointer approach: O(n) time complexity
	*/

	/**
		Traditional Array ke through
	*/

void moveToEndTraditionalWay(int arr[], int n, int element) {

	int i = 0;
	int j = n-1;

	while(i < j) {

		while(i < j && arr[j] == element) {
			--j;
		}
		//j is at the index where element is not placed.
		if(arr[i] == element) {
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
		++i;
	}
}

	/**
		STL ke through
	*/


void moveToEndSTLWay(vector<int> arr, int element) {

	int low = 0;
	int high = arr.size()-1;

	while(low < high) {

		while(low < high && arr[high] == element) {
			--high;
		}
		//j is at the index where element is not placed.
		if(arr.at(low) == element) {
			std::swap(arr[low], arr[high]);
		}
		++low;
	}
}

int main() {

	int arr[] = {2, 1, 2, 2, 2, 3, 4, 2};
	int n = sizeof(arr) / sizeof(arr[0]);
	moveToEndTraditionalWay(arr, n, 2);
	cout << "Array After pushing all "<<2<<" to end of array:\n";
	for(int i=0; i<n; i++)
		cout << arr[i] <<"  ";
	cout << endl;

	vector<int> vectorArr = {2, 1, 2, 2, 2, 3, 4, 2};
	cout << "Vector After pushing all "<<2<<" to end of array:\n";
	moveToEndSTLWay(vectorArr, 2);
	for(int i : vectorArr) {
		cout << i <<"  ";
	}
	cout << "\n";
	return 0;  
}