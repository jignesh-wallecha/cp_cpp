#include<iostream>
#include<vector>
#include<climits>

using namespace std;

int maximumProduct(vector<int>& nums) {

	if(nums.size() < 3) {
		return -1;
	}
	int sz = nums.size();
	int max_product = INT_MIN;

	for(int i=0; i<sz-2; ++i) {
		for(int j=i+1; j<sz-1; ++j) {
			for(int k = j+1; k<sz; ++k) {
				max_product = max(max_product, { nums[i]* nums[j] * nums[k] });
			}
		}
	}
	return max_product;
}

int main() {

/*	vector<int> nums = {2, 3, -2, 4, -1};

	int res = maximumProduct(nums);
	cout << "Result : " << res << endl;

	nums = {-2, 0, -1};
	res = maximumProduct(nums);
	cout << "Result : " << res << endl;

	nums = {1, 2, 3};
	res = maximumProduct(nums);
	cout << "Result : " << res << endl;

	nums = {1, 2, 3, 4};
	res = maximumProduct(nums);
	cout << "Result : " << res << endl;

	nums = {-2};
	res = maximumProduct(nums);
	cout << "Result : " << res << endl;

	nums = {-1,-2,-3};
	res = maximumProduct(nums);
	cout << "Result : " << res << endl;
*/
	vector<int> nums = {-100, -98, -1, 2, 3, 4};
	int res = maximumProduct(nums);
	cout << "Result : " << res << endl;

	nums = {2, 3, -2, 4, -1};
	 3res = maximumProduct(nums);
	cout << "Result : " << res << endl;
}