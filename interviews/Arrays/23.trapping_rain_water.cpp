#include<iostream>
#include<vector>
using namespace std;

/**
* Time Complexity: O(n) + O(n) = O(2n) = O(n)
* Space Complexity: O(n) + O(n) = O(2n) = O(n)
*
int trap(vector<int>& height) {

	vector<int> leftWalls(height.size(), 0);
	vector<int> rightWalls(height.size(), 0);

	//filling max walls for leftWalls 
	int currentLeftMax = 0;
	for(int i=0; i<height.size(); ++i) {
		leftWalls[i] = currentLeftMax;
		currentLeftMax = max(currentLeftMax, height[i]);
	}
	/**
	* LeftWalls = {0  0  8  8  8  8  8  8  10  10  10  10  10  10}
	*

	//filling max walls for rightWalls
	int currentRightMax = 0;
	for(int i=height.size()-1; i>=0; --i) {
		rightWalls[i] = currentRightMax;
		currentRightMax = max(currentRightMax, height[i]);
	}
	/**
	* RightWalls = {0  0  8  8  8  8  8  8  10  10  10  10  10  10}
	*

	/**calculating total how much water can be trapped i.e by getting
	* min height of leftWalls & rightWalls and then subtracting it from 
	* currentWall. 
	*
	int total = 0;
	for(int i=0; i<height.size(); ++i) {
		int currentHeight = height[i];
		int minHeight = min(leftWalls[i], rightWalls[i]);
		if(minHeight > currentHeight) {
			int currentWaterTrap = minHeight - currentHeight;
			total += currentWaterTrap;
		}
	}
	return total;
}
*/
/**
* Time: O(n) & Space: O(n)
*
int trap(vector<int>& height) {

	vector<int> leftWalls(height.size(), 0);
	vector<int> rightWalls(height.size(), 0);

	//filling max walls for leftWalls 
	int currentLeftMax = 0;
	for(int i=0; i<height.size(); ++i) { 
		leftWalls[i] = currentLeftMax;
		currentLeftMax = max(currentLeftMax, height[i]);
	}
	

	//filling max walls for rightWalls
	int currentRightMax = 0;
	int total = 0;
	for(int i=height.size()-1; i>=0; --i) {
		int currentHeight = height[i];
		//cout <<"left: "<< leftWalls[i] <<endl;
		int minHeight = min(currentRightMax, leftWalls[i]);
		if(currentHeight < minHeight) {
			int currentWaterTrap = minHeight - currentHeight;
			total += currentWaterTrap;
		}
		currentRightMax = max(currentRightMax, height[i]);
	}
	return total;
}
*/
/**
* 2 pointers Approach:
* Time: O(n) & Space: O(1)
*/

int trap(vector<int>& height) {
	int left = 0, right = height.size()-1;
	int leftMax = 0, rightMax = 0;
	int total = 0;

	while(left < right) {
		leftMax = max(leftMax, height[left]);
		rightMax = max(rightMax, height[right]);

		if(height[left] < height[right]) {
			//currentHeight = height[left];
			int currentWaterTrap = leftMax - height[left];
			total += currentWaterTrap;
			++left;
		} else {
			//currentHeight = height[right];
			int currentWaterTrap = rightMax - height[right];
			total += currentWaterTrap;
			--right;
		}
	}
	return total;
}


int main() {

	vector<int> height = {0, 8, 0, 0, 5, 0, 0, 10, 0, 0, 1, 1, 0, 3};
	int res = trap(height);
	cout << "Total Water Trapped is: " << res <<endl;

	height = {0,1,0,2,1,0,1,3,2,1,2,1};
	res = trap(height);
	cout << "Total Water Trapped is: " << res <<endl;

	height = {4,2,0,3,2,5};
	res = trap(height);
	cout << "Total Water Trapped is: " << res <<endl;
 
	return 0;
}