#include<iostream>
#include<vector>
using namespace std;

int findMin(vector<int>& nums) {

	int low = 0;
	int high = nums.size()-1;

	if(nums.size() == 1 || nums[0] < nums[high]) {
		return nums[0];
	}

	while(low <= high) {

		int mid = low + (high-low)/2;

		//checking whether mid is inflection point or not.
		if(nums[mid] > nums[mid+1]) {
			return nums[mid+1];
		}
		if(nums[mid] < nums[mid-1]) {
			if(nums[mid-1] == nums[mid]) {
				continue;
			}
			return nums[mid];
		}

		//end of check so now update low ot high accordingly
		if(nums[mid] > nums[0]) {
			low = mid+1;
		} else {
			high = mid -1;
		}
	}
	return -1; 
}

int main()
{
	//vector<int> nums = {4, 5, 7, 0, 1, 2};
	int res;
/*	res = findMin(nums);
	cout << "Min in rotated sorted array is = " << res << endl;

	nums = {-2};
	res = findMin(nums);
	cout << "Min in rotated sorted array is = " << res << endl;

	nums = {11, 13, 15, 17};
	res = findMin(nums);
	cout << "Min in rotated sorted array is = " << res << endl;
*/
	vector<int> nums = {3, 4, 5, 6, 7, 8, 2, 2, 3, 4};
	res = findMin(nums);
	cout << "Min in rotated sorted array is = " << res << endl;

	return 0;
}

