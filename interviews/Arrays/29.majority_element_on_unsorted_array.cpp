#include<iostream>
#include<vector>
#include<unordered_map>

using namespace std;

/**
* Bruteforce Approach:
*
int majorityElement(vector<int>& nums) {
	int majorityCount = nums.size()/2;
	int i, j;
	for(i=0; i<nums.size(); ++i) {
		int candidate = nums[i];
		int count = 0;
		for(j=0; j<nums.size(); ++j) {
			if(candidate == nums[j]) {
				++count;
			}
		}
		if(count > majorityCount) {
			return nums[i];
		}
	}
	return -1;

	//for-each through:
	for(const auto& candidate : nums) {
		int count = 0;
		for(const auto& ele : nums) {
			if(candidate == ele) {
				++count;
			}
		}
		if(count > majorityCount) {
			return candidate;
		}
	}
	return -1;
}
*/
/**
* HashMap Approach:
*
int majorityElementUsingHashMap(vector<int>& nums) {
	unordered_map<int, int> hash_table;
	int target = nums.size()/2;

	for(int i = 0; i<nums.size(); ++i) {
		if(hash_table.find(nums[i] != hash_table.end())) {
			hash_table[nums[i]]++;
		} else {
			hash_table.insert(make_pair(nums[i], i));
		}

		if(hash_table.find(nums[i] != hash_table.end() && hash_table[nums[i]] > target)) {
			return nums[i];
		}
	}
	return -1;
}
*/

/**
	Complexity:
	Time: O(n) & Space: O(1)
*/
int mooreVotingAlgo(vector<int>& nums) {
	int candidate = nums[0];
	int votes = 1;

	for(int i=1; i<nums.size(); ++i) {
		if(nums[i] == candidate) {
			votes++;
		} else {
			votes--;
			if(votes == 0) {
				votes = 1;
				candidate = nums[i];
			}
		}
	}
	return candidate;
}

int majorityElement(vector<int>& nums) {

	int candidate = mooreVotingAlgo(nums);
	int counter = 0;
	for(int num : nums) {
		if(num == candidate) counter++;
	}
	if(counter > (nums.size()/2)) {
		return candidate;
	}
	return -1;
}

int main() {

	vector<int> nums = {3, 2, 3};
	int res = majorityElement(nums);
	cout << "Majority element in unsorted Array is : " << res <<endl;
	
	nums = {1, 2, 1, 1, 1, 2, 3};
	res = majorityElement(nums);
	cout << "Majority element in unsorted Array is : " << res <<endl;

	nums = {1, 2, 2, 3, 3, 4, 5, 5};
	res = majorityElement(nums);
	cout << "Majority element in unsorted Array is : " << res <<endl;
	return 0;
}