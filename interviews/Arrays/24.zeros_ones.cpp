#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int countZeros(vector<int>& nums) {
	return count(begin(nums), end(nums), 0);
}

void zerosOnes(vector<int>& nums) {

	int zerosCount = countZeros(nums);
	for(int i=0; i<nums.size(); ++i) {
		if(i < zerosCount) {
			nums[i] = 0;
		} else {
			nums[i] = 1;
		}
	}
}

int main() {

	vector<int> nums = {0, 0, 1, 0, 1, 1, 0, 1, 1, 0};
	zerosOnes(nums);

	for(int i=0; i<nums.size(); ++i) {
		cout << nums[i] <<"  ";
	}
	cout << '\n';

	return 0;
}