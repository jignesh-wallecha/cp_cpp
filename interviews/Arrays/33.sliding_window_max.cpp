#include<iostream>
#include<vector>
#include<list>
#include<set>
#include<climits>

using namespace std;

/**
* Naive OR Intutive Approach:(Bruteforce)
* Time: O(nk)
* Space: O(n)
**
*/

vector<int> slidingWindowMaximum(vector<int>& nums, int k) {
	vector<int> maxes;
	int maxEle = INT_MIN;
	int size_ = nums.size();
	for(int i=0; i<=size_-k; ++i) {
		for(int j=i; j<i+k; ++j) {
			maxEle = std::max(maxEle, nums[j]);
		}	
		maxes.push_back(maxEle);
		maxEle = -1;
	}
	return maxes;
}

/**
* Using AVL Tree Data Structure
1. Construction of tree is O(k)
2. Search the max from tree will take O(log k)
3. delete the first/max from tree as window slides : O(log k)
4. again insert will take O(log k)

Step 2-4 will be repeated n times, so final Complexity is, O(nlogk) which is 
less than above approach
**/

int findMax(multiset<int>& avlTree) {
	int maxElement;
	if(!avlTree.empty()) {
		maxElement = *(avlTree.rbegin());
	}
	return maxElement;
}

vector<int> slidingWindowMaximum(vector<int>& nums, int k) {

	vector<int> maxes;
	//constructed avlTree which taked O(k).
	multiset<int> avlTree(begin(nums), begin(nums) + k);

/*	for(int i=0; i<k; ++i) {
		avlTree.insert(nums[i]);
	}
*/	
	//searched the max element and pushed in vector
	maxes.emplace_back(findMax(avlTree));

	//as window gets slided, element is not their in that window
	//so delete that element, which will take O(logk)(as joh bhi element
	// delete hoga n woo leaf mai rahega).
	
	for(int i=k; i<nums.size(); ++i) {
		//erase passed window ele
		avlTree.erase(avlTree.find(nums[i-k]));
		//insert new ele
		avlTree.insert(nums[i]);
		//push maxEle from avlTree to vector of maxes.
		maxes.emplace_back(findMax(avlTree));
	}
	return maxes;
}

/**
* Using DoublyLL (Data Structure).
* 
* 
*/
vector<int> slidingWindowMaximum(vector<int>& nums, int k) {

	vector<int> maxes;
	list<int> window;

	for(int i=0; i<k; ++i) {
		while(!window.empty() && nums[i] >= nums[window.back()]) {
			window.pop_back();
		}
		window.push_back(i);
	}

	for(int i=k; i<nums.size(); ++i) {
		maxes.push_back(nums[window.front()]);

		if(window.front() == (i-k)) {
			window.pop_front();
		}

		while(!window.empty() && nums[i] >= nums[window.back()]) {
			window.pop_back();
		}
		window.push_back(i);
	}
	maxes.push_back(nums[window.front()]);
	return maxes;
}


int main() {

	vector<int> nums = {10, 4, 2, 11, 3, 15, 12, 8, 7, 9, 21, 24};
	int k = 3;
	vector<int> res = slidingWindowMaximum(nums, k);
	for(auto& e : res) {
		cout << e <<" ";
	}
	cout << '\n';
	return 0;
}