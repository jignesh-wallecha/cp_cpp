#include<iostream>
#include<vector>
#include<climits>
#include<algorithm>

using namespace std;

/**
* Sorting Way:

void UnsortedContinuousSubArray(vector<int>& nums) {

	if(is_sorted(begin(nums), end(nums))) {
		return {-1, -1};
	}
	
	vector<int> v_copy(nums.size());

	copy(begin(nums), end(nums), begin(v_copy)); //O(n);

	sort(begin(v_copy), end(v_copy)); //O(nlogn)

	int i = 0;
	while(i < nums.size() && nums[i] != v_copy[i]) {
		i++;
	}
	return make_pair( ,i-1);


}
*/

/**
	This below program takes time complexity of O(n) + O(n) + O(n) = O(n).
	Space Complexity = O(n) for pushing out of order elements in another vector.
*/
pair<int, int> UnsortedSubArray(vector<int>& nums) {

	int minElementOutOfOrder = INT_MAX;
	int maxElementOutOfOrder = INT_MIN;
	vector<int> outOfOrder = {};
	//cout << nums.at(nums.size()-1) << endl;
	/**
	* Find the unsorted subarray elements
	*/
	for(int i=0; i<nums.size()-1; ++i) {
		if(nums[i] > nums[i+1]) {
			outOfOrder.insert(end(outOfOrder), {nums[i], nums[i+1]});
		} else {
			continue;
		}
	}
	/**
	* Get min & max from out of order subarray elements
	*/

	for(int i=0; i<outOfOrder.size(); ++i) {
		minElementOutOfOrder = std::min(minElementOutOfOrder, outOfOrder[i]);
		maxElementOutOfOrder = std::max(maxElementOutOfOrder, outOfOrder[i]);
	}

	/**
	* Find the correct place for min & max element from nums array.
	*/
	int leftIndex = 0; 
	int rightIndex = nums.size()-1;

	while(minElementOutOfOrder >= nums[leftIndex]) {
		leftIndex++;
	}

	while(maxElementOutOfOrder <= nums[rightIndex]) {
		rightIndex--;
	}
	
	return make_pair(leftIndex, rightIndex);

}


/**
bool isOutOfOrder(int index, int currentNumber, vector<int>& nums) {
	if(index == 0) {
		return currentNumber > nums[index+1];
	} else if(index == nums.size()-1) {
		return currentNumber < nums[index-1];
	}

	return currentNumber > nums[index+1] || currentNumber < nums[index-1];
}
**/

//pair<int, int> shortestUnsortedContinuousSubArray(vector<int>& nums) {

/**	
	if(is_sorted(nums.begin(), nums.end())) {
		return make_pair(-1, -1);
	}
*/

/**	int minOutOfOrder = INT_MAX;
	int maxOutOfOrder = INT_MIN;

	for(int i=0; i<nums.size()-1; ++i) {
		int currentNumber = nums[i];
		if(isOutOfOrder(i, currentNumber, nums)) {
			minOutOfOrder = min(minOutOfOrder, currentNumber);
			maxOutOfOrder = max(maxOutOfOrder, currentNumber);
		}
	}

	
	if(minOutOfOrder == INT_MAX) {
		return make_pair(-1, -1);
	}

	int subArrayLeftIndex = 0;
	while(minOutOfOrder >= nums[subArrayLeftIndex]) {
		subArrayLeftIndex++;
	}

	int subArrayRightIndex = nums.size()-1;
	while(maxOutOfOrder <= nums[subArrayRightIndex]) {
		subArrayRightIndex--;
	}

	return make_pair(subArrayLeftIndex, subArrayRightIndex);

}
*/




int main() {

	vector<int> nums = {5, 2, 3, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19};
	//nums = {1, 2, 3, 4, 5, -1, 7, 10};
/*	pair<int, int> unsortedArrayIndex = shortestUnsortedContinuousSubArray(nums);
	cout << "Left: " << unsortedArrayIndex.first << endl;
	cout << "Right: " << unsortedArrayIndex.second <<endl;

	nums = {1, 2, 3, 4, 5, 6, 7, 8};
	unsortedArrayIndex = shortestUnsortedContinuousSubArray(nums);
	cout << "Left: " << unsortedArrayIndex.first << endl;
	cout << "Right: " << unsortedArrayIndex.second <<endl;

	nums = {2, 6, 4, 8, 10, 9, 15};
	unsortedArrayIndex = shortestUnsortedContinuousSubArray(nums);
	cout << "Left: " << unsortedArrayIndex.first << endl;
	cout << "Right: " << unsortedArrayIndex.second <<endl;
*/
	//UnsortedContinuousSubArray(nums);

//	pair<int, int> pairUnsortedArrayIndex = UnsortedSubArray(nums);
//	cout << pairUnsortedArrayIndex.first << ", " << pairUnsortedArrayIndex.second << endl;

	for(int i=0; i<nums.size(); ++i) {
		//if(i > 0 && nums[i] == nums[i-1] +1 )
		cout << nums[i-1] + 1 <<" ";
	}
	cout << endl;
	return 0;
}