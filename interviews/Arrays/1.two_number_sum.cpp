#include<iostream>
#include<algorithm>
#include<vector>
#include<unordered_map>

using namespace std;

vector<int> twoSum(vector<int>& nums, int target) {
	unordered_map<int, int> lookup;
	for(int i=0; i<nums.size(); i++) 
	{
		if(lookup.count(target-nums[i])) 
		{
			return {lookup[target-nums[i]], i};
		}
		lookup[nums[i]] = i;
	}
	return {-1, -1}; 
}

int main() {

	vector<int> nums = {2, 7, 11, 15};
	int target = 9;

	vector<int> res = twoSum(nums, target);
	for(auto& item : res) {
		cout << item << endl;
	}
	
	nums = {3, 5, -4, 8, 11, 1, -1, 6};
	target = 10;
	res = twoSum(nums, target);
	for(auto& item : res) {
		cout << item << endl;
	}
}