#include<iostream>
#include<vector>
#include<unordered_map>
#include<string>
#include<algorithm>
#include<climits>

using namespace std;

int distanceBetween(int index1, int index2) {
	return (std::abs(index1 - index2));
}

int getIndexOfMinValue(vector<int>& a) {
	int minValue = INT_MAX;
	int minIndex = -1;

	for(int i=0; i<a.size(); ++i) {
		if(minValue > a[i]) {
			minValue = a[i];
			minIndex = i;
		}
	}
	return minIndex;
}

int apartmentHunting(vector<unordered_map<string, bool>> blocks, vector<string> reqs) {

	vector<int> maxDistanceOfServiceAtBlocks(blocks.size(), INT_MIN);

	for(int i=0; i<blocks.size(); ++i) {

		if((i > 0) && maxDistanceOfServiceAtBlocks[i-1] == 0) {
			return i-1;
		}

		//loop through all requirments
		for(string req : reqs) {
			//find the nearest block from 'i' which has the requriment 'req'
			int nearestDistance = INT_MAX; 
			for(int j=0; j<blocks.size(); ++j) {
				if(blocks[j][req]) {
					nearestDistance = min(nearestDistance, distanceBetween(i, j));
				}
			}
			maxDistanceOfServiceAtBlocks[i] = max(maxDistanceOfServiceAtBlocks[i], nearestDistance);
		}
	}

	vector<int>::iterator it = std::min_element(begin(maxDistanceOfServiceAtBlocks), end(maxDistanceOfServiceAtBlocks));
	return std::distance(maxDistanceOfServiceAtBlocks.begin(), it);

	//return getIndexOfMinValue(maxDistanceOfServiceAtBlocks);
}

/**

void apartmentHunting(vector<unordered_map<std::string, bool>> blocks, vector<std::string> reqs) {

	for(int i=0; i<blocks.size(); ++i) {

		for(string req : reqs) {

			for(int j=0; j<blocks.size(); ++j) {

				if(blocks[j][req]) {

				}
			}
		}
	}
}
*/
int main() {

	vector<unordered_map<string, bool>> blocks = {
		{{"gym", false}, {"school", true}, {"store", false}},
		{{"gym", true}, {"school", false}, {"store", false}},
		{{"gym", true}, {"school", true}, {"store", false}},
		{{"gym", false}, {"school", true}, {"store", false}},
		{{"gym", false}, {"school", true}, {"store", true}},
	};

	vector<string> requirements = {"gym", "school", "store"};

	int blockNumber = apartmentHunting(blocks, requirements);
	cout << "Block num: " << blockNumber << endl; 
	return 0; 
}