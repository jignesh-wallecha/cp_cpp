#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

/**
*  Shuffle the Array
* Given the array nums consisting of 2n elements in the form 
[x1,x2,...,xn,y1,y2,...,yn].
Return the array in the form [x1,y1,x2,y2,...,xn,yn].

Example 1:

Input: nums = [2,5,1,3,4,7], n = 3
Output: [2,3,5,4,1,7] 
Explanation: Since x1=2, x2=5, x3=1, y1=3, y2=4, y3=7 then the answer is [2,3,5,4,1,7].

Example 2:

Input: nums = [1,2,3,4,4,3,2,1], n = 4
Output: [1,4,2,3,3,2,4,1]


**/
/**
* Two Pointer Approach:
* Time: O(n) & Space: O(n)
**/
vector<int> shuffle(vector<int>& nums, int& n) {
	vector<int> res(2 * n);
    int i =0, j = n;
    int k =0;
    while(i < j && j < nums.size()) {
        res[k++] = nums[i++];
        res[k++] = nums[j++];
   	}
    return res;
}

int main() {

	vector<int> nums = {2,5,1,3,4,7};
	int n = 3;

	vector<int> res = shuffle(nums, n);
	for_each(begin(res), end(res), [](auto& i) { cout << i <<"  "; });

    cout << '\n';

    nums = {1,2,3,4,4,3,2,1};
    n = 4;
    res = shuffle(nums, n);
    for_each(begin(res), end(res), [](auto& i) { cout << i <<"  "; });


}