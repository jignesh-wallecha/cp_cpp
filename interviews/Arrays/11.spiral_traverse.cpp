#include<iostream>
#include<vector>

using namespace std;

void spiralFill(vector<vector<int>>& matrix, int startRow, int endRow, int startCol, int endCol, vector<int>& result) {

	if(startRow > endRow || startCol > endCol) {
		return;
	}

	//traversing top border
	for(int i=startCol; i<=endCol; ++i) {
		result.push_back(matrix[startRow][i]);
	}

	//traversing the right border
	for(int i=startRow+1; i<=endRow; ++i) {
		result.push_back(matrix[i][endCol]);
	}

	//traversing the bottom border
	for(int i=endCol-1; i>=startCol; --i) {
		result.push_back(matrix[endRow][i]);
	}

	//traversing the left border
	for(int i=endRow-1; i > startRow; --i) {
		result.push_back(matrix[i][startCol]);
	}

	spiralFill(matrix, startRow+1, endRow-1, startCol+1, endCol-1, result);
}

vector<int> spiralTraverse(vector<vector<int>>& matrix) {

	vector<int> result;

	int startRow, startCol, endRow, endCol;

	startRow = startCol = 0;
	endRow = matrix.size()-1;
	endCol = matrix[0].size()-1;

	spiralFill(matrix, startRow, endRow, startCol, endCol, result);

	return result;
}

/*
vector<int> spiralTraverse(vector<vector<int>>& matrix) {

	vector<int> result;

	int startRow, startCol, endRow, endCol;

	startRow = startCol = 0;
	endRow = matrix.size()-1;
	endCol = matrix[0].size()-1;

	while(startRow <= endRow && startCol <=endCol) {

		//traversing top border
		for(int i=startCol; i<=endCol; ++i) {
			result.push_back(matrix[startRow][i]);
		}

		//traversing the right border
		for(int i=startRow+1; i<=endRow; ++i) {
			result.push_back(matrix[i][endCol]);
		}

		//traversing the bottom border
		for(int i=endCol-1; i>=startCol; --i) {
			result.push_back(matrix[endRow][i]);
		}

		//traversing the left border
		for(int i=endRow-1; i > startRow; --i) {
			result.
		}

		startCol++;
		startRow++;
		endCol--;
		endRow--;
	}
	return result;
}
*/

int main() {

	vector<vector<int>> spiralMatrix = {
		{1, 2, 3, 4},
		{12, 12, 14, 5},
		{11, 16, 15, 6},
		{10, 9, 8, 7},
		{1, 2, 3, 4}
	};

	vector<int> res = spiralTraverse(spiralMatrix);
	for(auto& item: res) {
		cout << item <<"  ";
	}
	return 0;
}
