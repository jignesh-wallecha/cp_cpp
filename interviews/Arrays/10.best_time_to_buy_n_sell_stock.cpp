#include<iostream>
#include<algorithm>
#include<vector>
#include<climits> //for INT_MAX

using namespace std;

int maxProfit(vector<int>& prices) {

	if(prices.empty())
		return 0;

	int minPrice = INT_MAX;
	int maxProfit = 0;

	for(const auto& price : prices) {
		/*
			if(price < minPrice) {
				minPrice = price;
			} else if(price - minPrice > maxProfit) {
				maxProfit = price - minPrice;
			}
		*/ 
		minPrice = min(minPrice, price);
		maxProfit = max(maxProfit, price-minPrice);
	}
	return maxProfit;
}

int main() {

	vector<int> prices = {-2, 1, -3, 4, -1, 2, 1, -5, 4};

	int profit = maxProfit(prices);
	cout << "Max Profit = " << profit << endl;

	prices = {};
	profit = maxProfit(prices);
	cout << "Max Profit = " << profit << endl;

	return 0;
}