#include<iostream>
#include<vector>

using namespace std;

vector<int> zigzagTraverse(vector<vector<int>>& matrix) {

	vector<int> result;
	bool goingDiagDown = true;
	int row, col;
	row = 0;
	col = 0;
	while(row < matrix.size() && col < matrix[0].size()) {

		//continue our zigzag traversal

		//1.append the current value in the result
		result.push_back(matrix[row][col]);

		//2.take the decision according to current direction
		if(goingDiagDown) {
			//we're going down
			if(col == 0 || row == matrix.size()-1) {
				//either reached last row or 1st col
				goingDiagDown = false;
				if(row == matrix.size()-1) {
					++col;
				} else {
					++row;
				}
			} else {
				//we're going diagonally down
				++row;
				--col;
			}
		} else {
			//we're going up
			if(row == 0 || col == matrix[0].size()-1) {
				//either we reached first row or last col so change direction
				goingDiagDown = true;
				if(col == matrix[0].size()-1) {
					++row;
				} else {
					++col;
				}
			} else {
				//we are going diagonally up
				--row;
				++col;
			}
		}
	}
	return result;
}

int main() {

	vector<vector<int>> matrix = {
			{1, 3, 4, 10},
			{2, 5, 9, 11},
			{6, 8, 12, 15},
			{7, 13, 14, 6}
		};

	vector<int> result = zigzagTraverse(matrix);
	cout << "result: \n"
	for(auto& item : result) {
		cout << item <<"  ";
	}
	cout << endl;

	return 0; 
}