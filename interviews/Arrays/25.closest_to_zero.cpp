#include<iostream>
#include<vector>
#include<climits>

using namespace std;

/**
	Bruteforce Approach: Time Complexity: O(n^2) & Space Complexity : O(1)
*/

int closestToZero(int* nums, size_t numsofSize) {

	//breaking condn
	if(numsofSize < 2) {
		return -1;
	}

	int currentMin = INT_MAX;
	for(int i=0; i<numsofSize; ++i) {
		for(int j=i+1; j<numsofSize; ++j) {
			currentMin = min(currentMin, {std::abs(nums[i] + nums[j])});
		}
	}	
	return currentMin;
}

/**
	2 pointer approach: 
	Time Complexity : O(n) & Space Complexity: O(1)
*/

int closestToZero(vector<int>& nums) {

	//breaking condn
	if(nums.size() < 2) {
		return -1;
	}

	int currentSum = INT_MAX;	
	int low = 0, high = nums.size()-1;
	while(low < high) {
		currentSum = min(currentSum, {std::abs(nums.at(low) + nums.at(high))});
		if(currentSum < 0) {
			++low;
		} else {
			--high;
		}

	}
	return currentSum;
}


int main() {
/*

	int nums[] = {1, 2, 3, 4, 5, 6, 7};
	size_t numsofSize = sizeof(nums) / sizeof(int);
	int res = closestToZero(nums, numsofSize);
	cout << "Closest to Zero is : " << res << endl;

   	int nums1[] = {-2, 9, 6, 1, 2, -5};
	numsofSize = sizeof(nums1) / sizeof(int);
	res = closestToZero(nums1, numsofSize);
	cout << "Closest to Zero is : " << res << endl;

	int nums2[] = {-3, 4, -5};
	numsofSize = sizeof(nums2) / sizeof(int);
	res = closestToZero(nums2, numsofSize);
	cout << "Closest to Zero is : " << res << endl;
*/

	vector<int> nums = {-2, 9, 6, 1, 2, -5};
	int res = closestToZero(nums);
	cout << "Closest to Zero is : " << res << endl;
	// O/P: 0

	nums = {1, 2, 3, 4, 5, 6,};
	res = closestToZero(nums);
	cout << "Closest to Zero is : " << res << endl;
	//O/P: 3

	return 0;
}

