#include<iostream>
#include<vector>
#include<queue>
#include<algorithm>
using namespace std;

/**
* kth Smallest Element:
* 
* Given an array arr[] and a number k where k is smaller than size of the array, the task is to find the kth smallest element in the given array. It is given that all array elements are distinct.

Ex1:
I/P: N = 6;
arr[] = [7, 10, 4, 3, 20, 15];
K = 3;
O/P: 7

Logic: 
Using Min heap data structure.

Using Sort Approach:
[7, 10, 4, 3, 20, 15];
After Sort:
0	1  2   3  4   5  	
[3, 4, 7, 10, 15, 20];
return nums[k-1];

**/

int kthSmallestEle(vector<int>& nums, int k) {
	if(nums.empty()) return -1;

	priority_queue<int, vector<int>, std::greater<int>> minHeap;
    for(auto& e : nums) minHeap.emplace(e);
    int res;
    for(int i = 0; i<k; ++i) {
        res = minHeap.top();
        minHeap.pop();
    }
    return res;   
}

/**
* Space: O(1)
* Time: O(1)
**/
int kthSmallestEle(vector<int>& nums, int k) {
	sort(begin(nums), end(nums));
	return nums[k-1];

}
int main() {
	vector<int> nums = {7, 10, 4, 3, 20, 15};
	int k = 3;
	int res = kthSmallestEle(nums, k);
	cout << res << endl;
}