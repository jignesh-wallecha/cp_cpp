#include<iostream>
#include<vector>

using namespace std;

/**
* Using Bruteforce Approach:
* count of subarray which sums to x / target
**

int sumOfSubArray(vector<int>& nums, int& x) {
	int count = 0;
	for(int i=0; i<nums.size(); ++i) {
		for(int j=i+1; j<nums.size(); ++j) {
			int sum = 0;
			for(int k = i; k<=j; ++k) {
				sum += nums[k];
			}
			if(sum == x) count++;
		}
	}
	return count;
}
*/

/**
* Using Bruteforce Approach
* return subarray with sums to x / target
**/
vector<vector<int>> sumOfSubArray(vector<int>& nums, int& x) {
	vector<vector<int>> pairs;
	for(int i=0; i<nums.size(); ++i) {
		for(int j=i+1; j<nums.size(); ++j) {
			int sum = 0;
			for(int k = i; k<=j; ++k) {
				sum += nums[k];
			}
			if(sum == x) {
				pairs.push_back({i, j});
			}
		}
	}
	return pairs;
}

/**
* Using two Pointer Approach:
**
bool sumOfSubArray(vector<int>& nums, int target) {

	if(nums.size() < 1) {
		return false;
	}
	int low, high, sum;
	low = 0;
	sum = nums[0];

	for(high = 1; high <=nums.size(); ++high) {
		while(sum > target && low < high-1) {
			sum = sum - nums[low++];
		}
		if(sum == target) return true;

		if(high < nums.size()) {
			sum = sum + nums[high];
		}
	}
	return false;
}
*/
int main() {

/*	vector<int> nums = {10, 5, 6, 12, 8, 2};
	int target = 2;
	cout << std::boolalpha;
	bool res = sumOfSubArray(nums, target);
	cout << res << endl;
	//true

	nums = {5, 4, 6, 7, 9, 8, 3, 1, 2};
	target = 5;
	res = sumOfSubArray(nums, target);
	cout << res << endl;
	//true

	nums = {5, 4, 6, 7, 9, 8, 3, 1, 2};
	target = 0;
	res = sumOfSubArray(nums, target);
	cout << res << endl;
	//false
*/
	vector<int> nums = {10, 5, 6, 12, 8, 2};
	int target = 26;
	nums = {5, 4, 6, 7, 9, 8, 3, 1, 2};
	target = 21;
	vector<vector<int>> res = sumOfSubArray(nums, target);
	for(auto& r : res) {
		for(auto& e : r) {
			cout << e <<"  ";
		}
		cout << '\n';
	}

}
