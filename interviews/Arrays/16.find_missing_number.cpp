#include<iostream>
#include<vector>
#include<algorithm> //for sort()
using namespace std;

/**
* Question: Find the missing one Number
* Given an array nums containing n distinct numbers in the range [0, n], return the only number in the range that is missing from the array.

Follow up: Could you implement a solution using only O(1) extra space complexity and O(n) runtime complexity?

Example 1:

Input: nums = [3,0,1]
Output: 2
Explanation: n = 3 since there are 3 numbers, so all numbers are in the range [0,3]. 2 is the missing number in the range since it does not appear in nums.

Example 2:
Input: nums = [9,6,4,2,3,5,7,0,1]
Output: 8
Explanation: n = 9 since there are 9 numbers, so all numbers are in the range [0,9]. 8 is the missing number in the range since it does not appear in nums.

Constraints:

n == nums.length
1 <= n <= 104
0 <= nums[i] <= n
All the numbers of nums are unique.

LOGIC:


**/


/**
* Intutive Approach: Sorting
**/

int missingNumber(vector<int>& nums) {
	sort(begin(nums), end(nums));

	for(auto& n : nums) {
		cout << n <<" ";
	}
	cout << '\n';
	//ensure that 0 ele is at beg i.e at 0th idx, is it is not thier then return
	//that 0 is missing from array, same for check for last idx i.e than nth ele.
	if(nums[0] != 0) {
		return 0;
	} else if(nums[nums.size()-1] != nums.size()) {
		return nums.size();
	}

	//above corner cases are tested.
	for(int i=1; i<nums.size(); ++i) {
		int expectedNum = nums[i-1] + 1;
		if(nums[i] != expectedNum) {
			return expectedNum;
		}
	}

	//Array was not missing any numbers.
	return -1;
}

int main() {

	vector<int> nums = {9,6,4,2,3,5,7,0,1};
	int res = missingNumber(nums);
	cout << "Missing Number in Array is: " << missingNumber << endl;

	return 0;
}