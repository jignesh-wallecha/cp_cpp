#include<iostream>
#include<vector>
#include<tuple> //used for tie function, can do without tie function also
#include<algorithm> //used for min & max function which contains { } i.e. set of values
#include<climits>
#include<utility> //for pair constructor

using namespace std;
/*
int maxProduct(vector<int>& nums) {
	int result = INT_MIN;

	int currentMin = 1;
	int currentMax = 1;

	for(const auto num : nums) {
	/*
		1st Way of doing:
		int tempCurrentMax = currentMax;
		currentMax = max({num, num*currentMax, num*currentMin});
		currentMin = min({num, num*tempCurrentMax, num*currentMin});
	*/
	/*
		2nd Way:
		int candidateMax = num * currentMax;
		int candidateMin = num * currentMin;
		currentMax = max({num, candidateMax, candidateMin});
		currentMin = min({num, candidateMax, candidateMin});
	
	//3rd Way:
		 tie(currentMin, currentMax) = make_pair(min({num, num*currentMax, num*currentMin}), max({num, num*currentMax, num*currentMin}));
		 result = max(result, currentMax);
		
	}
	return result;
}
*/
int maxProduct(vector<int>& nums) {
        
    vector<int> maxProd(nums.size());
    vector<int> minProd(nums.size());
        
    maxProd[0] = nums[0];
    minProd[0] = nums[0];
    int maxProduct = INT_MIN;
        
    for(int i=1; i<nums.size(); ++i) {
        maxProd[i] = max({nums[i], nums[i] * maxProd[i-1], nums[i] * minProd[i-1]});          minProd[i] = min({nums[i], nums[i] * maxProd[i-1], nums[i] * minProd[i-1]});
    }
        
    for(int i=0; i<maxProd.size(); ++i) {
        maxProduct = max(maxProduct, maxProd[i]);
    }
    return maxProduct;
}

int main() {

	vector<int> nums = {2, 3, -2, 4};
	int res;

	res = maxProduct(nums);
	cout << "Max Product = " << res << endl;


	nums = {2, 3, -2, 4, -1};
	res = maxProduct(nums);
	cout << "Max Product = " << res << endl;

	nums = {-2};
	res = maxProduct(nums);
	cout << "Max Product = " << res << endl;

	nums = {-100, -98, -1, 2, 3, 4};
	res = maxProduct(nums);
	cout << "Max Product = " << res << endl;
	//maxProduct(nums);
}