#include<iostream>
#include<vector>
#include<unordered_map>

using namespace std;

/**
time: O(n) & Space : O(n)
*/
bool subarraySums(vector<int>& nums, int x) {
	unordered_map<int, int> hashmap;
	int sum = 0;
	for(int i=0; i<nums.size(); ++i) {
		sum = sum + nums[i];
		int difference = sum - x;

		if(difference == 0 || hashmap.find(difference) != hashmap.end()) {
			return true;
		} 
		hashmap[sum] = i;
	}
	return false;
}

int main() {
	vector<int> nums = {8, 5, -2, 3, 4, -6, 70};
	int x = 2;
	cout << std::boolalpha;
	bool res = subarraySums(nums, x);
	cout << res << endl;
	//false
}