#include<iostream>
#include<vector>
#include <algorithm>
using namespace std;

/**
* Find All Duplicates in an Array

Given an integer array nums of length n where all the integers of nums are in the range [1, n] and each integer appears once or twice, return an array of all the integers that appears twice.

Example 1:

Input: nums = [4,3,2,7,8,2,3,1]
Output: [2,3]
Example 2:

Input: nums = [1,1,2]
Output: [1]

Constraints:

n == nums.length
1 <= n <= 105
1 <= nums[i] <= n
Each element in nums appears once or twice.

**/

vector<int> findAllDuplicates(vector<int>& nums) {
    vector<int> res;
        
    for(int i = 0; i<nums.size(); ++i) {
        if(nums[std::abs(nums[i])-1] < 0) {
            res.push_back(std::abs(nums[i]));
        } else {
            nums[std::abs(nums[i])-1] = -1 * nums[std::abs(nums[i])-1];
        }
    }
    return res;
}

int main() {

	vector<int> nums = {4,3,2,7,8,2,3,1};
	vector<int> res = findAllDuplicates(nums);
	std::for_each(begin(res), end(res), [](const auto& e) { cout << e <<" "; });
}