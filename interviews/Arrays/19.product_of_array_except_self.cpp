#include<iostream>
#include<vector>
using namespace std;

/**
	Time Complexity: O(n) & Space Complexity: O(n)
*/
vector<int> productExceptSelf(vector<int>& nums) {

	if(nums.size() == 0) {
		return {};
	}

	vector<int> output(nums.size());

	output[0] = 1;
	//going from left to right calculate the leftProducst
	for(int i=1; i<nums.size(); ++i) {
		output[i] = output[i-1] * nums[i-1];
	}

	//we don't need to create a new array to keep rightProducts so we're creating 
	//var of rightProduct and this rightProduct will be get Updatted
	//if we create new array for rightProducts this will be naive approach i.e
	//space complexity would be O(n), so we don't need this so we're creating 
	//a var of rightProduct to keep rightProducts
	int rightProduct = nums[nums.size()-1];
	//int rightProduct = 1;
	for(int i=nums.size()-2; i>=0; --i) {
		//rightProduct *= nums[i+1];
		// output[i] = output[i] * rightProduct;
		output[i] = output[i] * rightProduct;
		rightProduct *= nums[i];
	}
	return output;
}

int main() {
	vector<int> nums = {5, 3, 2, 4};

	vector<int> res;

	res = productExceptSelf(nums);
	for(auto& item : res) {
		cout << item <<" ";
	}
	cout <<endl;
}