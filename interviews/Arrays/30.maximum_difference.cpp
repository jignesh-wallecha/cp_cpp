#include<iostream>
#include<vector>
#include<climits>

using namespace std;

/**
* Solution 1:
* Time: O(n)
* Space: O(n)
*

int maxSubArray(vector<int>& nums) {

	int n = nums.size();

	int currentElement = nums[0];
	int result = nums[0];
	for(int i=1; i<n; i++) {
		currentElement = max(currentElement + nums[i], nums[i]);
		result = max(result, currentElement);
	}
	return result;
}

int maximumDifference(vector<int>& nums) {
	vector<int> difference(nums.size()-1);

	for(int i=0; i<nums.size()-1; ++i) {
		difference[i] = nums[i+1] - nums[i];
	}
	return maxSubArray(difference);
}
*/
/**
* Solution 2:
* Time : O(n)
* Space : O(1)
*
int maximumDifference(vector<int>& nums) {

	int currentMax = nums[1] - nums[0];
	int result = currentMax;
	for(int i=1; i<nums.size()-1; ++i) {
		int currentDifference = nums[i+1]-nums[i];
		currentMax = max(currentMax + currentDifference, currentDifference);
		result = max(result, currentMax);
	}
	return result;
}
*/
/**
* Intuitive Soln:
* Time: O(n)
* Space: O(1)
*/

int maximumDifferenceIntuitive(vector<int>& nums) {
	int minSoFar;
	int maxDiffSoFar = INT_MIN;
	minSoFar = nums[0];

	for(int i=1; i<nums.size(); ++i) {
		if(nums[i] > minSoFar) {
			int currentDifference = nums[i] - minSoFar;
			maxDiffSoFar = max(maxDiffSoFar, currentDifference);
		} else if(nums[i] < minSoFar) {
			minSoFar = nums[i];
		}
	}
	return maxDiffSoFar;
}

int main() {

	vector<int> nums = {4, 3, 10, 2, 11, 1, 6};
	maximumDifference(nums);
	//cout << "Maximum Difference in Array: " << res << endl;
	return 0;
}