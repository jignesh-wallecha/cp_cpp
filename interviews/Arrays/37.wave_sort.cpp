#include<iostream>
#include<vector>

using namespace std;

void waveSort(vector<int>& nums) {

	for(int i=0; i<nums.size(); i +=2) {

		if(i > 0 && nums[i-1] > nums[i]) {
			swap(nums[i-1], nums[i]);
		}

		//checking for right
		if(i < nums.size()-1 && nums[i] < nums[i+1]) {
			swap(nums[i], nums[i+1]);
		}
	}
}

void displayVector(vector<int>& nums) {
	for(const auto& item : nums) {
		cout << item <<"  ";
	}
	cout << endl;
}

int main() {
	vector<int> nums = {20, 8, 10, 6, 4};
	cout << "Before Sort\n";
	displayVector(nums);
	cout << "After Sort\n";
	waveSort(nums);
	displayVector(nums);

	return 0;
}