#include<iostream>
#include<vector>

using namespace std;

vector<int> twoSumSorted(vector<int> nums, int target) 
{
	int low = 0;
	int high = nums.size()-1;

	while(low < high) {

		int sum = nums[low] + nums[high];
		if(sum == target)
			return {low+1, high+1};
		if(sum < target)
			++low;
		else
			--high;
	}
	return {-1, -1};
}

int main() {

	vector<int> nums = {-4, -1, 1, 3, 5, 6, 8, 11};
	//
	int target = 6;

	vector<int> res = twoSumSorted(nums, target);

	for(auto& item : res)
		cout << item <<"  ";
	cout << '\n';

	nums = {2, 7, 11, 15};
	target = 9;
	res = twoSumSorted(nums, target);
	for(auto& item : res)
		cout << item <<"  ";
	cout << '\n';
	
	return 0;
}