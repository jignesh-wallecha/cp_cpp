#include<iostream>
#include<algorithm>
#include<vector>

using namespace std;

vector<vector<int>> threeSum(vector<int>& nums, int target) {

	vector<vector<int>> triplets;

	if(nums.size() < 3) 
		return triplets;

	sort(begin(nums), end(nums));

	//i = n-2 because as this for loop is for currentNum
	for(int i=0; i<nums.size()-2; ++i) 
	{
		if(i > 0 && nums[i] == nums[i-1])
		{
			continue;
		}
		int currentNum = nums[i];

		int left = i + 1;
		int right = nums.size() - 1;

		while(left < right)  {
			int currentSum = currentNum + nums[left] + nums[right];
			if(currentSum == target) {
				// vector<int> temptriplet(3, 0);
				// temptriplet[0] = currentNum;
				// temptriplet[1] = nums[left];
				// temptriplet[2] = nums[right];

				// triplets.push_back(temptriplet);

				triplets.push_back({currentNum, nums[left], nums[right]});

				++left;
				--right;

				while(left < right && nums[left] == nums[left-1])
				{
					++left;
				}
				while(left < right && nums[right] == nums[right+1])
				{
					--right;
				}
 
			} else if(currentSum < target) {
				++left;
			} else {
				--right;
			}
		}
	}
	return triplets;
}


int main() {

	vector<int> nums = {12, 3, 2, -8, -8, 6, 6, 2, -8, 1, 2, -6, -5, -8, 6};
	//vector<int> nums = {12, 3, 1, 2, -6, 5, -8, 6};
	int target = 0;
	vector<vector<int>> res = threeSum(nums, target);
	for(auto &item: res)  {
		for(auto &i: item) {
			cout<<i<<"  ";
		}
		cout<< endl;
	}


	return 0;
}
