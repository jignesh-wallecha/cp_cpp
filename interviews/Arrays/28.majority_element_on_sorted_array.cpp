#include<iostream>
#include<vector>
using namespace std;

/**
* Time Complexity: O(n) & Space Complexity: O(1)
*/
bool isMajorityElementInSortedArray(vector<int>& nums) {

/*
	int i =0;
	while((i<nums.size()) && nums[i] == (nums[i + nums.size()/2])) {
		//i++;
		return true;
		++i;
	}
	return false;
*/	

	if(nums.size() == 1) {
		return true;
	} 

	for(int i=0; i<nums.size()/2 && nums[i] == nums[i+nums.size()/2]; ++i) {
		return true;
	}
	return false;
}


int main() {

	vector<int> nums = {1, 1, 1, 1, 2, 2, 3};
	cout << std::boolalpha;

	bool res = isMajorityElementInSortedArray(nums);
	cout << res << endl;

	nums = {1, 1, 2, 2, 2, 3, 3};
	res = isMajorityElementInSortedArray(nums);
	cout << res << endl;

	nums = {1};
	res = isMajorityElementInSortedArray(nums);
	cout << res << endl;

}