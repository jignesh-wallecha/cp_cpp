#include<iostream>
#include<vector>
#include<unordered_map>

using namespace std;
/**
* Two Pointer Using DP like Approach
**/

void longestSubArrayOfZerosAndOnes(vector<int>& nums) {

	vector<int> ones(nums.size());
	vector<int> zeros(nums.size());
	int countOnes = 0, countZero = 0;

	for(int i=0; i<nums.size(); ++i) {

		if(nums[i] == 1) {
			countOnes++;
			ones[i] = countOnes;
			zeros[i] = countZero;
		} else if(nums[i] == 0) {
			countZero++;
			zeros[i] = countZero;
			ones[i] = countOnes;
		}
	}
	int left = 0, right = nums.size()-1;
	while(left < right) {
		if(countOnes > countZero) {
			int currentOnes = ones[right] - ones[left-1];
			if(currentOnes > currentZeros && currentOnes[left] == 1) {
				++left;
			} else {
				
			}
		}
	}

}
/**
time: O(n) & Space : O(n)
*

int longestSubArrayOfZerosAndOnes(vector<int>& nums) {

	unordered_map<int, int> hashmap;

	for(int i=0; i<nums.size(); ++i) {
		nums[i] = nums[i] == 0 ? -1 : 1;
	}

	hashmap[nums[0]] = 0;

	int sum = nums[0];
	int lb = -1;
	int ub = -1;
	int sizeOfLargestSubarray = 0;

	for(int i=1; i<nums.size(); ++i) {
		sum = sum + nums[i];

		//edge case where the sum results in 0 itself
		if(sum == 0) {
			int tempSize = i;
			if(tempSize > sizeOfLargestSubarray) {
				sizeOfLargestSubarray = tempSize;
				lb = 0;
				ub = i;
			}
		}
		if(hashmap.find(sum) != hashmap.end()) {

			int idx = hashmap[sum];

			if(i - (idx+1) > sizeOfLargestSubarray) {
				lb = idx + 1;
				ub = i;
				sizeOfLargestSubarray = ub - lb;
			}
		} else {
			hashmap[sum] = i;
		}
	}
	return (sizeOfLargestSubarray + 1)/2;
}
*/
int main() {
/*	vector<int> nums = {1, 0, 1, 0};
	int res = longestSubArrayOfZerosAndOnes(nums);
	cout << res << endl;
*/
	vector<int> nums = {1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0};
	longestSubArrayOfZerosAndOnes(nums);
	return 0; 
}
