#include<iostream>
#include<vector>

using namespace std;

/**
* Design a I/P matrix:
* n = 5 m = 5 i.e n = row & m = col
* var i if for row & j is for col
* 		1 2 3 0 1
*		2 3 4 1 0
*		0 1 1 2 3
*		2 3 4 5 6
*		1 2 5 4 3
*
* After Nullify Row for loop My Matrix will be :
*		0 0 0 0 0
*		0 0 0 0 0
*		0 0 0 0 0
*		2 3 4 5 6
*		1 2 5 4 3
* 
* After Nullify Col for loop My Matrix will be :
*		0 0 0 0 0
*		0 0 0 0 0
*		0 0 0 0 0
*		0 3 4 0 0
*		0 2 5 0 0
*				
* Final Output Matrix:
*
*		0 0 0 0 0
*		0 0 0 0 0
*		0 0 0 0 0
*		0 3 4 0 0
*		0 2	5 0 0
*
* Create a method known as 
*	void setZeros(int[][] matrix) {
		//default value of boolean is false in java but in c++ is undefined.
*		bool row[matrix.length]; //no. of rows 
*		bool col[matrix[0].length]; //no. of col
*		//i will store true jabh row mai 0 milega i.e. that index of row 			should be set as Zero.		
*		//and jabh col mai zero milega toh uss col idx ko set krr dehnge
		for(int i=0; i<matrix.size(); ++i) {
			for(int j=0; j<matrix[0].length; ++j) {
				if(matrix[i][j] == 0) {
					row[i] = true;
					col[j] = true;
				}
			}
		}
*		 	
*	}
*
*	So now how col and row array will look
*	row[0...4] = {true, true, true, false, false};
*	col[0...4] = {true, false, false, true, true}; 	
*
*	This row & col array is output of above method i.e. setZeros(int[][] mt);
*	
*	Now, we'll make all rows 0 in matrix jabh bhi row array of bool array mai true milega i.e. row array mai joh bhi true hai n toh uss index/row ko pura zero krr dehna hai of I/P matrix. And similarly do for column
*	
*	So, now i will nullify the rows to 0 which have true in a value
*	this for loop is for bool array of row
*	for(int i=0; i<row.length; ++i) {
		if(row[i]) {
			//creating a method nullifyRow() to set to zero i.e if row[i] == true then set that row to 0
			nullifyRow(matrix, i); //passing the matrix & i(index) which is to be nullify
		}
	}
	//Now we need to nullify cols to 0 which has true in the value
	for(int j=0; j<col.length; ++j) {
		if(col[j]) {
			nullifyCol(matrix, j);
		}
	}

	void nullifyRow(int[][] matrix, int row) {
		//row would be same only j would be changed.
		for(int j = 0; j<matrix[0].length; ++j) {
			matrix[row][j] = 0; 
		}
	} 

	void nullifyCol(int[][] matrix, int col) {
		//col would be same i.e j and row would be changed to make col nullify
		for(int i=0; i<matrix.length; ++i) {
			matrix[i][col] = 0;
		}
	}

	After this Soln  interviewer will ask the complexity ?

	Complexity:
		Time: O(n^m)
		Space: O(n+m)

	Follow Up Question asked by interviewer : 
	Can u reduce the complexity Time/Space ? 
	We cannot reduce Time Complexity because we need touch every element in matrix atleast once. We're touching multiple times but we need onces.
	So Time Complexity cannot be reduced.
	
	So Interviewer will ask can Space Complexity Can be reduced ?
	Space Complexity can be reduced.

	Soln:
		We could have used the first row and the first col to store the status of which row and which column needs to be nullified.

I/P:	1 | 2 3 0 1
		-----------
*		2 | 3 4 1 0
*		0 | 1 1 2 3
*		2 | 3 4 5 6
*		1 | 2 5 4 3
*
*		If any row contains 0 mark that row with 0, that means we have kept 		first row and first col for marking, i.e wo row yaha col zero hai 	      	ki nhi woo decide krke rakhega yaha status wala thing

		0 | 2 3 0 1				0 | 0 0 0 0
		-----------				------------
*		0 | 3 4 1 0				0 | 3 4 1 0
*		0 | 1 1 2 3		->   	0 | 1 1 2 3
*		2 | 3 4 5 6				2 | 3 4 5 6
*		1 | 2 5 4 3				1 | 2 5 4 3
*		
*		Should first row should be make all zeros OR first col should be all zeros

* Complexity: Time: O(n^m) & Space: O(1).
*	
		void setZeros(int[][] matrix) {
			boolean firstRowHasZero = false;
			boolean firstColHasZero = false; 
			//yeh true tabh hoga jabh original array first row & col mai zero hai toh.
			
			//check if the first row has 0 or not
			for(int j = 0; j<matrix[0].length; ++j) {
				if(matrix[0][j] == 0) {
					firstRowHasZero = true;
					break; //breaking the loop because ekk baar zero yaha 5 times, it doesnt matter we're only setting the status that firstRow has zero or not. If it is their make that row status to true and break the loop
				}
			}

			//check if first col has zero or not
			for(int i=0; i<matrix.length; ++i) {
				if(matrix[i][0] == 0) {
					firstColHasZero = true;
					break;
				}
			}
	
			//check for the remaining items which have zero and update the 1st row and col accordingly.

			for(int i=1; i<matrix.length; ++i) {
				for(int j=1; j<matrix[0].length; ++j) {
					//joh bhi row/col ko zero rahega uss sabh se value ko set krr dehnge
					if(matrix[i][j] == 0) {
						matrix[i][0] = 0;
						matrix[0][j] = 0;
					}
				}
			}

			first row pe jaha jaha col mai zero mila hai toh sabh jagha pe zero krr dehna hai and same for first col mai jaha jaha row pe zero mila hai n toh sabh jagha pe zero krr dehna hai

			//Nullify the row based on the value of 1st col
			for(int i=1; i<matrix.length; ++i) {
				if(matrix[i][0] == 0) {
					nullifyRow(matrix, i);
				}
			}

			void nullifyRow(int[][] matrix, int row) {
				//row would be same only j would be changed.
				for(int j = 0; j<matrix[0].length; ++j) {
					matrix[row][j] = 0; 
				}
			} 

			//Nullify the col based on the values of 1st row
			for(int j=1; j<matrix[0].length; ++j) {
				if(matrix[0][j] == 0) {
					nullifyCol(matrix, j);
				}
			}

			void nullifyCol(int[][] matrix, int col) {
				//col would be same i.e j and row would be changed to make col nullify
				for(int i=0; i<matrix.length; ++i) {
					matrix[i][col] = 0;
				}
			}
		^
		| -- //this would not nullify 1st row & 1st col
	
			//Nullify the first row if the flag is set
			if(firstRowHasZero) {
				nullifyRow(matrix, 0);
			}
			//Nullify the 1st col if the flag is set 
			if(firstColHasZero) {
				nullifyCol(matrix, 0);
			}


			 
*
*
*	
*
*
*
*
*
*
*
*
*/
void setZeroes(vector<vector<int>>& matrix) {
        
}

int main() {

	
}