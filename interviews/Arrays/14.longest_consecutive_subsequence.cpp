#include<iostream>
#include<vector>
#include<climits>
#include<unordered_map>

using namespace std;

void longestConsecutiveSubsequence(vector<int>& nums) {

	//pair<int, int> bestRange;

	vector<int> numsWhichContainedInMap = {};

	int longestLength = 0;

	unordered_map<int, bool> numsMap;
	for(int i=0; i<nums.size(); ++i) {
		numsMap.insert(end(numsMap), {nums[i], false});
	}	
	
	// for(auto& num : numsMap) {
	// 	cout << num.first << " -> " << num.second << endl;
	// }

	for(int i=0; i<nums.size(); ++i) {
		int currentNum = nums[i];
		numsWhichContainedInMap.push_back(currentNum);
		//numsMap[numsMap.size()-1] = i;

		//left-side
		while(numsMap.contains(currentNum-1)) {
			numsWhichContainedInMap.push_back(currentNum-1);
			//numsMap[] = true; 
		}

		//right-side 
		while(numsMap.contains(currentNum + 1)) {
			numsWhichContainedInMap.push_back(currentNum+1);
			//numsMap[] = true;
		}
	}	

	


}

int main() {

	vector<int> nums = {1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6};
	longestConsecutiveSubsequence(nums);
}