#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

/**
* Using Counting Array
* Time: O(n) & Space: O(n)
**
vector<int> findAllDisappeared(vector<int>& nums) {
	vector<int> res;

	vector<int> countingArr(nums.size());

	for(int i = 0; i<nums.size(); ++i) {
        countingArr[nums[i]]++;
    }

    for(int i = 1; i<countingArr.size(); ++i) {
    	if(countingArr[i] == 0) {
    		res.push_back(i);
    	}
    }
	return res;
}
*/

/**
* Time: O(n) & Space: O(1)
**/

vector<int> findAllDisappeared(vector<int>& nums) {

	vector<int> res;

	for(int i = 0; i<nums.size(); ++i) {
		while (1 <= nums[i] <= nums.size() and nums[i] != nums[nums[i]-1]) {
			std::swap(nums[i], nums[nums[i]-1]);
		}
	}

	for(int i = 0; i<nums.size(); ++i) {
		if((i+1) != nums[i]) {
			res.push_back(i+1);
		}
	}
	return res;
}

int main() {

	vector<int> nums = {4,3,2,7,8,2,3,1};
	vector<int> res = findAllDisappeared(nums);
	std::for_each(begin(res), end(res), [](const auto& e) { cout << e <<" "; });

	return 0;
}