#include<iostream>
#include<vector>
using namespace std;
/**
* Bruteforce Approach:
* Complexity: Time: O(n^2) & Space: O(1)
*/
/*
int maxArea(vector<int>& height) {


	//breaking condn
	if(height.size() < 2) {
		return -1;
	}

	int i, j;
	int sz = height.size();
	int maxlength = 0;

	for(i=0; i<sz-1; ++i) {
		//int h1 = height[i];
		//calculate for each 'i' check big wall/line from j = i+1, joh 
		//isse big and durr ho, and min ho i.e at 0th index is 1 so min 1 ho
		for(j=i+1; j<sz; ++j) {
			int currentWidth = (j - i) * min(height[i], height[j]);
			maxlength = max(maxlength, currentWidth);
		}	
	}
	return maxlength;
}
*/
/**
*	Two Pointer Approach: 
* Time Complexity: O(n) & Space Complexity: O(1)
*/


int maxArea(vector<int>& height) {

	int left = 0;
	int right = height.size()-1;
	int maxArea = 0;

	while(left < right) {
		int currentArea = (right-left) * min(height[left], height[right]);
		maxArea = max(maxArea, currentArea);
		//maxArea = max(maxArea, (right-left) * min(height[left], height[right]));
		if(height[left] == height[right]) {
			++left;
			++right;
		} else if(height[left] > height[right]) {
			--right;
		} else {
			++left;
		}
		
	}
	return maxArea;
}

int main() {

	vector<int> nums = {1, 8, 6, 2, 5, 4, 8, 3, 7};

	int res;
	res = maxArea(nums);
	cout << "Max Area of Container With Water = " << res << endl;
	// O/P: 49

	nums = {4,3,2,1,4};
	res = maxArea(nums);
	cout << "Max Area of Container With Water = " << res << endl;
	// O/P: 16

	nums = {1, 2, 1};
	res = maxArea(nums);
	cout << "Max Area of Container With Water = " << res << endl;
	// O/P: 2

	nums = {1, 1};
	res = maxArea(nums);
	cout << "Max Area of Container With Water = " << res << endl;
	// O/P: 1

}