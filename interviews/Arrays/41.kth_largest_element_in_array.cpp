#include<iostream>
#include<vector>
using namespace std;

/**
* Kth Largest Element in an Array:

* Given an integer array nums and an integer k, return the kth largest element in the array.

Note that it is the kth largest element in the sorted order, not the kth distinct element.

 

Example 1:

Input: nums = [3,2,1,5,6,4], k = 2
Output: 5
Example 2:

Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
Output: 4
 

Constraints:

1 <= k <= nums.length <= 104
-104 <= nums[i] <= 104

Logic:
Approach1: Intuitive 
Sort the array in ascending order and then get nums[nums.length-k] from array, and we will get kth largest element from array.


Approach 2: Using Max Heap
We can use Data Structure of Max Heaps.

put the array elements in max heap order. 
Then traverse the till k and pop from max heap k times.
And then we will get kth largest element from max heap.

**/

/**
* Space: O(n) for priority queue 
* Time: O(k) 
**/

int findKthLargest(vector<int>& nums, int k) {
    priority_queue<int, vector<int>, std::less<int>> maxHeap;
    for(auto& e : nums) maxHeap.emplace(e);
    int res;
    for(int i = 0; i<k; ++i) {
        res = maxHeap.top();
        maxHeap.pop();
    }
    return res;   
}

int main() {

	vector<int> nums = {3,2,3,1,2,4,5,5,6};
	int k = 4;
	int res = findKthLargest(nums, k);
	cout << res << endl;

	return 0;
}