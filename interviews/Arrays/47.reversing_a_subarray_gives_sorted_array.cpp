#include<iostream>
#include<vector>
#include<algorithm> //for copy
using namespace std;

/**
* Given an array find if array gets sorted by reversing any subarray of this array. Ex: In {1, 2, 3, 4, 8, 7, 6, 9} we can reverse subarray from index 4 to 6.
**/

/**
* Sorting Approach:
* Time: O(nlogn)
* Space: O(n)
**/

pair<int, int> sortSubarray(vector<int>& nums) {

	if(is_sorted(begin(nums), end(nums))) {
		return {-1, -1};
	}

	vector<int> temp(nums.size());
	copy(begin(nums), end(nums), begin(temp));
	sort(begin(temp), end(temp));

	int front;
	for(front = 0; front < nums.size(); ++front) {
		if(nums[front] != temp[front]) 
			break;
	}

	int back;
	for(back = nums.size()-1; back >=0; --back) {
		if(nums[back] != temp[back]) {
			break;
		}
	}

	if(front >= back) {
		return {-1, -1}; 
	}
	// cout << "front: " << front << endl;
	// cout << "back: " << back << endl;

	//checking whether subarray is decreasing or not
	do {
		front++;
		if(nums[front-1] < nums[front]) {
			return {-1, -1};
		}
	} while(front != back);
}

int main() {
	vector<int> nums = {1, 2, 3, 4, 8, 7, 6, 9};
	pair<int, int> res = sortSubarray(nums);
	cout << "Front: " << res.first << endl;
	cout << "Back: " << res.second << endl; 
	return 0;
}
