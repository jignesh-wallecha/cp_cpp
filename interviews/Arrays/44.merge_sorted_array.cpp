#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

/**
 * Time Complexity: O(n)
 * Space Complexity: O(1)
**/
void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
    int lastIdx = m + n - 1;
    int num1_ptr = m - 1;
    int num2_ptr = n - 1;
    while (num1_ptr >= 0 && num2_ptr >= 0) {
           
        if(nums1[num1_ptr] > nums2[num2_ptr]) {
            nums1[lastIdx--] = nums1[num1_ptr--];
        } else {
            nums1[lastIdx--] = nums2[num2_ptr--];
        }
    }
    //if in nums2 arr elements are their then append in nums1 arr
    while(num2_ptr >= 0) {
        nums1[lastIdx--] = nums2[num2_ptr--];
    }
}

void print(const vector<int>& v) {
	for(const auto& e : v) {
		cout << e <<"  ";
	}
	cout << '\n';
}

int main() {

	vector<int> nums1 = {1,2,3,0,0,0};
	vector<int> nums2 = {2,5,6};
	int m, n;
	m = 3, n = 3;

	merge(nums1, m, nums2, n);

	print(nums1);

	nums1 = {1};
	nums2 = {};
	m = 1, n = 0;
	merge(nums1, m, nums2, n);
	print(nums1);

	nums1 = {0};
	nums2 = {1};
	m = 0, n = 1;
	merge(nums1, m, nums2, n);
	print(nums1);

	
	return 0;
}