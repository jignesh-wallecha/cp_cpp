#include<iostream>
#include<vector>

using namespace std;

int partitionPoint(vector<int>& nums) {
	int i=0;
	while(nums[i] != 1) {
		i++;
	}
	return i;
}


int main() {

	vector<int> nums = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1};
	int res = partitionPoint(nums);
	cout << res << endl;
}