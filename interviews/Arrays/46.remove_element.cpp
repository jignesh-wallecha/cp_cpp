#include<iostream>
#include<vector>
using namespace std;

/**
* Remove Element:
* Given an array nums and a value val, remove all instances of that value   	in-place and return the new length.
**/

int removeElement(vector<int>& nums, int val) {
    int low = 0;
    int sz = nums.size();
    while (low < sz) {
        if(nums[low] == val) {
            nums[low] = nums[sz-1];
            sz--;
        } else {
            ++low;
        }
    }
    return sz;
}