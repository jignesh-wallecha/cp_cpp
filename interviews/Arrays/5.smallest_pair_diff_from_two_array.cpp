#include<iostream>
#include<vector>
#include<algorithm>
#include<climits>

using namespace std;

/*
	Question: Given 2 Arrays of integer your job is to find 2 elements say x & y 
			  such that x belongs to 1st array & y belongs to 2nd array AND x & y 
			  will have the smallest difference OR x & y will be the closest 
			  elements. 

	Ex:
		I/P: A1 = [-1, 5, 10, 20, 28, 3];
			 A2 = [26, 134,135, 15, 17];

		O/P: [28, 26]
*/

/**
	Bruteforce: This approach will take O(n^2)
*/


pair<int, int> smallestDifferenceBruteforce(vector<int> array1, vector<int> array2) {

	int i, j, n, m, minDiff, currentDiff; 
	n = array1.size();
	m = array2.size();
	minDiff = INT_MAX;
	currentDiff;
	pair<int, int> smallestPair;
	for(i =0; i<n; ++i) {
		int firstNum = array1[i];
		//cout <<"i ele: "<< firstNum << '\n';
		for(j = 0; j<m; ++j) {
			int secondNum = array2[j];
			currentDiff = std::abs(firstNum - secondNum);

			if(minDiff > currentDiff) {
				minDiff = currentDiff;
				cout << "Min diff: " << minDiff <<endl;
				smallestPair.first = firstNum;
				smallestPair.second = secondNum;
			}
		}
	}
	return smallestPair;
}
/**
pair<int, int> smallestDifference(int array1[], int array2[], int m, int n) {

	if(m == 0 || n == 0)
		return {};

	sort(array1, array1 + m);
	sort(array2, array2 + n);

	int indexOne = 0, indexTwo = 0;
	int smallestDiff = INT_MAX;

	pair<int, int> smallestPair;

	while(indexOne < m && indexTwo < n) {

		int firstNum = array1[indexOne];
		int secondNum = array2[indexTwo];
		int currentDiff;

		if(firstNum < secondNum) {
			indexOne++;
			currentDiff = secondNum - firstNum;
		} else if(firstNum > secondNum) {
			indexTwo++;
			currentDiff = firstNum - secondNum;
		} else {
			//dono first & second num same hai
			smallestPair.first = firstNum;
			smallestPair.second = secondNum;
			return smallestPair; // return {firstNum, secondNum}
		}

		if(smallestDiff > currentDiff) {
			smallestDiff = currentDiff;
			smallestPair.first = firstNum;
			smallestPair.second = secondNum;
		}
	} 
	return smallestPair;
}
*/
int main() {


	pair<int, int> smallestPair;
/*	int a1[] = {-1, 5, 10, 20, 28, 3};
	int a2[] = {26, 134, 135, 15, 17};

	int size1 = sizeof(a1) / sizeof(int);
	int size2 = sizeof(a2) / sizeof(int);

	smallestPair = smallestDifference(a1, a2, size1, size2);
	cout << "Smallest Pair:\n";
	cout << smallestPair.first << endl;
	cout << smallestPair.second << endl;
*/

	vector<int> a1 = {-1, 5, 10, 20, 28, 3};
	vector<int> a2 = {26, 134, 135, 15, 17};
	smallestPair = smallestDifferenceBruteforce(a1, a2);
	cout << "Smallest Pair:\n";
	cout << smallestPair.first << endl;
	cout << smallestPair.second << endl;

	return 0;
}