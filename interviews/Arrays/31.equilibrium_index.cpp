#include<iostream>
#include<vector>
#include<numeric>

using namespace std;
/**
* Bruteforce Approach: Time : O(n^2) & Space: O(1)
*
int equilibriumIndex(vector<int>& nums) {

	int leftSum, rightSum;
	leftSum = rightSum = 0;
	for(int i=0; i<nums.size(); ++i) {
		leftSum += nums[i];
		for(int j=i+1; j<nums.size(); ++j) {
			rightSum += nums[j]; 
		}
		if(leftSum == rightSum) {
			return i;
		} else {
			rightSum = 0;
		}
	}
	return -1;
}
*/

/**
* Intutive Approach: Time: O(n) + O(n) + O(n) = O(3n) = O(n)
*					 Space: O(n) + O(n) = O(2n) = O(n)
*
int equilibriumIndex(vector<int>& nums) {

	vector<int> leftSum(nums.size(), 0);
	vector<int> rightSum(nums.size(), 0);

	//calculate leftSum Array 
	for(int i=0; i<nums.size(); ++i) {
		leftSum[i] = leftSum[i-1] + nums[i];
	}
	//calculate rightSum Array
	for(int i=nums.size()-1; i>=0; --i) {
		rightSum[i] = rightSum[i+1] + nums[i];
	}

	for(int i=0; i<leftSum.size() && i<rightSum.size(); ++i) {
		if(leftSum[i] == rightSum[i+1]) {
			return i;
		}
	}
	return -1;

}
*/
/**
* Calculating leftSum on go and keeping only rightSum array 
* pre-computed
* We have reduced space complexity from O(2n) to O(n) but it is
* same only from above.
*

int equilibriumIndex(vector<int>& nums) {

	vector<int> rightSum(nums.size(), 0);

	//calculate rightSum Array
	for(int i=nums.size()-1; i>=0; --i) {
		rightSum[i] = rightSum[i+1] + nums[i];
	}

	//calculating leftSum on go and returning equilibrium by checking
	//if(leftSum == rightSum[i+1])
	int leftSum = 0;
	for(int i=0; i<nums.size(); ++i) {
		leftSum += nums[i];
		if(leftSum == rightSum[i+1]) {
			return i;
		}
	}
	return -1;
}
*/
int equilibriumIndex(vector<int>& nums) {

	int finalSum = 0;
	finalSum = accumulate(begin(nums), end(nums), 0);
	int leftSum, rightSum;
	leftSum = rightSum = 0;
	for(int i=0; i<nums.size(); ++i) {
		leftSum += nums[i];
		rightSum = finalSum - leftSum;
		if(leftSum == rightSum) {
			return i;
		}
	}
	return -1;
}

/**
* Time: O(n) & Space: O(1).
**

int equilibriumIndex(vector<int>& nums) {
	int sum = 0;
	sum = accumulate(begin(nums), end(nums), 0);

	int leftSum = 0;
	for(int i=0; i<nums.size(); ++i) {
		leftSum += nums[i];
		if(leftSum == (sum-leftSum)) {
			return i;
		}
	}
	return -1;
}
*/
int main() {

	vector<int> nums = {10, 5, 15, 3, 4, 21, 2};
	int res = equilibriumIndex(nums);
	cout << "Equilibrium Index : " <<res;
	cout << '\n';
	//O/P: 2

	nums = {1, 2, 3};
	res = equilibriumIndex(nums);
	cout << "Equilibrium Index: " << res;
	cout << '\n';
	// O/P: 1
	return 0;
}