#include<iostream>
#include<vector>
using namespace std;

int findMissingNumber(vector<int>& nums) {

	for(int i = 0; i<nums.size(); ++i) {
		signed int correctPos = nums[i]-1;

		while (nums[i] >= 1 && nums[i] <= nums.size() && nums[i] != nums[correctPos]) {
			swap(nums[i], nums[correctPos]);
			correctPos = nums[i] - 1; //now nums[i] has changed.
 		}
	}

	//checking any element which is not equal to i+1
	for(int i = 0; i<nums.size(); ++i) {
		if((i+1) != nums[i]) {
			return i+1;
		}
	}

	//nothing is present return last number
	return nums.size();

}

int main() {

/*	vector<int> nums = {3, 4, 7, 1};
	int res = findMissingNumber(nums);
	cout << res << endl;
*/

	vector<int> nums = {4,3,2,7,8,2,3,1};
	int res = findMissingNumber(nums);
	cout << res << endl;

	nums = {2147483647,2147483646,2147483645,3,2,1,-1,0,-2147483648};
	res = findMissingNumber(nums);
	cout << res << endl;
	return 0;
}