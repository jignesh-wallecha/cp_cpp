#include<iostream>
#include<vector>

using namespace std;

void sink(vector<vector<char>> &grid, int row, int col) {
	if(row >=0 && row < grid.size() && col >=0 && col < grid[0].size() && grid[row][col] == '1') {
		grid[row][col] = '0';
		sink(grid, row+1, col);
		sink(grid, row-1, col);
		sink(grid, row, col+1);
		sink(grid, row, col-1);
	}
}

int numIslands(vector<vector<char>>& grid) {
	int counter = 0;
	for(int row = 0; row < grid.size(); ++row) {
		for(int col = 0; col < grid[0].size(); ++col) {
			if(grid[row][col] == '1') {
				++counter;
				sink(grid, row, col);
			}
		}
	}
	return counter;
}

int main() {

	vector<vector<char>> grid = {
		{"1", "1", "0", "0", "0"},
		{"1", "1", "0", "1", "0"},
		{"0", "0", "1", "0", "0"},
		{"0", "0", "0", "1", "1"}
	};

	int res = numIslands(grid);
	cout << "res: " << res << endl;

	return 0;
}
