#include<iostream>
#include "treenode.hpp"

using namespace std;

//iterative Approach:
Node::TreeNode* lowestCommonAncestor(Node::TreeNode* root, Node::TreeNode* p, Node::TreeNode* q) {
    int qVal = q->val;
    int pVal = p->val;

    Node::TreeNode* currentNode = root;
    while(currentNode) {
        int currentVal = currentNode->val;
        if(currentVal < pVal && currentVal < qVal) {
            currentNode = currentNode->right;
        } else if(currentVal > pVal && currentVal > qVal) {
            currentNode = currentNode->left;
        } else {
            return currentNode;
        }
    }
    return nullptr;
}

/*
* recursive
*/
Node::TreeNode* lowestCommonAncestor(Node::TreeNode* root, Node::TreeNode* p, Node::TreeNode* q) {
    int qVal = q->val;
    int pVal = p->val;
    int rootVal = root->val;
    if(rootVal < pVal && rootVal < qVal) {
        //go right
        return lowestCommonAncestor(root->right, p, q);
    } else if(rootVal > pVal && rootVal > qVal) {
        //go left
        return lowestCommonAncestor(root->left, p, q);
    } else {
        return root;
    } 
}

int main() {

	Node::TreeNode node;
	Node::TreeNode *root = node.createTree();
	//node.inorder(root);
	int p, q;
	cout << "Enter p node:\n";
	cin >> p;
	Node::TreeNode* pnode = node.search(root, p);
	cout << "Enter q node:\n";
	cin >> q;
	Node::TreeNode* qnode = node.search(root, q);

	Node::TreeNode* lca = lowestCommonAncestor(root, pnode, qnode);
	cout << "lca is: " << lca->val << endl;

    return 0;
}