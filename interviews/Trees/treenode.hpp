#ifndef TREENODE_H
#define TREENODE_H
#include<iostream>
using namespace std;

namespace Node {

	struct TreeNode {

		int val;
		TreeNode *left;
		TreeNode *right;
		TreeNode *pnode = nullptr;
	public:

		TreeNode() : val(0) {}
		
		TreeNode(int val) {
			this->val = val;
			left = nullptr;
			right = nullptr;
		}
		
		~TreeNode() {
			delete pnode;
		}

		void inorder(TreeNode *t) {
			if(!t) return;
			inorder(t->left);
			cout << t->val <<"  ";
			inorder(t->right);
		}	

		TreeNode* insertValue(TreeNode* root, int value) { 
			pnode = new TreeNode(value);
			if(root == nullptr) {
				root = pnode;
				return root;
			}

			if(pnode->val < root->val) {
				//go left
				root->left = insertValue(root->left, pnode->val);
			} else if(pnode->val > root->val) {
				//go right
				root->right = insertValue(root->right, pnode->val);
			} else {
				return root;
			}

			return root;
		}

		TreeNode* createTree() {
			TreeNode* root = nullptr;
			int n, num;
			cout << "Enter the num of node" << endl;
			cin >> n;
			cout << "Enter one by one:" << endl;
			for(int i =0; i<n; ++i) {
				cin >> num;
				root = insertValue(root, num);
			}
			return root;
		}

		TreeNode* search(TreeNode* root, int k) {
			if(!root) return nullptr;
			else if(root->val == k) return root;
			return k < root->val ? search(root->left, k) : 
							       search(root->right, k);
		}

	};
}

#endif
