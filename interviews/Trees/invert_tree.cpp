//mirror tree
#include "treenode.hpp"


Node::TreeNode* invertTree(Node::TreeNode* root) {
	if(!root) {
		return nullptr;
	}

	Node::TreeNode* right = invertTree(root->right);
	Node::TreeNode* left = invertTree(root->left);
	root->left = right;
	root->right = left;
	return root;
}

int main() {

	Node::TreeNode node;
	Node::TreeNode *root = node.createTree();
	node.inorder(root);

	invertTree(root);
}