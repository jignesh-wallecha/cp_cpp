#include<iostream>
using namespace std;

/**
* Maximum Depth of Binary Tree:
* Given a root of binary tree, return its maximum depth.

* A binary tree's maximum depth is the number of nodes along the longest path 
* from the root node to the farthest leaf node.

**/

//iterative approach:
int maxDepth(TreeNode* root) {
    if(!root) return 0;
        
    vector<pair<int, TreeNode*>> my_stack;
    my_stack.push_back(pair<int, TreeNode*>(1, root));
    int max_depth = 0;
    
    while(!my_stack.empty()) {
        pair<int, TreeNode*> my_pair = my_stack.back();
        int curr_depth = my_pair.first;
        TreeNode* curr_node = my_pair.second;
        max_depth = std::max(max_depth, curr_depth);
        my_stack.pop_back();

        if(curr_node->left) {
            my_stack.push_back(pair<int, TreeNode*>(curr_depth+1, curr_node->left));
        }
            
        if(curr_node->right) {
            my_stack.push_back(pair<int, TreeNode*>(curr_depth+1, curr_node->right));
        }
    }
    return max_depth;
}