#include<iostream>
using namespace std;

struct TreeNode {
	int val;
	TreeNode* left;
	TreeNode* right;
};

TreeNode* createNode(int val) {
	TreeNode* pnode = new TreeNode();
	if(!pnode) return nullptr;
	pnode->val = val;
	pnode->left = pnode->right = nullptr;
	return pnode;
}

int closestValue(TreeNode* root, float& target) {
	int val, closest = root->val;
	while(root) {
		val = root->val;
		closest = std::abs(val - target) < std::abs(closest - target) ? val : closest;
		root = target < val ? root->left : root->right;
	}
	return closest;
}


int main() {

	float target = 3.714286;
	TreeNode* root = createNode(4);
	root->left = createNode(2);
	root->right = createNode(5);
	root->left->left = createNode(1);
	root->left->right = createNode(3);

	int res = closestValue(root, target);
	cout << res << endl;
}
