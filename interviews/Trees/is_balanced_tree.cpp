#include "treenode.hpp"

using namespace std;

int heightOfTree(Node::TreeNode* root) {
	if(!root) return 0;
	if(!root->left && !root->right) return 0;

	return 1 + std::max(heightOfTree(root->left), heightOfTree(root->right));
}

bool isBalanced(TreeNode* root) {
	if(!root) return true;
	if(!root->left || !root->right) return true;

	
}
int main() {

	Node::TreeNode pnode;
	Node::TreeNode* root = pnode.createTree();
	cout << "height: "  << heightOfTree(root) << endl;
	return 0;
}