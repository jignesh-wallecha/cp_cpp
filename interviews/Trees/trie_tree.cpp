#include<unordered_map>
#include<string>

using namespace std;

struct TrieNode {
public:
    bool isEnd;
    TrieNode() {
        isEnd = false;
    }
    unordered_map<char, TrieNode*> leaves;
};

class Trie {
private:
    TrieNode* root;   
    
    TrieNode* childSearch(string word) {
        auto* current = root;
         for(const auto& c : word) {
            if(current->leaves.count(c))
                current = current->leaves[c]; 
            else 
                return nullptr;
        }
        return current;
    }
    
public:
    
    /** Initialize your data structure here. */
    Trie() {
        root = new TrieNode();
    }
    
    /** Inserts a word into the trie. */
    void insert(string word) {
        auto* current = root;
        for(const auto& c : word) {
            if(!current->leaves.count(c)) {
                current->leaves[c] = new TrieNode();
            }
            current = current->leaves[c];
        }
        current->isEnd = true;
    }
    
    /** Returns if the word is in the trie. */
    bool search(string word) {
        auto* node = childSearch(word);
        if(node) {
            return node->isEnd;
        }
        return false;
    }
    
    /** Returns if there is any word in the trie that starts with the given prefix. */
    bool startsWith(string prefix) {
        return childSearch(prefix);
    }
};

int main()
{
    Trie trie;
    trie.insert("apple");
    trie.search("apple");   // returns true
    trie.search("app");     // returns false
    trie.startsWith("app"); // returns true
    trie.insert("app");   
    trie.search("app");     // returns true
    return 0;
}