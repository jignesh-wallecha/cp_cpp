#include<iostream>
#include<vector>
#include "treenode.hpp"

using namespace std;

vector<vector<int>> levels;

void helper(Node::TreeNode *root, int level) {
	if(levels.size() == level) {
		vector<int> l = {};
		levels.push_back(l);
	}

	levels[level].push_back(root->val);

	if(root->left) {
		helper(root->left, level+1);
	}

	if(root->right) {
		helper(root->right, level+1);
	}
}

vector<vector<int>> levelOrder(Node::TreeNode* root) {
	if(!root) return levels;
	helper(root, 0);
	//std::reverse(begin(levels), end(levels)) //for levelOrderBottom problem
	return levels;
}

int main() {

	Node::TreeNode treeNode;
	Node::TreeNode *root = treeNode.createTree();
	treeNode.inorder(root);
	cout << '\n';
	vector<vector<int>> levels = levelOrder(root);

	for(const auto& level : levels) {
		cout <<"[";
		for(const auto& e : level) {
			cout << e <<" ";
		}
		cout << " ]\n";
 	}



}
