#include<iostream>
#include<vector>
#include "treenode.hpp"

using namespace std;
	
vector<int> data;

void inorder(Node::TreeNode* root) {
	if(root == nullptr) return;
	inorder(root->left);
	data.push_back(root->val);
	inorder(root->right);
}

int kthSmallestEle(Node::TreeNode* root, int k) {
	inorder(root);
	return data[k-1];
}

int main() {

	Node::TreeNode node;
	Node::TreeNode* root = node.createTree();
	int k;
	cout << "Enter kth int:\n";
	cin >> k;
	int res = kthSmallestEle(root, k);
	cout <<"kth smallest ele is: "<< res << endl;
}