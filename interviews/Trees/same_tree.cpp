#include<iostream>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
};

TreeNode* createNode(int val) {
	TreeNode *pnode = new TreeNode();
	pnode->val = val;
	pnode->left = pnode->right = nullptr;
	return pnode;
}

/**
* Time Complexity: O(n): where n is a num of nodes in the tree, since 
*						 one visits each node exactly once.
* 
* Space: O(lg n): in the best case of completely balanced tree & O(n) 
*		in worst case of complexity unbalanced tree, to keep recursion stack. 
**/
    

bool isSameTree(TreeNode* p, TreeNode *q) {
	if(!p && !q) return true;
        
    return p && q && p->val == q->val && 
        isSameTree(p->left, q->left) &&
        isSameTree(p->right, q->right);
}

int main() {

	TreeNode* p = createNode(1);
	p->left = createNode(2);
	p->right = createNode(3);

	TreeNode* q = createNode(1);
	q->left = createNode(2);
	q->right = createNode(3);

	cout << std::boolalpha;
	bool res = isSameTree(p, q);
	cout << res << endl;
	//true

	p = createNode(1);
	p->left = createNode(2);
	p->right = createNode(1);

	q = createNode(1);
	q->left = createNode(1);
	q->right = createNode(2);

	res = isSameTree(p, q);
	cout << res << endl;
	//false	
}

