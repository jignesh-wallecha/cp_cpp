#include<iostream>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
};

TreeNode* createNode(int val) {
    TreeNode *pnode = new TreeNode();
    pnode->val = val;
    pnode->left = pnode->right = nullptr;
    return pnode;
}


bool isSameTree(TreeNode* p, TreeNode *q) {
    if(!p && !q) return true;
    else if(!p || !q) return false;
    return p->val == q->val && 
        isSameTree(p->left, q->left) &&
        isSameTree(p->right, q->right);
}
    
bool preorder(TreeNode* s, TreeNode* t) {
    return s && (isSameTree(s, t)) ||
        preorder(s->left, t) ||
        preorder(s->right, t);
}
    
bool isSubtree(TreeNode* s, TreeNode* t) {
   return preorder(s, t);
}

int main() {

    string demo = "Demo";
    for(auto& c: demo){
        cout << (int)c <<"  ";
    }
    cout << '\n';

    TreeNode* s = createNode(3);
    s->left = createNode(4);
    s->right = createNode(5);
    s->left->left = createNode(1);
    s->left->right = createNode(2);
    s->left->right->left = createNode(0);

    TreeNode* t = createNode(4);
    t->left = createNode(1);
    t->right = createNode(2);

    cout << std::boolalpha;
    bool res = isSubtree(s, t);
    cout << res << endl;
    //false


}