#include<iostream>
#include<vector>

using namespace std;

template <typename T>
struct Queue {
	vector<T> v;
public:
	Queue() = default;

	int sizeOfQueue() {
		return v.size();
	}

	bool isEmpty() {
		return sizeOfQueue() == 0 ? true : false;
	}
	void push(T& x) {
		v.insert(v.begin(), x);
	}

	void pop() {
		if(isEmpty()) {
			return;
		} 
		v.erase(v.begin());
	}

	void display() {
		if(isEmpty()) {
			return;
		}
		for(auto& t : *this) {
			cout << t <<"  ";
		}
		cout << '\n';
	}

	void pop_Priority() {
		if(isEmpty()) return;
		auto idx = v.begin();
		auto *large = v.begin();
		for(auto i = v.begin()+1; i != v.end(); ++i) {
			if(*large < v[i]) {
				*large = v[i];
				idx = i;  
			}
		}
		cout << *large << endl;
	}


}

int main() {

	Queue<int> q;
	q.push(10);
	q.push(20);
	q.push(30);
	q.push(40);

	q.display();
}