#include<iostream>
#include<cstring>
#include<algorithm>

using namespace std;

/**
* Question: Palindrome Permutation (face-to-face)
* Given a string, write a function to check if it is a permutation of a 
  palindrome.
* A palindrome is a word or phrase that is same forwards & backwards.
* A permutation is re-arrangement of letters.
* Note: Palindrome does not need to be limited to just dictionary words.

Ex1: 
I/P: "tact coa"
O/P: true
Expln: Permutations are, atco cta, etc. which are palindrome.

LOGIC:

We're not considering spaces.

Agar face-to-face yeh question hai toh we need to ask interviewer 1. it is 
only small letters, 2. yaha capital letters 3. Or both is it.

Note: that we need to permute the first half, then the second half must follow
the reversed order of the first half. Also, for a given string the exact characters which will form each half is fixed & we need to find the num of permutations of the characters forming the first half. 

Here're some ex, 
String "ABAG" cannot become palindrome so answer is 0/false.
String "ABBNAAA" contains 4'A's, 2'B's, 1'N'. This means that N is the middle and each half contains 2'A's & 1'B'. And since that makes 3!/(2!*1!), there're 3 permutations of "ABBNAAA" which are palindromes. And a last one, string "ABBA"
contains 2'A's & 2'B's, meaning that each half contains 1'A' & 1'B' which gives 
2!/(1!*1!) = 2 permutations.


Bruteforce Approach:
If constraints, says that string is guaranteed to be shorter than 11 characters 
long then we should defintely do the "bruteforce" soln, try every permutation &
check if it's palindrome or not. 

Once you are thinking this way, here is an example to get you thinking about it. What do the following strings have in common? abbacac xyyxzxz
They share a similar "pattern" in terms of their characters. The first string has 3 a's, 2 b's and 2 c's. Similarly the second string has 3 x's, 2 y's and 2 z's. Intuitively, these will have the same answer (number of palindromic permutations). Also, the original order doesn't matter (since you will be permuting them.) So the "counts" of the characters are important -- how many of each character appear in the string.
Secondly, a palindrome is special because it is "symmetric" (by definition). Using the "abbacac" example above, can you think of examples of palindromes from this string?
For example: "abcacba" is such a palindrome. You will notice that it has 1 "a" in the middle, and then the remainder of the letters appear around the middle "a" in a mirrored fashion. In fact, ALL palindromes of abbacac have this property:
acbabca
bacacab
bcaaacb
etc.
Try more examples yourself.
Anyway, the point is, since there are an odd number of a's, that odd "a" has to be in the middle. For everything else, there is (and must be) an even number, and half of them appear on the right, and half of them appear on the left.
So, in the above example, we must have 1 a, 1 b, and 1 c appear on the left, and similarly on the right (of the middle a). The order doesn't matter. You can choose any order of them on the left. Once you know the order of the letters on the left, the remaining letters on the right have only one choice (they must appear as the mirror image of the left side).
This holds true in general:
Start by counting how many times each letter appears.
All letters must occur an even number of times (except possibly one letter, which may be odd)
If there is an odd-occurring letter, one occurrence of it must be in the middle
Of the remaining characters, half of them must appear on the left, half on the right.
You can permute the characters in any way on the left, and then there is a single choice of permutation on the right, so that they appear as a mirror image.
So, the number of answers is precisely the number of ways you can permute the left side. This is a classic permutation problem (I think it's called the "multinomial coefficient" or something fancy).


Approach 1: Hashmap of array 

**/

bool isEven(string& s) {
	return s.length() % 2 == 0 ? true : false;
}

bool isPalindromePermutation(string& s) {

	int hashmap[26];

	memset(hashmap, 0, sizeof(hashmap));

	for(const auto& c : s) {
		++hashmap[c-'a'];
	}
	int n = sizeof(hashmap) / sizeof(int);
	//we'll scan this hashmap array and it should have max 1 odd in it, then 
	//we'll set a flag if again we'll again get odd then return false,
	//otherwise after scanning whole array .
	int count_odd = 0; 
	for(int i=0; i<n; ++i) {
		if(hashmap[i] % 2 != 0) {
			count_odd++;
		}
	}

	if(isEven(s)) {
		return count_odd > 1 ? false : true;
	}
	if(count_odd != 1) {
		return false;
	}
	return true;
}

int main() {

	cout << std::boolalpha <<endl;
	string s = "tact coa";
	bool res = isPalindromePermutation(s);
	cout << res << endl;

	s = "abbnaaa";
	res = isPalindromePermutation(s);
	cout << res << endl;

	s = "abba";
	res = isPalindromePermutation(s);
	cout << res << endl;

	s = "abbacac xyyxzxz";
	res = isPalindromePermutation(s);
	cout << res << endl;
	return 0;
}