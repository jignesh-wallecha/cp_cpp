#include<iostream>
#include<vector>
#include<string>
#include<stdio.h>

using namespace std;

/**
* Question: Encode and Decode String: (face-to-face)

* Design an algorithm to encode a list of strings to string.
* The encoded string is sent over the network and is decoded back to the original list of strings:

* Machine 1 (Sender) has the function:
	string encode(vector<string> strs) {
	
	}

* Machine 2 (Receiver) has the function:
	vector<string> decode(string s) {
	
	}

LOGIC:

** 
*/
string encodeString(vector<string> toBeEncodedStrings )
{
	string encodedString = "";

	for( string n : toBeEncodedStrings)
	{
		encodedString += to_string(n.size()) +".";
	}

	encodedString += ".";

	for( string n : toBeEncodedStrings)
	{
		encodedString += n;
	}

	return encodedString;
}

int main() {

	vector<string> vector_of_strings = {"Study", "Link", "The", "Best", "is", "next"}; 

	string encoded_string = encodeString(vector_of_strings);

	cout << encoded_string << endl;

	return 0;
}