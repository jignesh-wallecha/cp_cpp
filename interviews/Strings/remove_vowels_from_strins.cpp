#include<iostream>
#include<string>
#include<numeric> //for accumulate
#include<unordered_set>

using namespace std;

/**
* Question: Remove Vowels From a String

* Given a string s, remove the vowels 'a', 'e', 'i', 'o', & 'u' from it, and 
	return the new string.

Ex1: 
I/P: "datastructureisfunny"
O/P: "dtstrctrsfnny"

Ex2:
I/P: "aeiou"
O/P: ""

**/

string removeVowels(string s) {

	return accumulate(begin(s), end(s), string(), [](const auto& a, const auto& b) {
		unordered_set<char> lookup = {'a', 'e', 'i', 'o', 'u'};
		if(!lookup.count(b)) {
			return a + b;
		}
		return a;
	});
}

int main() {

	string s = "datastructureisfunny";
	string removedVowelsString = removeVowels(s);
	cout << removedVowelsString << endl;

	s = "aeiou";
	removedVowelsString = removeVowels(s);
	cout << removedVowelsString << endl;

	s = "justrythiswithyourgame";
	removedVowelsString = removeVowels(s);
	cout << removedVowelsString << endl;
}