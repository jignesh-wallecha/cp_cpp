#include<iostream>
#include<string>
#include<stdio.h>

using namespace std;

string caserCipher(string str, int key) {
	key = key % 26;

	for(int i=0; i<str.length(); ++i) {
		if(str[i] >= 'a' && str[i] <= 'z') {
			str[i] = ((str[i] - 'a' + key) % 26) + 'a';
		} else if(str[i] >= 'A' && str[i] <= 'Z') {
			str[i] = ((str[i] - 'A' + key) % 26) + 'A';	
		}
	}
	return str;
}

int main() {

	string str = "There's-a-starman-waiting-in-the-sky";
	int key = 3;

	string result = caserCipher(str, key);

	cout << result << endl;

	char j = 'j';
	int check = (j - 'a' + 3 % 26) + 'a';
	cout << check << endl;
	return 0;
}