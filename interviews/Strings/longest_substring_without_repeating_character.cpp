#include<iostream>
#include<string>
#include<unordered_map>

using namespace std;

/**
* Question: Longest Substring without repeating character:
			Given a string 's', find the length of the longest substring without repeating characters.

	Ex1: I/P: "abc abc abb"
		 O/P: 3
		 Expln: The answer is 'abc' with the length of 3

	Ex2: I/P: "bbbb"
		 O/P: 1 	Explanation: The answer is "b", with the length of 1.

	Ex3: I/P: "pwwkew"
		 O/P: 3		The answer is "wke", with the length of 3.
					Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

	
LOGIC:
	
	We'll figure out that we need to get all substrings from string. Then we'll figure out ki length kiska max hai and it does not contains duplicate elements.
	So Complexity is O(n^2)-for all substrings * O(n) to get max length of all substring without duplicate elements. So final Complixity is O(n^3).
	
	We need to reduce!!!
	
	s stands for start & e stands for end
			
			  0 1 2 3 4 5 6 7 8 9 10 11	12	13	14  15  16  17 18 19 20 21	
	String = "C o m p e t i t i v e   P  r 	o 	g 	r 	a 	m  m  i  n  g"
			  ^ 			^
			  s				e

	s is pointing to start, and increment 'e' till u get unique characters, and stop 'e' where u get duplicate character.

	so from above 'e' ptr is at 't' character which is repeating character 't' at 5th idx. So we'll save "competi" substring.

			  0 1 2 3 4 5 6 7 8 9 10 11	12	13	14  15  16  17 18 19 20 21	
	String = "C o m p e t i t i v e   P  r 	o 	g 	r 	a 	m  m  i  n  g"
			 			  ^ ^
			  			  s e

	As, 't' character was repeating, so we'll go back, and start new ptr from which we can get longest substring with duplicate characters. So we'll start the ptr from 6th idx i.e. from 'i' character, so directly jump the start ptr to 6th idx where previous character was matched('t') + 1.

	So this approach sliding window approach.

			  0 1 2 3 4 5 6 7 8 9 10 11	12	13	14  15  16  17 18 19 20 21	
	String = "[C] o m p e t i t i v e   P  r 	o 	g 	r 	a 	m  m  i  n  g"
			   ^
	This is current window issme koi repeatetive character nhi hai.
	Using hashmap and insert current window ele in hashmap and position where it found.So has for current Window the hashmap contains/has C as key and value as 0th idx as position where it found. And then increase the window till we don't find the repeative elment/or it contains in hashmap.

			  0  1  2 3 4 5 6 7 8 9 10 11	12	13	14  15  16  17 18 19 20 21	
	String = "[C o] m p e t i t i v e   P  r 	o 	g 	r 	a 	m  m  i  n  g"	
			   ^
			   s

	As in current Window = [C o] is their. So check 'o' is in hashmap. If no put 'o' character in hashmap. And increase/grow window.

	[C], [C o], [C o m], [C o m p], [C o m p t], [C o m p t i], [C o m p t i t]

	currentMaxStr = [C o m p t i]   &   currentString = [C o m p t i t] (This has   														repeartetive character , so this is not currentString).

	Step:(from above)
	1. find 't' in hashmap, so 't' is at 5th position, so directly move 's' ptr(start) / left bracket to found position + 1. i.e at 6idx.

	  0  1  2 3 4 5 6 7 8 9 10 11	12	13	14  15  16  17 18 19 20 21	
	String = "C o m p e t [i t] i v e   P  r 	o 	g 	r 	a 	m  m  i  n  g"	
			   			   ^
			     		   s
	
	2. So now, currentString would be currentString = "it" & currentMaxStr = "Competi".

	3. Now match length of currentString & currentMaxStr. So from above thing currentStr = 2 & currentMaxStr = 7. So we'll not update currentMaxStr.

	4. So now current window is [i t] so now we would go further.

	[i t], [i t i].

	Same 'i' character is found at hashmap. So left/start will increment where i was found + 1.
		
		 	  0 1 2 3 4 5 6  7 8  9 10 11	12	13	14  15  16  17 18 19 20 21	
	String = "C o m p e t i [t i v e]   P  r 	o 	g 	r 	a 	m  m  i  n  g"	
			   			     ^
			     		     s

	Now currenStr = "ti"[2] & currentMaxStr = "Computi"[7] the currentMaxStr will not be updated.

	[t i], [t i v], [t i v e]
	
	As 'e' contains in  hashmap already which is at 4th idx/posn so now we cannot take left/start ptr back to 't' i.e is at 5th idx, because repeatetive characters the isliye we take the left ptr to found idx + 1. So we cannot move left back.

	There're two phases: Growing & Shrinking Phase.
	Growing - right ptr/bracket will increase
	Shrinking - left ptr/bracket will increse not decrease.

	So in above case left ptr was decreasing.

	So we'll use max(currentLeft, newLeft).


		 	  0 1 2 3 4 5 6  7 8 9 10 11 12	13 14  15  16  17  18 19 20 21 22
	String = "C o m p e t i [t i v e  t]  P  r 	o 	g 	r 	a 	m  m  i  n  g"	
			   			     ^
			     		     s

	so now right ptr is at 't' character which already contains in hashmap. So where should left Ptr should go it should be their only where it is or go to 6th idx i.e. at 'i' character or 8th idx i.e 'i' character. So left ptr  should go at 8th idx.


		 	  0 1 2 3 4 5 6 7 8 9 10 11 12	13 14  15  16  17  18 19 20 21 22
	String = "C o m p e t i t [i v e  t]  P  r 	o 	g 	r 	a 	m  m  i  n  g"	
			   			       ^
			     		       s
	
	This is achieve when we found 2nd 't' character while checking where we found that 1st 't' is at 5th idx, at that point we should update 't' character position/index with new index. i.e now in hashmap 't' character has new position which is 7th idx. So when next time we find 't' character again in string, so it will get 7th index and left ptr will go to 7 + 1 = 8th position/idx.

	
HashMap
  k  V
[ C  0 ] [ i  6 ]
[ o  1 ] [ v  9 ]
[ m  2 ] [      ]
[ p  3 ]
[ e  4 ]
[ t  5/7 ]
	

	New String = "a  b 	a  c  b  d"

	1.[a] 2.[a b] 3.[a b a] 4.[b a] 5.[b a c] 6.[b a c b] 7.[a c b] 8.[ a c b d].

	So final longest substring without repreating character is [a c b d].
	
Note: 	Space Complexity is O(1) because, Agar Input String 10000 characters ka 		bhi rahega toh hashmap mai 10000 characters ka nhi jayega woo sirf 26 			characters tak lehga. toh agar hashmap 26 characters lehga toh O(n) nhi 		rahega woo sir O(1) rahega.
*/	

/**
* Complexity: 
* Time: O(n)
* Space: O(1)
*/
int lengthOfLongestSubstring(string str) {

	if(str.length() == 0) {
		return str.length();
	}

	unordered_map<char, int> lookup; //woo character konse last idx/posn pe mila.
	int currentMax = 0;
	for(int left = 0, right = 0; right < str.length(); ++right) {
		//we'll insert new character at every iteration, so right will grow.
		//right would be newly inserted character.

		if(lookup.count(str[right])) {
			int lastOccurenceIdx = lookup[str[right]];
			//now left would be modified
			left = max(left, lastOccurenceIdx+1);
		}
		lookup[str[right]] = right;
		currentMax = max(currentMax, right - left + 1);
	}
	return currentMax;
}

int main() {

	string str = "abcabcbb";
	int res = lengthOfLongestSubstring(str);
	cout << res << endl;
	// O/P: 3
	str = "bbbbb";
	res = lengthOfLongestSubstring(str);
	cout << res << endl;
	// O/P: 1 
	str = "pwwkew";
	res = lengthOfLongestSubstring(str);
	cout << res << endl;
	// O/P: 3
	str = "";
	res = lengthOfLongestSubstring(str);
	cout << res << endl;
	// O/P: 0

	str = "competitiveprogramming";
	res = lengthOfLongestSubstring(str);
	cout << res << endl;
	// O/P: 8

	return 0;
}
