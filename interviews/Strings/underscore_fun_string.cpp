#include<iostream>
#include<vector>
#include<numeric>
#include<string>

using namespace std;

/**
* Question: 
* Create A Program, that takes 2 Strings; one of the main string and let the 		other be named as "fun string".
* Your task is to return a version of the main string with every instance of "fun string" in it wrapped between underscores.
* If two instances of the fun string 
**/
vector<vector<int>> getIndexes(string str, string subStr) {

	vector<vector<int>> indexes();
	int startIdx = 0;
	while(startIdx < str.length()) {
		int nextIdx = str.find(subStr, startIdx) {
			if(nextIdx != -1) {
				indexes.push_back(vector<int>(nextIdx, nextIdx + subStr.length()));
				startIdx = nextIdx + 1;
			} else {
				break;
			}
		}
	}
	return indexes;
}

vector<vector<int>> reduce(vector<vector<int>> indexes) {

	if(indexes.empty()) {
		return indexes;
	}
	vector<vector<int>> newIndexes(indexes[0]);
	vector<int> *previous = &newIndexes[0];

	for(int i=1; i<indexes.size(); ++i) {
		vector<int> *current = &indexes[i];

		if(current->at(0) <= previous=>at(1)) {
			previous->at(1) = current->at(1);
		} else {
			newIndexes.push_back(*current);
			previous = &newIndexes(newIndexes.size()-1);
		}
	}
	return newIndexes;
}

string under_scorify(string str, vector<vector<int>> indexes) {

	int idx = 0;
	int strIdx = 0;
	vector<string> finalChars();

	int toggleIdx = 0;

	while(strIdx < str.length() && idx < indexes.size()) {

		if(strIdx == indexes[idx][toggleIdx]) {
			finalChars.push_back("_");
			if(toggleIdx == 1) {
				idx++;
			}
			toggleIdx = toggleIdx == 1 ? 0 : 1;
		}
		string s(1, str[strIdx]);
		finalChars.push_back(s);
		strIdx++;
	}
	if(idx < indexes.size()) {
		finalChars.push_back("_");
	} else if(strIdx < str.length()){
		finalChars.push_back(str.substr(strIdx));
	}

	return accumulate(finalChars.begin(), finalChars.end(), string());
}

string under_score_Fun_String(string str, string subStr) {
	vector<vector<int>> indexes = reduce(getIndexes(str, subStr));
	return under_scorify(str, indexes);
}

int main() {

	string str = "testthis is a testtest to see testtesttest works fine with testtest test or not";
	string subStr = "test";

	string res = under_score_Fun_String(str, subStr);
	cout << str << endl;
	cout << res << endl;

	return 0;
}