#include<iostream>
#include<string>
using namespace std;

/**
* Question: Reverse Reverse: Reverse Words in a String

* Give an algorithm for reversing words in sentence.

Given an input string s, reverse the order of the words.

A word is defined as a sequence of non-space characters. The words in s will be separated by at least one space.

Return a string of the words in reverse order concatenated by a single space.

Note that s may contain leading or trailing spaces or multiple spaces between two words. The returned string should only have a single space separating the words. Do not include any extra spaces.

Example 1:

Input: s = "the sky is blue"
Output: "blue is sky the"

Example 2:

I/P: s = "This is so simple to implement"
O/P: "implement to simple so is This"

Example 2:

Input: s = "  hello world  "
Output: "world hello"
Explanation: Your reversed string should not contain leading or trailing spaces.

Example 3:

Input: s = "a good   example"
Output: "example good a"
Explanation: You need to reduce multiple spaces between two words to a single space in the reversed string.

LOGIC:

**/

//endIdx is exlusive
string reverseString(string &s, int startIdx, int endIdx) {
	string reverseString;
	for(int i=endIdx; i>=startIdx; --i) {
		reverseString += s[i];
	}
	return reverseString;
}

/**
* Space Complexity: O(n)
* Time Complexity: O(n);
**/
string reverseWords(string str) {
	
	string new_rev_str = reverseString(str, 0, str.length()-1);
	for(int i=0; i<new_rev_str.length(); ++i) {
		auto idx = s.find("\\s", i);
	}
}

int main() {

	string str = "This is so simple to implement";
	string res = reverseWords(str);
	cout << res << endl;

	return 0;
}