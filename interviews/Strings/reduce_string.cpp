#include<iostream>
#include<string>
using namespace std;

/**
* Question: Reduce String
* Given a string, write an algorithm for recursively removing the adjacent 
  characters, if they are same.

  EX1:
  I/P: "ABCCBCBA";
  O/P: "ACBA"
  Expln: ABCCBCBA -> ABBCBA -> ACBA

  LOGIC: 

  The Question is for removing recursively, not one time only.
		0	1	2	3	4	5	6	7
  Str = A 	B	C 	C 	B 	C 	B 	A
  	    ^	^
  	    j	i

  Their should be adjacent comparision between two characters. So from above
  j & i ptr is kept on characters for adjacent comparisions.

  if s[j] != s[i] then both i & j ptr would be moved. As we're doing in fly. 
  And x[j] is not equal to x[i] so we'll paste s[i] their only and then move
  forward. As last written character in string array was s[i] character where 
  we paste something which was not an adjacent characters. 

		0	1	2	3	4	5	6	7
  Str = A 	B	C 	C 	B 	C 	B 	A
  	    		^	^
  	    		j	i  
  Now, s[j] == s[i] (their are same adjacent characters), so we'll remove these
  both characters from string array. And j ptr will be decremented and i ptr 
  will be incremented.
		0	1	2	3	4	5	6	7
  Str = A 	B	B 			C 	B 	A
  	    	^	^
  	    	j	i

  Again both are same, so removed them.  
		 0	1	2	3	4	5	6	7
   Str = A 		 			C 	B 	A
  	     ^					^
  	     j					i

  As x[j] != x[i] is not equal so we'll copy x[i] at x[++j] place.
		0	1	2	3	4	5	6	7
  Str = A 	C 					B 	A
  	    	^					^
  	    	j					i

  		0	1	2	3	4	5	6	7
  Str = A 	C 	B 					A
  	    		^					^
  	    		j					i

  x[j] != x[i] so, we copy x[i] ele and paste at x[++j] place & x[j] is
  pointing to that paste character, and i++ is incremented.

  		0	1	2	3	4	5	6	7
  Str = A 	C 	B 	A
  	    			^					^
  	    			j					i

  As i ptr has reached past the end of list so we'll delete from j+1 till end 
  of list. 
**/

/**
* Approach: Reducing String on fly i.e removing adjacent characters on fly
* Time: O(n) & Space: O(1).
**/

string reduceString(string s) {

	int j=0; 
	for(int i=1; i<s.size(); i++) {
		while((j >= 0) && s[j] == s[i]) {
			//cancel the characters
			--j;
			++i;
		}
		//cout << "i: " << i << endl;
		s[++j] = s[i];
	}
	//cout << "j: " << j << endl; //as per 1st I/P it is pointing to 3rd idx.

	//now remove/delete from j+1 till end of list the characters
	s.erase(j+1);
	return s;
}


int main() {

	string s = "ABCCBCBA";
	string res = reduceString(s);
	cout << res << endl;
	//O/P: ACBA 

	s = "BBAAABBABA";
	res = reduceString(s);
	cout << res << endl;
	//O/P: BA

	s = "AAAAAA";
	res = reduceString(s);
	cout << res << endl;
	// O/P: ""
	return 0;
}

