#include<iostream>
#include<string>
#include<stdio.h>
#include<stack>
#include<cctype>

using namespace std;

/**
* Question: Backspace String Compare.

* Given two strings S and T, return if they are equal when both are typed into 		  empty text editors. # means a backspace character.
* Note that after backspacing an empty text, the text will continue empty.
Ex1:									Ex2:
Input: S = "ab#c", T = "ad#c"			Input: S = "ab##", T = "c#d#"
Output: true							Output: true
Explanation: Both S and T become "ac"	Explanation: Both S and T become "".

Ex3:
Input: S = "a#c", T = "b"
Output: false
Explanation: S becomes "c" while T becomes "b".

Note:
1 <= S.length <= 200
1 <= T.length <= 200
S and T only contain lowercase letters and '#' characters.

LOGIC:

Intutive Approach: Using Stack

S = "abc##d" T = "ad"
		^

Abhi character '#' aya string traverse krte waqt toh abhi stack mai se joh top pe hai n usko pop krr dehnge
Stack 
|	   |
|	   |	
|  c   |
|  b   |	
|  a   |	
|------|



Another Approach: Two pointer:

S = "a 	b 	c 	# 	# 	d"  	T = "a 	d"
						^				^
						s 				t

check whether at 's' & 't' ptr it is valid character or not. Agar valid character hai toh match it. If it is matching, then bring down both ptr 's' & 't' down.

S = "a 	b 	c 	# 	# 	d"  	T = "a 	d"
					^				 ^
					s 				 t

Again check both are valid character or not. So from above, at T string ptr 't' is at valid character but in S string it is not. i.e. it is '#'. So keep a count of hash('#') character, so now count it 1. i.e countOf# = 1. So now bring down 's' ptr not 't' because 't' ptr is at valid character. So bring 's' ptr down until we get valid character 

S = "a 	b 	c 	# 	# 	d"  	T = "a 	d"
			^				 		 ^
			s 				 		 t

countof# = 2, and 's' ptr is at valid character. So, now -1 from countOf#. i.e countOf# -= 1, and then decrement 's' ptr from string until countOf# == 0.

S = "a 	b 	c 	# 	# 	d"  	T = "a 	d"
	 ^				 				 ^
	 s 				 		 		 t

So, now counOf# = 0, and both 's' & 't' ptr are pointing at valid character, so compare both 's' & 't' ptrs characters, If it equal then return true otherwise return false.

Complexity: Time: O(n) & Space: O(1).

Algo:
1. Start both pointers from end of string i.e from length of string.
2. CountOf# if it is their in string.
3. Jaise hi '#' nhi mila toh, hrr valid character mai count ko decrement karenge.


*/
/**
string build(string s) {

	stack<char> stk;

	for(auto& c : s.c_str()) {
		if(c != '#') {
			stk.push(c);
		} else {
			stk.pop();
		}
	}
	return std::to_string(stk);
}

bool backspaceCompare(string S, string T) {
	return false;
}

*/
/**
bool backspaceCompare(string S, string T) {
    
    int s = S.length()-1;
    int t = T.length()-1;
    int skipValidCharactersInS = 0;
    int skipValidCharactersInT = 0;
    while(s >=0 || t >=0) {
    	while(s >=0) {
    		if(S[s] == '#') {
    			skipValidCharactersInS += 1;
    			--s;
    		} else if(skipValidCharactersInS > 0) {
    			skipValidCharactersInS -=1;
    			--s;
    		} else {
    			//if in 's' string character is valid then break the loop to check whether s[c] == t[c] if yes then return true otherwise false;
    			break;
    		}
    	}
    	while(t >=0) {
    		if(T[t] == '#') {
    			skipValidCharactersInT += 1;
    			--t;
    		} else if(skipValidCharactersInT > 0) {
    			skipValidCharactersInT -=1;
    			--t;
    		} else {
    			//if in 't' string character is valid then break the loop to check whether s[c] == t[c] if yes then return true otherwise false;
    			break;
    		}
    	}
    	//cout << S[s] << T[t] << endl;
    	if((s >=0) && (t >= 0) && (S[s] != T[t])) {
    		return false;
    	}
    	--s;
    	--t;
    }  
    return true;
}
*/

/*
char findNextValidChar(string& s, int *i) {
	int skipValidCharacters = 0;
	if(s[i] == '#') {
		skipValidCharacters += 1;
	} else {
		if(skipValidCharacters > 0) {
			skipValidCharacters -= 1;

		}
	}
	cout << s[i];
	return s[i];
}
*/
/**
bool backspaceCompare(string S, string T) {
	
	for(int i = S.length()-1, j = T.length()-1; i>=0 || j>=0; --i, --j) {
		if(findNextValidChar(S, &i) != findNextValidChar(T, &j)) {
			return false;
		}
	}
	return true;
}

*/

char nextChar(string &s, int i) {
	int skip = 0;
	if(s[i] == '#') {
		skip += 1;
		--i;
	} else if((bool)isalpha(s[i]) && skip > 0) {
		--skip;
		i > 0 && --i;
	} else {
		return s[i];
	}
	return '\0';
}

int main() {

	cout << std::boolalpha;
	string S = "abc##d", T = "ad";
/*	bool res = backspaceCompare(S, T);
	cout << res << endl;
*/
	/*
		0	1	2	3	4	5
		a 	b 	c 	# 	# 	d
	*/ 
	//cout << S[S.length()-1];
	// for(int i = S.length()-1; i>=0; --i) {
	// 	if(S[i] == '#') {
	// 		skip += 1;
	// 		--i;
	// 	} else if((bool)isalpha(S[i]) && skip > 0) {
	// 		//cout <<"skip in : "<< skip << endl;
	// 		--skip;
	// 		//cout << "i: " << i << endl;
	// 		i > 0 && --i;
	// 	} else {
	// 		cout <<"S[i]: "<< S[i] << endl;
	// 	} 
	// }

	for(int i=S.length()-1; i>=0; --i) {
		cout << nextChar(S, i);
	}
	return 0;
}