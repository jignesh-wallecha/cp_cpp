#include<iostream>
#include<string>
#include<stack>

using namespace std;

/**
Question: Valid Parantheses:(Very Important for interview)

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:
Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.

Ex1:Input: s = "()"		Ex2: Input: s = "()[]{}"	Ex3: Input: s = "([)]"
	Output: true			 Output: true				 Output: false
			 	 			
Constraints:
1 <= s.length <= 104
s consists of parentheses only '()[]{}'.

LOGIC:
The expressions that we'll deal with in this problem can consist of three different type of paranthesis:
* ( ), 
* { } and
* [ ]

Approach 1: str = "(()(()()()))"
1. We can process the expression one bracket at a time starting from the left.

2. Suppose we encounter an opening bracket i.e (, it may or may not invalid expression becuase there can be matching ending bracket somewhere in the remaining part of the expression. Here, we simply increment the counter keeping track of left parenthesis till now. left += 1.

3. If we encounter a closing bracket, this has two meanings:
	
	1. One, there was no matching opening bracket for this closing bracket and in that case we have an invalid expression. This is the case when left == 0. i.e. when there're no unmatched left brackets available.

	2. We had some unmatched Opening bracket available to match this closing bracket. This is the case when left > 0 i.e we have unmatched left brackets available.

4. If we encounter a closing bracket i.e when left == 0, then we have invalid 			expression on our hands. Else we decrement left thus reducing the no. of 			unmatched left paranthsis available.

5. Continue processing the string until all parenthesis have been processsed.

6. If in end we still have unmatched left paranthesis available, this implies an invalid expression.

Approach 2: Stacks

An interesting property about a valid paranthesis expression is that a sub-expression of a valid expression should also be a valid expression. (Not every sub-expression) eg:
	  
	   0	1	 2	  3	   4	5 	 6	  7    8	9	10	 11
	[ '{', '[', '[', ']', '{', '}', ']', '}', '(', ')', '(', ')' ]
			--------------------------		   ------    ------
				valid sub-expression			valid.    valid
	   ------------------------------------    ----------------
	   		valid Sub-expression			  valid sub-expression


*/
/**
bool isValid(string s) {
	if(s.length() == 1) {
		return false;
	}
	int left = 0;
	for(const auto& c : s) {
		if(c == '(') {
			left += 1;
		} else {
			if(left == 0) {
				return false;
			} 
			left -= 1;
		}
	}
	//cout << left << endl;
	if(left != 0) {
		return false;
	}
	return true;
}
*/
/**
    * Complexity: Time: O(n) because we simply traverse the given string one 						character at a time and push & pop operations on a stack  					  take O(1) time.
    *             Space: O(n) as we push all opening brackets onto the stack and 					  in the worst case, we will end up pushing all the 							 brackets onto the stack e.g."((((((((".  
*/
bool isValid(string s) {
    stack<char> stk;
      
    for(const auto& c : s) {
        if(c == '(' || c == '{' || c == '[') {
            stk.push(c);
        } else {
            if(c == ')' && stk.top() != '(') {
                return false;
            } else if(c == '}' && stk.top() != '{') {
                return false;
            } else if(c == ']' && stk.top() != '[') {
                return false;   
            }
            stk.pop();
        }
    }
    //cout << "Stack size: " << stk.size() << endl;
    if(!stk.empty()) { 
    	return false;
    }
    return true;
}
int main() {

	cout << std::boolalpha;
/**
Approach 1:

	string str = "(()(()()()))";
	bool res = isValid(str);
	cout << res << endl;
	//true

	str = "(()";
	res = isValid(str);
	cout << res << endl;
	//false

	str = "(((((())))))";
	res = isValid(str);
	cout << res << endl;
	//true 

	str = "()()()()";
	res = isValid(str);
	cout << res << endl;
	//true

	str = "\"(((((((()\"";
	res = isValid(str);
	cout << res << endl;
	//false
**/
	string s = "()[]{}";
	bool res = isValid(s);
	cout << res << endl;
	//true

	s = "(]";
	res = isValid(s);
	cout << res << endl;
	//false

	s = "([)]";
	res = isValid(s);
	cout << res << endl;
	//false

	s = "{[]}";
	res = isValid(s);
	cout << res << endl;
	//true

	s = "]";
	res = isValid(s);
	cout << res << endl;

	return 0;
}

