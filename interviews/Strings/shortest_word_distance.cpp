#include<iostream>
#include<string>
#include<vector>
#include<algorithm> //for find()
#include<climits> //for INT_MAX
using namespace std;

/**
* Shortest Word Distance:
* Given a list of words and two words word1 & word2, return the shortest distance between these two words in the list.
* You may assume that word1 does not equal to word2, and word1 & word2 are both in list.

EX1: 
I/P: 
words = ["practice", "makes", "perfect", "coding", "makes"]
word1 = "coding", word2 = "practice"

O/P: 3

EX2:
I/P: 
words = ["practice", "makes", "perfect", "coding", "makes"]
word1 = "makes", word2 = "coding"
O/P: 1

LOGIC:

1. Two Pointer Logic:
	
l = 0, r = 0; 

-> 'l' ptr is keeping point for word1 & 'r' ptr is keeping point on/for 		word2. 

-> To avoid chance for mismatch we'll take another variable word1Ptr & word2Ptr which will going to keep track for found word1 & word2 respectively.

-> By default word1Ptr & wordPtr is init with -1.
	
	word1Ptr = -1, word2Ptr = -1;

-> if str[l] == word1 then word1Ptr will be updated with 'l' then 'l' will be incremented.

->  if str[r] == word2 then word2Ptr will be updated with 'r' then 'r' will be incremented.

-> if 'l' & 'r' is matched or not matched then also 'l' & 'r' will be incremented.

word1 = "makes" , word2 = "coding"

strs = "practice", "makes", "perfect", "coding", "makes"
   		^	^
   		l 	r

strs = "practice", "makes", "perfect", "coding", "makes"
   					^	^
   					l 	r
   				  w1Ptr

Now, as we have marked word1Ptr but word2Ptr is -1, so, we'll go forward in vector and check whether again their is str[l] == word1 or str[r] == word2.

strs = "practice", "makes", "perfect", "coding", "makes"
   					^					 ^
   					l 					 r
   				  word1Ptr 				word2Ptr

Now both word1 & word2 are found in vector, so now we'll take difference of both word1Ptr & word2Ptr. And store the difference in minDistance variable.

So now minDistance = 2

Now again we'll continue the 'l' & 'r' ptr in vector to find whether their is more minDistance between both these words.

strs = "practice", "makes", "perfect", "coding", "makes"
   										 ^			^
   					 					 r  		l
   				   					 word2Ptr 	 word1Ptr

So now difference between both these word2Ptr & word1Ptr is less than prev minDistance, so we'll update minDistance variable.

For calculating Difference we'll take std::abs() of these ptrs.

Final, minDistance = 1;

**/


class ShortestWordDistance {

	vector<string>& strs;
public:
	ShortestWordDistance(vector<string>& strs_) : strs(strs_) {}

	/**
	* Two pointer Approach:
	* Time Complexity: O(n)
	* Space Complexity: O(1)
	**/
	int shortestWordDistance(string word1, string word2) {
		if(find(begin(strs), end(strs), word1) == strs.end() || find(begin(strs), end(strs), word2) == strs.end()) {
			return -1;
		}
		if(word1 == word2) {
			return 0;
		}

		int word1Ptr = -1, word2Ptr = -1;
		int minDistance  = INT_MAX;
		int left = 0, right = 0;

		while(left < strs.size() && right < strs.size()) {
			if(strs[left] == word1) {
				word1Ptr = left;
				//cout << "left: " << left << endl;
				left++;
				if(minDistance > std::abs(word1Ptr - word2Ptr) && word2Ptr > -1) {
					minDistance = std::abs(word1Ptr - word2Ptr);
				}

			} else if(strs[right] == word2) {
				word2Ptr = right;
				//cout << "right: " << right << endl;
				right++;
				if(minDistance > std::abs(word1Ptr - word2Ptr) && word1Ptr > -1) {
					minDistance = std::abs(word1Ptr - word2Ptr);
				}	
			} else {
				left++;
				right++;
			}
		}
		return minDistance;
	}
};

int main() {

	vector<string> strs = {"practice", "makes", "perfect", "coding", "makes", "simple"};

	ShortestWordDistance shortwordDist(strs);
	string word1 = "makes";
	string word2 = "coding";
	int res = shortwordDist.shortestWordDistance(word1, word2);
	cout << res << endl;
	//O/P: 1

	word1 = "coding";
	word2 = "practice";
	res = shortwordDist.shortestWordDistance(word1, word2);
	cout << res << endl;
	//O/P: 3
}