#include<iostream>
#include<string>
#include<stack>
#include<queue>
#include<algorithm> //for remove
using namespace std;

/**
* Question: Minimum Remove to Make Valid Paranthesis:

Given a string s of '(' , ')' and lowercase English characters. 

Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that the resulting parentheses string is valid and return any valid string.

Formally, a parentheses string is valid if and only if:

It is the empty string, contains only lowercase characters, or
It can be written as AB (A concatenated with B), where A and B are valid strings, or
It can be written as (A), where A is a valid string.
 

Example 1:

Input: s = "lee(t(c)o)de)"
Output: "lee(t(c)o)de"
Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.

Example 2:

Input: s = "a)b(c)d"
Output: "ab(c)d"
Example 3:

Input: s = "))(("
Output: ""
Explanation: An empty string is also valid.
Example 4:

Input: s = "(a(b(c)d)"
Output: "a(b(c)d)"
 

Constraints:

1 <= s.length <= 10^5
s[i] is one of  '(' , ')' and lowercase English letters.

LOGIC:

Approach 1: (Using Stack & Queue)

It could solve two purposes,
1. give me the indexes which are wrong openings & closing paranthesis.


Follow Up Question Can i reduce space complexity ?

Yes

Approach 2: Two pass One Variable:
Let's us keep, Balance = 0;


**/

/**
* Apprach 1:
**
*/
string minRemoveToMakeValid(string s) {

	stack<int> stk;
	queue<int> que;

	for(int i=0; i<s.size(); ++i) {
		if(s[i] == '(') {
			stk.push(i);
		} else if(s[i] == ')') {
			if(stk.empty()) {
				//stack is empty so push in queue
				que.push(i);
			} else {
				stk.pop();
			}
		}
	}
	if(!stk.empty()) {
		while(stk.empty() != 0) {
			s[stk.top()] = '#';
			stk.pop();
		}
	} else if(!que.empty()) {
		while(!que.empty()) {
			s[que.front()] = '#';
			que.pop();
		}
	}
	// cout << "STack size: " << stk.size() << endl;
	// cout << "Queue size: " << que.size() << endl;
	s.erase(remove(s.begin(), s.end(), '#'), s.end());
	return s;
}

/**
string minRemoveToMakeValid(string s) {

	//traverse left of list to keep track for opening parenthesis & increment bal track on found open paranthesis & decrement when closing paranthesis comes if bal == -1 then we'll delete/remove that closing paranthesis

	//balance for opening paranthesis
	int balLeft = 0;
	for(int i=0; i<s.size(); ++i) {
		if(s[i] == '(') {
			balLeft += 1;
		} else if(s[i] == ')') {
			if(balLeft != -1) {
				balLeft -= 1;
			}
			//balance = -1, so del/remove that closing paranthesis 
			if(balLeft == -1) {
				s[i] = '#';
				++balLeft;
			}

		}
	}
	//traverse from right to left of list and keep track for closing parathesis, if we encounter closing paranthesis we'll keep incrementing & decrementing on opening paranthesis.
	
	int balRight = 0;
	if(balLeft > 0) {
		for(int i=s.size()-1; i>=0; --i) {
			if(s[i] == ')' || s[i] == '#') {
				balRight += 1;
				//cout <<"i : -> " << i << "right: " << balRight << endl;
			} else if(s[i] == '(') {
				if(balRight != -1) {
					balRight -= 1;
					//cout << "r: " << balRight << endl;
				}
				if(balRight == -1) {
					s[i] = '#';
					++balRight;
				//	cout << balRight << endl;
				}
			}
		}
	}
	s.erase(remove(s.begin(), s.end(), '#'), s.end());
	return s;
}
**/
int main() {

	string s = "d(a)))ta((co)d(e";
	string res = minRemoveToMakeValid(s);
	cout << res << endl;
	return 0;
}