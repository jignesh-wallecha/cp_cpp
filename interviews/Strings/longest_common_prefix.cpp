#include<iostream>
#include<vector>
#include<string>
using namespace std;

/**
* Question: Longest Common Prefix: 
* Write a function to find the longest common prefix string amongst an array of strings.

* If there is no common prefix, then return an empty string.

Ex1:
I/P: strs = ["flower", "flow", "flight"]
O/P: "fl"

Ex2:
Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

I/P: strs = ["study", "student", "stud"]
O/P: "stud"

Constraints:
0 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lower-case English letters.

LOGIC:

Approach 1: 
Horizontal Scanning:

strs = {"flower", "flow", "flight"}

-> We'll take or assume that strs[0] as prefix, which exist in all.

prefix = strs[0]; //"flower"

-> from index 1 in strs array we'll check/search whether prefix is (indexOf/find) 
	their in strs[1] if not then we'll remove/subtract from back one character 
	and then again check. This will go on till we'll find longest commonf prefix

->  If found then check with other string in array. Till < strs.n 

Perform:

strs = {"flower", "flow", "flight"}
				  /
			   	 /
flower ---------/
flowe ----/
flow---/ (found)
				    	/
flow------------------ /
flo--/
fl--/ (found)

return "fl" as longest common prefix

Complexity:
Time: Worst case would be it will match all the characters,
So Time Complexity: O(S) where S is sum of length of all words. 

Approach 2: 
Vertical Scanning: 
	

	   0	1 	2	3	4	5	
strs = F 	L 	O 	W 	E 	R
	   F    L   O   W
	   F 	L   I   G 	H 	T
	   
	   ^	
	   	
We'll check at 0th idx if all strings 0th character is same then we'll give
further and again check.

So 0th idx's all characters of their repective strings first character is same.
Same for 1st idx also.

	   0	1 	2	3	4	5	
strs = F 	L 	O 	W 	E 	R
	   F    L   O   W
	   F 	L   I   G 	H 	T
	   
	  		    ^	
At 2nd idx it is not same so return prev both matched characters of strings.

Complexity:

Time: O(nk): where n is no. of words & k is length of longest prefix. As at 
2nd idx it stopped so it run two times only.
**/

/**
* Horizontal Approach:
**

string longestCommonPrefix(vector<string>& strs) {

	string prefix = strs[0];
	for(int i=1; i<strs.size(); ++i) {
		while(strs[i].find(prefix) > 0)
		{
			prefix = prefix.substr(0, prefix.length()-1);
			//cout << "prefix: "  << prefix << endl;
			if(prefix.empty()) {
				return "";
			}
		}
	}
	return prefix;
}
*/
/**
* Vertical Approach:
**/

string longestCommonPrefix(vector<string>& strs) {

	if(strs.empty()) {
		return "";
	}

	for(int i=0; i<strs[0].length(); ++i) {
		char ch = strs[0].at(i);
		//cout << ch << endl;
		for(int j=1; j<strs.size(); ++j) {
			if(strs[j].at(i) != ch) {
				return strs[0].substr(0, i);
			}
		}
	}
	return strs[0];
}

int main() {

	vector<string> strs = {"flower", "flow", "flight"};
	string lcmPrefix = longestCommonPrefix(strs);
	cout << lcmPrefix << endl;

	strs = {"dog", "day"};
	lcmPrefix = longestCommonPrefix(strs);
	cout << lcmPrefix << endl;

	int x = 10;
	auto y = x - '0';
	cout << "Interconvert: " << y << endl;
	return 0;
}