#include<iostream>
#include<string>
#include<unordered_map>
#include<memory> //used for memset function in approach 2
#include<cstring>

using namespace std;

/**
* Question: Valid Anagrams

Given two strings s and t , write a function to determine if t is an 				anagram of s.
Anagram are the strings that can be defined as "If one string can be re-arranged to form the other one, then we say that they are anagram".

Ex1: Input: s = "anagram", t = "nagaram" 	EX2: Input: s = "rat", t = "car"
     Output: true								 Output: false

Note:
You may assume the string contains only lowercase alphabets.


LOGIC:
2 strings ka agar length alag hai toh woo anagram nhi ho sakta hai.

Use the logic of Hashmap.

	s = "anagram", t = "nagaram"

Hashmap:Will Process 's' string, and count ki each character kitni baar aha raha hai and keep count of characters as value and character as Key.

For s 'string' process.
 k  V 						
[a	3 ]	         					
[n	1 ]
[g 	1 ]
[r	1 ]
[m	1 ]

So now same no. of counts hai of 't' string in hashmap then it is anagram. If in 't' string if their is a character which is not present then hashmap, then also it is not a anagram.

So while processing 't' string, decrement the values(count of character) in hashmap. 

t = "nagaram"

For t 'string' process.
 k  V 						
[a	3/2/1/0 ]	         					
[n	1/0 ]
[g 	1/0 ]
[r	1/0 ]
[m	1/0 ]

Now process the hashmap, if all keys values are zeros then it is anagram, agar decrement krte waqt agar value negative hogaya toh return false.

Agar process krne badh string & hashmap agar usme koi key ki value positive then also return false as it also not a anagram.

Complexity:
Time: O(n) & Space: O(1)

Intutive Method:

Sort the both strings, then compare both strings, agar anagram hoga toh, sorted string is same. Complexity is O(nlogn).

**/

/* Method1: Sort n Compare
* Time: O(nlogn) & Space: O(1)
*/

/* Method2: 
* Time: O(n) & Space: O(1)
*

bool isAnagram(string s, string t) {

    if(s.length() != t.length()) return false;

    unordered_map<char, int> counter;

    for(const char& c : s) {
    	++counter[c];
    	//by default whenever we insert it will be printed as zero/default value of integer.
    }

    for(const char& c : t) { 
    	//if 'c' character nhi hai 't' string mai then also it is not a anagram.
    	//if(!counter.count(c)) return false;
    	--counter[c];
    	if(counter[c] < 0) {
    		return false;
    	}
    }
    return true;
}	
*/
/**
* Method3: If question says that it string is of small characters
* Time: O(n) & Space: O(1).
*/

bool isAnagram(string s, string t) {

    if(s.length() != t.length()) return false;

    int hashmap[26];
        
    memset(hashmap, 0, sizeof(hashmap));
       
    for(const char& c : s) {
        hashmap[c-'a']++;
    }
 	
 	for(const char& c : t) {
        hashmap[c-'a']--;
    }
   
    for(auto it : hashmap) {
        if(it != 0) {
            return false;
        }
    } 
    return true;
}	

/**
* Logically method 3 is more efficient then method 2 as in method 2 hashing is used. i.e. it is internally calling hash function to calculate hash value for key and this is done both at insertion & removing time. So method 2 is more complexed version than compare to method 3. So logically method 2 is slower than method 3.

This thing was asked in microsoft interview: In which they gave such program, and asked them to better this program. i.e they used a word "logically" kahi se bhi program ko fast krr sakte hai kya, not by Big O time. 

So method 3 is logical fast wala soln.
Method 3 can be used when input of string is of character set of UPPERCASE or LOWERCASE only. So this can replace the logic of hashing.

So while in face-to-face interview we should bring this logic, and ask interviewer shall i consider input of character set as only lowercase or Uppercase only. So if interviewer says it is unicode characters then this is logic of method 3 is not possible. Then Hashing is only fastest way to solve it.

*/
int main() {

	string s = "anagram";
	string t = "nagaram";
	bool res = isAnagram(s, t);
	cout <<std::boolalpha;
	cout << res << endl;
	//true

	s = "cat";
	t = "rat";
	res = isAnagram(s, t);
	cout << res << endl;	
	//false

	return 0;
}