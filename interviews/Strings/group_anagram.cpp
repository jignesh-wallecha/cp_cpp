#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<unordered_map>

using namespace std;

/**
* Question: Group Anagrams:(Very Important Question)
Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

Ex1: Input: strs = ["eat","tea","tan","ate","nat","bat"]
	 Output: [["bat"],["nat","tan"],["ate","eat","tea"]]

Ex2: Input: strs = [""]		Ex3: Input: strs = ["a"]
	 Output: [[""]]				 Output: [["a"]]

Constraints:

1 <= strs.length <= 104
0 <= strs[i].length <= 100
strs[i] consists of lower-case English letters.

LOGIC:

strs = ["eat","tea","tan","ate","nat","bat"]

create a intermidiate array which contains sorted characters of string
				
				0		1		2		3	  4		5
intermediate = ["aet", "aet", "ant", "aet", "ant", "abt"]
	 
Now, we need to group from intermediate.

How to do Grouping ?

Soln1:

Sort the intermeidate array of strings
						
sorted intermediate = ["abt", "aet", "aet", "aet", "ant", "ant"]

Then check str[0] == str[1] if they are same then their is a group otherwise not.

So from above str[0] != str[1] so, str[0] is one group. But we have to keep respected index of string from unsorted immediate array in sorted immediate array also.Like,

						  0		 1		 2		3	   4	  5
unsorted intermediate = ["aet", "aet", "ant", "aet", "ant", "abt"]
						
						  5      0      1	 	3      2      4
sorted intermediate =   ["abt", "aet", "aet", "aet", "ant", "ant"]

Now, when we see sorted immediate array i.e str[0]("abt") then we think/see logically 5th idx of original array, str[1]("aet") we'll see logically 0th idx of original array and so till end of sorted immediate array.

But this Soln1, will get complex while doing code. So, we'll not follow soln1 logic, as this is the problem.

Complexity would be nlogn to sort for each str in array. Therefore final complexity is, O(w nlogn) in which 'w': each word of array, to be sorted.
Then, O(wlogw) will be complexity to sort whole array of unsorted immediate array of sorted characters but not sorted word.
O(w nlogn) + O(wlogw).

Soln2:
		 0 		 1	 	  2		  3		  4		  5	 		
strs = 	["eat",	"tea",	"tan",	"ate",	"nat",	"bat"]

				0		1		2		3	  4		5
intermediate = ["aet", "aet", "ant", "aet", "ant", "abt"]

We'll use hashmap: we'll consider key from intermediate array.

For ex: "aet" is key in hashmap toh jitne bhi "aet" hai intermediate array woo sabh common bann jayenge and value for this key would be i.e "aet" value/element will be from input string array i.e jiska key "aet" rahega, unno sabh ko ekk array mai daal ke hashmap ke values mai place krr dehnge. 

 k 			V
[aet	[eat, tea, ate] ]
[ant	[tan, nat]	    ]
[abt	[bat]     		]

*/

/**
Complexity: Time: O(w nlogn): where 'w' is no. of words and n is length of the   							 longest word.

            Space: O(wn): where 'w' is each string will go, and each 'w' will     			    get 'n' is length of set/vector.
*/

vector<vector<string>> groupAnagrams(vector<string>& strs) {
       
	unordered_map<string, vector<string>> groups;
        
    for(const auto& str : strs) {
        string tmp{str};
        std::sort(tmp.begin(), tmp.end());
        groups[tmp].emplace_back(str);
    }
        
   	// for(const auto& kvp : groups) {
    //     for(auto& value : kvp.second) {
    //     	cout << kvp.first << " -> " << value << endl;
    //     }
    // }
            
    vector<vector<string>> anagrams;
    for(const auto& kvp : groups) {
    	vector<string> group;
    	for(const auto& str : kvp.second) {
    		group.push_back(str);
    	}
    	anagrams.push_back(group);
    }

    return anagrams;
}

int main() {

	vector<string> strs = {"eat","tea","tan","ate","nat","bat"};
	vector<vector<string>> res = groupAnagrams(strs);

	for(const auto& items : res) {
		cout << "[";
		for(const auto& s : items) {
			cout <<"["<< s <<"]," <<" ";
		}
		cout << "]";
		cout << '\n';
	}
	cout << '\n';
	return 0;
}