#include<iostream>
#include<string>
#include<vector>

using namespace std;

/**
    * This Approach Takes Complexity, Time: O(n) & Space: O(n)
    *
    
    //helper function 
    bool helper(string& s, int i) {
        int j = s.length() - 1 - i;
        return i >=j ? true : s[i] == s[j] && helper(s, i+1);
    }
    
    bool isPalindrome(string s) {
        
        //Sanitizing the input
        vector<char> cleanChars; 
        for(int i=0; i<s.length(); ++i) {
            if(isalnum(s[i])) {
                cleanChars.emplace_back(tolower(s[i]));
            }
        }
        
        string cleanStr = string(begin(cleanChars), end(cleanChars));
        
        //cout << cleanStr << '\n';
    
        return helper(cleanStr, 0);
    }
*/

bool isPalindrome(string s) {
        
    int left = 0, right = s.length()-1;
        
    while(left <= right) {
        while(left < right && !isalnum(s[left])) {
            ++left;
        }
        while(left < right && !isalnum(s[right])) {
            --right;
        }
        if(tolower(s[left]) != tolower(s[right])) {
           return false;
        }
       	++left;
       	--right;
    }
    return true;
}

int main() {

	string s = "race a car";
	bool res = isPalindrome(s);
	cout << std::boolalpha;
	cout << res << endl;
	// O/P: false

	s = "A man, a plan, a canal: Panama";
	res = isPalindrome(s);
	cout << res << endl;
	//true

	s = "Racecar";
	res = isPalindrome(s);
	cout << res << endl;
	//true

    s = "0P";
    res = isPalindrome(s);
    cout << res << endl;
	return 0; 
}