#include<iostream>
#include<string>
#include<cstring>
#include<vector>

using namespace std;

/**
* Question: Compare Version Numbers

* Given two version numbers, version1 & version2, compare them.

* Version number consist of one or more revisions joined by a dot "." . Each 
 revision consists of digits and may contain leading zeros. Every revision 
 contains atleast one character. Revisions are 0-indexed from left to right, with
 the leftmost revision being revision 0, the next revision being revision 1, and
 so on. For Ex, 2.5.33 and 0.1 are valid version numbers.

* To compare version numbers, compare their revisions in left-to-right order. Revisions are compared using their integer value ignoring any leading zeros. This means that revisions 1 and 001 are considered equal. If a version number does not specify a revision at an index, then treat the revision as 0. For example, version 1.0 is less than version 1.1 because their revision 0s are the same, but their revision 1s are 0 and 1 respectively, and 0 < 1.

* Return the following:

If version1 < version2, return -1.
If version1 > version2, return 1.
Otherwise, return 0.

Constraints:

1 <= version1.length, version2.length <= 500
version1 and version2 only contain digits and '.'.
version1 and version2 are valid version numbers.
All the given revisions in version1 and version2 can be stored in a 32-bit integer.

* Example 1:
Input: version1 = "1.01", version2 = "1.001"
Output: 0
Explanation: Ignoring leading zeroes, both "01" and "001" represent the same integer "1".


LOGIC:

In practical world, it is used. Git internally does this. Some companies
which work on version controls ask this question/problem (cisco hot favorite).

Approach 1: 

Verion 1 = "1 . 2 . 3 . 2"
Version2 = "1 . 2 . 3 . 0"

-> We need to compare from left to right 

-> Split on dot "."

-> v1 = {"1", "2", "3", "2"};
   v2 = {"1", "2", "3", "0"};
		  
		  0	   0 	0	 1

   Now we need to compare both array if v1[i] == v2[i] then return 0, 
   else if(v1[i] > v2[i]) return 1 otherwise return -1(as v1 > v2).

Small Twist:
While comparing both the arrays we need to manage something.

v1 = 1 . 2 . 3
v2 = 1 . 2 . 2 . 4 . 2

So, in v1 we need to virtually add two zeros in it. 

So we need to concentrate on the length.

As v2 > v1 so we could have return v2 but We need to add two zeros because, 
their is a edge case,
v1 = 1 . 2 . 3 
v2 = 1 . 2 . 3 . 0 . 0 . 0
then v1 & v2 is same.
So don't need to go on length of versions, As we'll padd the zeros in it.

Complexity:

version1 -> length n(n dots in string), so to split it we'll require n times
version2 -> length m.

So complexity for spliting strings is O(n + m), And max of length of both string to compare both string array.

hence, O(n + m + max(n, m)); 

Approach 2:

Split on Fly:


**/

vector<char> split(const char* c_arr, const char a_delim) {
	vector<char> santized_arr();
	int i=0;
	while( (i < strlen(c_arr)) && c_arr[i] != a_delim) {
		santized_arr[i++] = c_arr[i++];
		//cout << santized_arr[i];
	}
	return santized_arr;
}

// int compareVersion(string version1, string version2) {

// 	char* v = strdup(version1.c_str());
// 	char* v1 = strtok(v, "\\.");
// 	//char* v2 = strtok(version2.data(), "\\.");
// 	//char* v1 = strsep(&v, ".");
// 	// for(int i=0; i<7; ++i) {
// 	// 	cout << v1[i] <<" ";
// 	// }
// 	//cout << '\n';

// 	return -1;
// }


/**
* Approach 2: Split on fly
**

int compareVersion(string version1, string version2) {

	string s1 = "", s2 = "";
	for(int i=0, j=0; i<version1.size() || j<version2.size(); ++i, ++j) {

		while((i < version1.size()) && version1[i] != '.') {
			s1 += version1[i++];
		}

		while((j < version2.size()) && version2[j] != '.') {
			s2 += version1[j++];
		}
		if(s1.empty()) {
			s1 = '0';
		} else if(s2.empty()) {
			s2 = '0';
		}
		int s1 = stoi(s1);
		int s2 = stoi(s2);
		if(s1 < s2) return -1;
		else if(s1 > s2) return 1;
	}
	return 0;
}
*/
int main() {

	string version1 = "1.2.3";
	string version2 = "1.2.3.4";
	// int res = compareVersion(version1, version2);
	// cout << res << endl;

	vector<char> n_arr = split(version2.c_str(), '.');
	for(int i=0; i<n_arr.size(); ++i) {
	 	cout << "n_arr: "<< n_arr[i] <<" ";
	}
}

