#include<iostream>
#include<string>
#include<unordered_map>
#include<cstring>
using namespace std;

/**
* First Unique Character in String:
* 
* Given a String, find the first non-repeating character in it and return its index.
* If no such characters exists then return -1.
* You may assume that the string contains only lowercase English letters.

EX1: 					EX2:

I/p S = "madam"			I/P S: "competitive"
O/p: 2					O/P: 0
Expln:
'd' is the first 
unique character in S.


LOGIC:
**/

/**
* Using HashMap: 
* Time Complexity: O(n)
* Space Complexity: O(1)
**

int firstUniqueChar(string s) {

	unordered_map<char, int> counter;

	for(const auto& c : s) {
		++counter[c];
	}

	for(int i=0; i<s.length(); ++i) {
		if(counter[s[i]] == 1) {
			return i;
		}	
	}
	return -1;
}	
*/
/**
* Using of Array of fixed Size of 26
* Time Complexity: O(n)
* Space Complexity: O(1)
**/

int firstUniqueChar(string s) {

	int hashmap[26];
	memset(hashmap, 0, sizeof(hashmap));

	for(const auto& c : s) {
		++hashmap[c-'a'];
	}

	for(int i=0; i<s.length(); ++i) {
		int idx = s.at(i)-'a';
		if(hashmap[idx] == 1) return i;
	}
	return -1;
}

int main() {

	string s = "madam";
	int res = firstUniqueChar(s);
	cout << res << endl;
	//O/P: 2

	s = "competitive";
	res = firstUniqueChar(s);
	cout << res << endl;
	//O/P: 0
}
