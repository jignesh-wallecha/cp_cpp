#include<iostream>
#include<string>
#include<vector>

using namespace std;

/**
* 	Imp Question:(hot favourite Question in Interview)
*	Given a string "str" return the longest palindromic substring in "str"
	
	Ex1: str = "babad"			EX2: str = "ac"
		 O/P: "bab" or "aba"   		 O/P: "a"


logic: 

The Question can be asked also to return index, length of largest palindromic substring.

BruteForce : Cost of each substring to take out will be O(n^2), and that substring is palindrome or not this will take O(n), it will have internal nested loop, Therefore Total Cost is O(n^3), because for every n^2 we will spend O(n) times.

Interview mai nhi chalega toh we need to reduce Complexity

What is Palindrome?
Mirror image from one particular point.

Ex: a b c b a 
    <- -|- ->

	
	a b c b a 
	|
 <--i------>

	At any given point i.e. find left & right from 'i' ptr. We'll increment right || decrement left ptr if left idx element is equal to right idx element from that ith ptr.
	If we go further i.e (decrement/increment) then palindromic substring length will be incresed.

	Suppose,
	1. 	 a  b  c  b  a
			 ^	 ^  ^
			left i  right

	Left is not in range in string i.e it is out of string scope, so their no point of comparing this left & right ptrs characters, so for this ith ptr, palindrome which we're getting is only palindrome = a. So we did not get any other palindrome so we just save this palindrome.

	2. 	0	1	2	3	4 
		a  	b 	c   b   a
		^	^	^
		l	i   r

	Compare l & r ptr characters, are they same ? No!.
	So now only 'b' is the palindrome. Old palindrome is 'a' and new is 'b' both have the same length. We have to find the longest Palindrome substring, so we can ask the interview which one take as palindrome new or old as both has same length. For now palindrome will be same i.e 'a'. Does need to save 'b' as length is same of both.

	3.	0	1	2	3	4 
		a  	b 	c   b   a
			^	^	^
			l	i   r
	Now, both l & r ptr characters, are same. So now, we'll do shifting ie. shift left downwards & right ptr upwards.

	4.	0	1	2	3	4 
		a  	b 	c   b   a
		^		^	    ^
		l	    i   	r
	
	Same, both l & r ptr characters, are same. So now, we'll do shifting ie. shift left downwards & right ptr upwards.

	5.	0	1	2	3	4 
		a  	b 	c   b   a
	 ^			^	    	^
	 l	    	i   		r

	Now, both l & r ptr are not in range. So we'll take previous of l & r ptrs as substring i.e it is palindrome 

	6. | 0	1	2	3	4 |
	   | a  b 	c   b   a |
	 ^ |		^	      |	^
	 l |	    i   	  |	r


	So palindrome = abcba, so we'll update this palindrome, because length is greater than previous palindrome.

	This above implementation will work for all ODD LENGTH PALINDROMES.

FOR EVEN PALINDROME, let us see is/if above implementation work fine or not for even.
	
	0	1	2	3
	a 	b 	b 	a 		
 	^	^	^			
 	l  	i 	r 

 	As l is output of scope not point of comparing at 					  starting so consider pal = a, and increment 'i' ptr.

 	As from above ith is at 1st idx, so both l & r ptr ka character is not same so pal is same i.e pal = a.	

	0	1	2	3
	a 	b 	b 	a 		
 		^	^	^			
 		l  	i 	r 

 	As from above ith is at 2nd idx, so both l & r ptr ka character is not same so pal is same i.e pal = a.	

 	0	1	2	3
	a 	b 	b 	a 		
 			^	^	^			
 			l  	i 	r 

 	r ptr goes outbounds. So pal remains same i.e pal = a.

 	But output should be "abba" not "a" of pal. Therefore Above Applied logic is for Even Length Palindrome not for Odd Length Palindrome.

EVEN LENGTH PALINDROME LOGIC:
	
	0	1	2	3
	a 	b 	b 	a
		-----
		  |
	This is center

	So, we can take anyone ptr.ie. keep i and l ptr at same point and next ptr will be r otherwise take/keep i & r ptr at same point and prev ptr will be l. Take anyone from both of these, because it is a mirror & it is even length. So hamme kisi ekk index ko same rakh ke chalna padhega.

	0	1	2	3		OR 		0	1	2	3			
	a 	b 	b 	a				a 	b 	b 	a
		^	^						^	^
		l   i						i 	r
			r						l
	
	
	Applying above logic now for even length.

	0	1	2	3
	a 	b 	b 	a
		^	^
		l 	i
			r
	
	Now l & r ptrs characters are same so we'll shift l & r ptrs.

	0	1	2	3
	a 	b 	b 	a
	^		^	^
	l 		i 	r
				
	Both l & r are same, so same shifting
	
	0	1	2	3
	a 	b 	b 	a
 ^			^		^
 l 			i 		r

 	Both l & r out of range. So prev ptrs of l & r or substring is palindromic.

 	So output is pal = a b b a

 	So now if there is odd length then work of logic will different if it is even then work of logic will be different.

 	We have to work according the length of palindromic length not string length. We cannot figure out the length of palindrome substring just by looking to the string. 

 	So we'll find even & odd palindrome from string. And fir issme se joh big/badha rahega woo return karenge as final output for longest palindrome substring.

 	Complexity: 
 	At any given point of ith ptr in array we're visting all characters at left and right from that ith ptr. So this will take O(n). And this is done for every characters that is ith ptr will be their at each character, so this will take O(n). So it is O(n) * O(n) = O(n^2).


*/			

vector<int> getLongestPalindromeFrom(string str, int leftIdx, int rightIdx) {

	while(leftIdx >=0 && rightIdx < str.length()) {
		if(str[leftIdx] != str[rightIdx]) {
			break;
		}
		--leftIdx;
		++rightIdx;
	}
	return vector<int> {leftIdx + 1, rightIdx};
}

string longestPalindrome(string str) {

	//for every character we need to do something, so for every character we need to figure it out what is odd and what is even.

	vector<int> currentLongest{0, 1}; //startIdx, endIdx, actual string is endIdx - 1 tak, so it is (0, 0)

	for(int i=1; i<str.length(); ++i) {
		vector<int> odd =  getLongestPalindromeFrom(str, i-1, i+1);
		vector<int> even = getLongestPalindromeFrom(str, i-1, i);

		vector<int> longest = (odd[1] - odd[0]) > (even[1] - even[0]) ? odd : even;
		currentLongest = currentLongest[1] - currentLongest[0] > longest[1] - longest[0] ? currentLongest : longest;
	}
	return str.substr(currentLongest[0], currentLongest[1] - currentLongest[0]);
}

int main() {

	string str = "ababac";
	string res = longestPalindrome(str);
	cout << res << endl;

	return 0;
}