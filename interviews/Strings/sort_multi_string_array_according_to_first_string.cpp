#include<iostream>
#include<string>
#include<algorithm>
#include<vector>

using namespace std;

void sort(vector<vector<string>>& strArr) {

//	cout << strArr[0][0] << endl;

	for(int i = 1; i<strArr.size(); ++i) {
		if(strArr[i][0] < strArr[i-1][0]) {
			std::swap(strArr[i], strArr[i-1]);
		}
	}
}

void show(vector<vector<string>>& strArr) {
	for(auto& arr : strArr) {
		for(auto& e : arr) {
			cout << e <<"  ";
		}
		cout << '\n';
	}
}

int main() {

	vector<vector<string>> strArr = {
		{"150", "Disha", "Dep1"},
		{"145", "Isha", "Dept2"},
		{"175", "Heer", "Dept3"},
		{"135", "Ved", "Dept4"},
		{"147", "Tara", "Dept5"}
	};

	//sort(strArr);
	sort(strArr.begin(), strArr.end());
	show(strArr);
}