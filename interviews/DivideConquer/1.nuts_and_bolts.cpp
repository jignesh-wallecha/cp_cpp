#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

/**
* Nuts and Bolts:
* Given a set of 'n' nuts of different sizes and n bolts of different sizes. Their is a one-one mapping between nuts & bolts.

Comparison of a nut to another nut or a bolt to another bolt is not allowed.
It means nut can only be compared with bolt & bolt can only be compared with nut to see which one is bigger/smaller.

Using the function we give you, you are supposed to sort nuts or bolts, so that they can map in order.

Ex1:

I/p: nuts = ['a', 'b', 'd', 'g']
	 bolts = ['a', 'g', 'd', 'b']

O/p: nuts =  ['a', 'b', 'd', 'g']
	 bolts = ['a', 'b', 'd', 'g']


Logic:

nuts = ['a', 'b', 'd', 'g']
bolts = ['a', 'g', 'd', 'b']

We can't compare nuts with another nuts as it is not allowed. 

**
*/

int partition(vector<char> target, int low, int high, char pivot) {

	int i = low-1;
	for(int j = low; j < high; ++j) {
		if(target[j] <= pivot) {
			++i;
			swap(target[i], target[j]);
		}
	}	
	for(auto e : target) {
		cout << e <<" ";
	}
	cout << '\n';
	cout <<"i: " << i << endl;
	return -1;
}

void matchPairs(vector<char>& nuts, vector<char>& bolts, int low, int high) {

	if(low < high) {

		int pivot = partition(nuts, low, high, bolts[low]);

		partition(bolts, low, high, nuts[pivot]);

		matchPairs(nuts, bolts, low, pivot-1);
		matchPairs(nuts, bolts, pivot+1, high);
	}
}

int main() {

	vector<char> nuts = {'a', 'g', 'd', 'b'};
	vector<char> bolts = {'a', 'b', 'g', 'd'};

	//matchPairs(nuts, bolts, 0, nuts.size());
/*
	cout << "nuts: \n";
	for(auto e : nuts) {
		cout << e << "  ";
	}
	cout << '\n';

	cout << "bolts:\n";
	for(auto e : bolts) {
		cout << e << "  ";
	}
	cout << '\n';

	cout << "size: " << nuts.size();
*/	int j = partition(nuts, 0, nuts.size(), nuts[nuts.size()-1]);
	cout << "j: " << j << endl;

	for(auto e : nuts) {
		cout << e << "  ";
	}
	cout << '\n';

	//printf("0 = %d\n", '0');

	//char a = 'a', b = 'b';
	// char arr[] = {'a', 'b'};
	// std::swap(arr[0], arr[1]);

	// for(auto c : arr) {
	// 	cout << c << "  ";
	// }
	// cout << '\n';


	return 0;
}