#include<iostream>
using namespace std;

/**
* Implement a custom pow() function:

* Hot favorite F2F Question of many companies:

* Power function takes base and second is exponent(which is raise to) & it will return base^n

Simplest method :

result = 1;

for i = 1; i<=n; ++i
	result = result * base

result will have pow

Complexity: 
Time: O(n). n is number which is power of base 

Divide & Conquer:

If i want to compute of power(5, 10) i.e 5^10

pow(5, 10) = pow(5, 5) * pow(5, 5);

We have divided n = n/2 and making two parts of power one is pow(base, n/2) & second is (base, n/2);

The answer would be same as per laws of indices:

5^5 * 5^5 
= 5^10

Recurrence Eqn:

It would be seen as, we have converted into 2T(n/2) but we have not converted, because we'll save answer of this pow(5, 5) and then multiply this answer with other half pow(5, 5) 's answer.

This means, we're not computing twice pow(5, 5) * pow(5, 5). We're computing only once. Because n/2 will remain same at both side. But this will work when n is even number. 
If n is odd number then it would not work. 

For ex: 2^7 = 128
Suppose we divide:
= 2^3 * 2^3 
= 8 * 8
= 64 
Jabki 2^7 = 128

SO this will not work odd number,

Odd Number Logic:
multiple with n/2 (func(n/2) * funct(n/2))
then joh bhi base hai n multiple with result of func(n/2) * funct(n/2).
For ex: 2^7 = 128
Suppose we divide:
= 2^3 * 2^3 
= 8 * 8
= 64 * base 
= 64 * 2 = 128

Algorithm:

power(base, n)
	if n is Even
		power(base, n/2) * power(base, n/2);
	if n is Odd
		power(base, n/2) * power(base, n/2) * base;
			^
		We have to store result of this we don't have to write again & again
		We have to call only once.

So time Complextiy
T(n) = T(n/2)
	
		n
	   /
	  n/2
	 /
	n/4
   /
   1 
it will go only one side 
So complexity, O(lgn) (height of tree).


**/

//O(n)
/*
int power(int base, int n) {

	int result = 1;
	for(long i= 1; i<=n; ++i) {
		result = result * base;
	}	
	return result;
}
*/
/**
* Time Complexity: O(lg n)
* Space: O(lg n): recursive stack
**/
long power(int base, int n) {

	long temp;
	if(n == 0) return 1;
	if(n == 1) return base;

	temp = power(base, n/2);

	if(n % 2 == 0) {
		return temp * temp;
	}
	return temp * temp * base;
}


int main() {

	int res = power(2, 8);
	cout << "res: " << res << endl;
}