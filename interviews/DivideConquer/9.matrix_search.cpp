#include<iostream>
#include<vector>
using namespace std;

/**
* Matrix Search:

* Given a matrix of nxn size and a number 'x', find the number x in it.
* The given matrix is sorted row wise as well as column wise.

Ex1:
I/P: [1 2 3]
	 [4 5 6]
	 [7 8 9]
	key = 5

O/p: true
**/
/*
bool helperSearch(vector<vector<int>>& matrix, int rowStart, int rowEnd, int colStart, int colEnd, int target) {

	if(rowStart <= rowEnd) {

		int mid = (rowStart + rowEnd) / 2;

		if(matrix[mid][mid] == target) {
			return true;
		}

		if(target < matrix[mid][mid]) {
			if(target <= matrix[mid-1][mid-1]){
				//search top-left
				return helperSearch(matrix, rowStart, mid-1, colStart, mid-1, target);
			} else if(target <= matrix[mid][mid-1]) {
				//search bottom left
				return helperSearch(matrix, rowStart+mid, rowEnd, colStart, colEnd-mid-1, target);
			} else if(target <= matrix[mid-1][mid]) {
				//search top-right
				return helperSearch(matrix, rowStart, mid-1, mid, colEnd, target);
			}
		} else {
			//target greater than mid
			if(target > matrix[rowStart][mid+1]) {
				//search in top right
				return helperSearch(matrix, rowStart, mid, mid+1, colEnd, target);
			} else if(target > matrix[mid+1][colStart]) {
				//search in bottom left
				return helperSearch(matrix, mid+1, rowEnd, colStart, mid-1, target);
			} else {
				//search in bottom right
				return helperSearch(matrix, mid-1, rowEnd, mid, colEnd, target);
			}
		}

	}
	return false;			
}
*/
/**
* binary search:

**/
bool binarySearchRecursively(vector<vector<int>>& matrix, int low, int high, int target) {

	int n = matrix.size();
	int m = matrix[0].size();

	if(high >= low) {
		int mid = low + (high - low) / 2;
		if(matrix[mid/m][mid%m] == target) {
			return true;
		} else if(target < matrix[mid/m][mid%m]) {
			//search left
			return binarySearchRecursively(matrix, low, mid-1, target);
		} else {
			//search right
			return binarySearchRecursively(matrix, mid+1, high, target);
		}

	}
	return false;
}
bool matrixSearch(vector<vector<int>>& matrix, int target) {
	int n = matrix.size();
	int m = matrix[0].size();

	int low = 0, high = n*m-1;
	
	while (low <= high) {
		int mid = low + (high - low) / 2;
		if(matrix[mid/m][mid%m] == target) {
			return true;
		} else if(target < matrix[mid/m][mid%m]) {
			//search left
			high = mid-1;
		} else {
			//search right
			low = mid + 1;
		}
	}
	return false;
}

int main() {

	cout<<std::boolalpha;

/*	vector<vector<int>> matrix = {
		{1, 5, 15, 20, 25},
		{10, 15, 20, 25, 30},
		{20, 25, 40, 45, 60},
		{30, 35, 60, 65, 90},
		{40, 45, 80, 85, 120}
	};
*/
	vector<vector<int>> matrix = {
		{1,3,5,7},
		{10,11,16,20},
		{23,30,34,60}
	};

	bool res = matrixSearch(matrix, 3);
	cout << res << endl;

	 res = matrixSearch(matrix, 13);
	cout << res << endl;

	return 0;
}