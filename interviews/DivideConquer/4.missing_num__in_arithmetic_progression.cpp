#include<iostream>
#include<vector>
using namespace std;

/**
* Missing Number in Arithmetic Progression:
*
* In some array arr, the values were in arithmetic progression: the values
 arr[i+1] - arr[i] are all equal for every 0 ≤ i ≤ arr.length-1. 

* Then, a value from arr was removed that was not the first or last in the  	array. 

* Return the removed value.

Ex1: 

I/P: [5,7,11,13]
O/P: 9

Ex2:

I/P: [15, 13, 12]
O/P: 14


LOGIC:

Arithmetic Progression: Joh first 2 element/number ke difference rahege n toh wohi number ke difference continue honge. 

	1, 3, 5, 7

This means their is difference of 2 between all elements. 

	1, 3, 5, 7, 11, 15,

We have skiped 9, so their is exactly one number which we have skipped, their are no multiple number which have been skipped. So we have to find that skipped number.
	

1. Get/Decide the progression number:

					0  1  2 3 4  5  6
					1, 3, 5 7 11 13 15

Start from 1, to get difference, so difference of arr[0] & arr[1] is 2.
But their is a drawback in this, i.e if arr[1] is missing then arr[0] & arr[2] ke bich ka difference 4 ka ho jayega. And we'll think that progression is working 4, which is not true.


So to get progression number their is a formula:
n- size/length of array

formula:
progression number = arr[n-1] - arr[0] 
					-------------------
						   n

ex: 
		15 - 1 / 7 = 2.

So, their is progression of 2 from above array.

Now, do linear search in array, and check whether next number is +2 or not. And where we stopped, return the element which was mising.

Complexity: O(n)

We have to reduce the complexity through binary search

If we're searching and progression is going from small number to big which means it is sorted and if we're searching on sorted array, that means kahi-kahi n kahi ham binary search laga sakte hai.  

How to apply binary search ?

	0  1  2 3 4  5  6
	1, 3, 5 7 11 13 15

low = 0
high = 6
mid = 3

we have to first check whether their is a missing number from low till mid.
which is 0 - 3 to do so, difference = 2.

mid * difference + arr[0] = mid value
3 * 2 = 6
6 + 1 = 7

This means their is no missing number from low - mid (0-3)

So, their should be some missing number from mid to high (3-6)

**/


int modifiedBinarySearch(vector<int>& nums, int low, int high, int diff) {

//	int diff = nums[nums.size()-1] - nums[0] / n;

	if(low >= high) {
		return -1;
	}

	int mid = (low + high) / 2;
	cout << "mid: " << nums[mid-1] + diff << endl;
	//the element before mid is missing
	if(mid > 0 && (nums[mid] - nums[mid-1]) != diff) {
		cout <<"m: "<< (nums[mid-1] + diff) << endl;
		return (nums[mid-1] + diff);
	}

	//the element just after the mid is missing
	if(nums[mid+1] - nums[mid] != diff) {
		return (nums[mid] + diff);
	}

	//if elements till mid follow Arithmetic Progression, then recur for right half.
	if(nums[mid] == (mid * diff + nums[0])) {
		return modifiedBinarySearch(nums, mid+1, high, diff);
	}

	//or recurs for left part
	return modifiedBinarySearch(nums, low, mid-1, diff);
}

int findMissingNumber(vector<int>& nums) {
	int sz_ = nums.size();
	int diff = (nums[sz_- 1] - nums[0]) / sz_;
	cout << "diff:" << diff << endl;
	return modifiedBinarySearch(nums, 0, nums.size()-1, diff);
}

int main() {

	vector<int> nums = {1, 3, 5, 7, 11, 13, 15};
	int res = findMissingNumber(nums);
	cout << res << endl;
	//9

	nums = {15, 13, 12};
	res = findMissingNumber(nums);
	cout << res << endl;
	//14
	return 0;
}