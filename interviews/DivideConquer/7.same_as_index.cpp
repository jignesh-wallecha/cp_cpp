#include<iostream>
#include<vector>
using namespace std;

/**
* Same as Index:

* Given an sorted array on non repeated integers a, check whether their is an index "i" for which will a[i] = i.

Ex1:
I/P: [-1, 0, 1, 3, 7, 8, 9, 10]
O/P: True.

Logic:

Their should be exactly one number in which a[i] = i, in array, so give that number.


The question tells us that it is a sorted array, we need to find one number(searching), so it is binary search.

  0  1  2  3  4  5  6  7
[-1, 0, 1, 3, 7, 8, 9, 10]

mid = 7 / 2 = 3

a[mid] == mid ? true : false;

Suppose, at mid there would have been 2, and idx is 3.

  0  1  2  3  4  5  6  7
[-1, 0, 1, 2, 7, 8, 9, 10]

As at mid element is 2, so left side from mid, their would be no element greater than or equal to a[mid]. So their would be no chance of matching idx with their element. So agar match ho sakta hai (idx with element) then it will be at right side of mid. Because it has exactly n numbers.


Note: Duplicates, are not allowed, if their is duplicate elements then we can't do binary search on a array.


Therefore, 

mid == a[mid], then we got it.

Otherwise,

mid > a[mid], search right side

mid < a[mid], search left side.


**/


bool sameIndex(vector<int>& nums, int low, int high) {

	while (low <= high) {

		int mid = (low + high) / 2;

		if(mid == nums[mid]) {
			return true;
		}
		if(mid > nums[mid]) {
			//search right
			low = mid + 1;
		} else if(mid < nums[mid]) {
			//search left
			high = mid - 1;
		}
	}
	return false;
}


int main() {

	cout << std::boolalpha;

	vector<int> nums = {-1, 0, 1, 3, 7, 8, 9, 10};

	bool res = sameIndex(nums, 0, nums.size()-1);
	cout << res << endl;
	//true
	
	nums = {1, 2, 3, 4, 5, 6, 7};	
	res = sameIndex(nums, 0, nums.size()-1);
	cout << res << endl;	
	//false
	
}