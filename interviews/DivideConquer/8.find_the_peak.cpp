#include<iostream>
#include<vector>
using namespace std;

/**
* Find the Peak:

Given an array arr, there exists index 'i' (1 <= i <= n) such that 
arr[0]...arr[i] is an increasing sequence and arr[i+1]...arr[n] is a decreasing sequence. Find the value of i.

Ex1:
I/P: [10, 20, 30, 25, 15]
O/P: 2

Ex2:
I/P: [1, 2, 3, 4, 5, 7, 6]
O/P: 5

Logic:

Approach 1: Linear Scan:

In this approach, we make use of the fact that two consecutive numbers nums[j] & nums[j+1] are never equal. Thus, we can traverse over the the nums array starting from the begining. Whenever, we find a number nums[i], we only need to check if it is larger than the next number nums[i+1] for determining if 
nums[i] is the peak element. The reasoning behind this can be understood by taking the following three cases which cover every case into which any problem can be divided.

Case1: All the numbers appeare in descending order. In this case, the first element corresponds to the peak element. We start off by checking if the current element is larger than the next one, The first element satisfies this criteria, and is hence identified as the peak correctly. In this case, we didn't reach a point where we needed to compare nums[i] with nums[i−1] also, to determine if it is the peak element or not.

Dia:
	  peak
 	5	*
		 \
 n	4	  *
		   \
 u	3		*  
			 \
 m	2		  * 
			   \
 s	1			*
				 \
	0		   	  *
		0 1 2 3 4 5
		 index


Case2: 
**/	

int findPeak(vector<int>& nums, int low, int high) {
	//if their is only one element
	if(low == high) {
		return low;
	}

	//if their are two elements then peak would be greater element amongst both element
	if(high == low+1 && nums[low] > nums[high]) {
		return low;
	} else if(high == low+1 && nums[high] > nums[low]) {
		return high;
	}
	
	int mid = (low + high) / 2;

	if(nums[mid] > nums[mid+1] && nums[mid] > nums[mid-1]) {
		//it is a peak point
		return mid;
	}

	if(nums[mid] < nums[mid+1]) {
		return findPeak(nums, mid+1, high);
	} 
	return findPeak(nums, low, mid-1);
}

int main() {

	vector<int> nums = {1, 2, 3, 4, 5, 7, 6};
	int res = findPeak(nums, 0, nums.size()-1);

	cout << res << endl;

	return 0;
}