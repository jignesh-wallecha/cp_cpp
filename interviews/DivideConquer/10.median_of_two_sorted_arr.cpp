#include<iostream>
#include<vector>
using namespace std;

/**
* Median of Two Sorted Arrays
* Given two sorted arrays nums1 & nums2 of size m & n respectively, return the median of the two sorted arrays.

Ex1: 
I/P: nums1 = [1, 3] , nums2 = [2]
O/P: 2.00000

Ex2:
I/P: nums1 = [1, 2] , nums2 = [3, 4]
O/P: 2.50000
**/

/**
* Approach 1: 
* Space: O(n+m)
**/

vector<int> merger(vector<int>& nums1, vector<int>& nums2) {
	vector<int> result(nums1.size() + nums2.size());
	int i = 0, j = 0, k = 0;

	while (i < nums1.size() && j < nums2.size()) {
		if(nums1[i] <= nums2[j]) {
			result[k++] = nums1[i++];
		} else {
			result[k++] = nums2[j++];
		}
	}

	while (i < nums1.size()) {
		result[k++] = nums1[i++];
	}

	while (j < nums2.size()) {
		result[k++] = nums2[j++];
	}

	return result;
}

double findMedianOfSortedArray(vector<int>& nums1, vector<int>& nums2) {

	vector<int> mergedArr = merger(nums1, nums2);

	int mid = mergedArr.size() / 2;
//	cout << "mid: " << mergedArr[mid] << endl;

	if(mergedArr.size() % 2 == 0) {
		return (double) (mergedArr[mid-1] + mergedArr[mid]) / 2;
	}
	return (double) mergedArr[mid];
}

int main() {

	vector<int> nums1 = {1, 2};
	vector<int> nums2 = {3, 4};


	double res = findMedianOfSortedArray(nums1, nums2);
	cout << res << endl;

	nums1 = {1, 3};
	nums2 = {2};
	res = findMedianOfSortedArray(nums1, nums2);
	cout << res << endl;

}	