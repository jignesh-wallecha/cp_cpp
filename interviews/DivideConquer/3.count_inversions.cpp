#include<iostream>
#include<vector>
using namespace std;

/**
* Count Inversions:

Given an array of integers, our job is to count the number of inversions present in the array.

Inversion as if you get smaller number after bigger number (it may not be consecutive)

Ex 1:
I/P: [7, 5, 1, 3, 4, 6]
O/P: 8

Logic:

Kitne nums hai joh 7 ke badh 7 se chote aha rahe hai ? And same it lies to every element/number from array

	7, 5, 1, 3, 4, 6 
	|  |  |  |	|  |
	5  3  0  0  0  0


Total : 8

Bruteforce Approach: n^2 hrr number ko dusre number se compare krna hai and it is n comparinson for n elements, therefore complexity is O(n^2).


We' can't think of sorting, if we sort then their will no inversion to see.

Divide-and-conquer approach:
We'll use mergesort. We play at merging time. 

1. Divide the array 

			7, 5, 1, 3, 4, 6 
					|
				 /	   \
			7, 5, 1    3, 4, 6
			    |		   |
			  /   \      /   \
			7,5    1   3,4   6
			 |			|
			/ \        / \
		   7  5       3	 4


2. Conquer: while conquering we'll count inversion.

Assume, all seperated elements are list. So 7 is list1 & 5 is list2 

1.
if list1.element > list2.element
	then at merging time sort the elements 
	and place in array. And count inversion as 1, because we say 1 inversion as list1.element > list2.element

We'll first check, then if their is inversion, we'll count then we sort the elements and place in array. 


Brackets mai inversion count hai, from below fig.

					7, 5, 1, 3, 4, 6 
							|
						 /	   \
					7, 5, 1    3, 4, 6
					    |		   |
					  /   \      /   \
					7,5    1   3,4   6
					 |	   |	|    |   
					/ \    |   / \   |
				   7  5    |  3	 4	 |
  				    \/(1)  |   \/	 |
				   [5, 7](2)(0)[3, 4]|
 					  \   /	       \/
				   [1, 5, 7]  [3, 4, 6] (0)

2. 
While at checking/merging 5,7 with 1, their is a twist as 5 > 1 so their is one inversion, but we'll not do like this we'll take lists size [5, 7] which is 2 and with 1 element it had two inversion. Because 5 > 1 so 7 > 1 as array [5,7] is sorted. So their is count 2 inversion for [5,7] with [1] 
and we got count 1 inversion from (7 > 5). So total count inversion is 3.
Therefore from [7, 5, 1] subarray we got three inversion.

3.			
3.1: arr[i] < arr[j]
		inversion count = 0
		copy arr[i] in new temp array 
		and incr i ptr.

				 [1, 5, 7]  [3, 4, 6]
				  ^ 		 ^
				  i	         j
		 (0)	    \       / 
				 	 \     /
				 	 [1,       ]



3.2: arr[i] > arr[j]
		their is inversion, increment inversion count = 1
		and copy from file2 which is 3
		but before shifting j ptr to next element, their is a twist 
		as from 5 elements onwards which 7 is also greater than 3 so their is a inversion so before shifting we'll count inversion = 2. Because 3 is making inversion with 5 & 7. And after this move j ptr forward.


				[1, 5, 7]  [3, 4, 6]
				    ^ 		 ^
				    i	     j
		(2)		   	\       / 
				 	 \     /
				 	[1 |3 |      ]
	
3.3: arr[i] > arr[j] (5 > 4 & 7 > 4)
		so their are two inversions 
		and put elements from file2 which is arr[j] 
		to tempArray.

					[1, 5, 7]  [3, 4, 6]
					    ^ 		   ^
					    i	       j
		(2)			   	\         / 
					 	 \       /
					 	[1 |3 | 4 |   ]


3.4: arr[i] < arr[j] (5 < 6) 
		so no inversion
		copy 5 and incr i ptr.
				


					[1, 5, 7]  [3, 4, 6]
					    ^ 		      ^
					    i	          j
		(0)			   	\            / 
					 	 \          /
					 	[1 |3 | 4 | 5  ]

3.5: 			
	arr[i] > arr[j] (7 > 6)
		their is inversion count = 1.
		copy 6 element in tempArray 
		and incr j ptr.



					
					[1, 5, 7]  [3, 4, 6]
					       ^ 		  ^
					       i	      j
		(1)			   	   \         / 
					 	    \       /
					 	[1 |3 | 4 | 5 | 6 ]

Now file 2 has been completed, as mergesort algos says copy remaining elements from file if other has completed.
	
final sorted:			[1, 3, 4, 5, 6, 7]


And sums all inversion count which will be 8.

 
We applied mergesort technique to solve this problem.

Complexity: 
Time: O(n lg n)

Simply, while merging, if we're picking data from file2 and puting to file3 then their is inversion, So while file2 we need to add inversion count. 

So their would be:

		formula: (mid - i) inversions. 
**/


int merge(vector<int>& arr, vector<int>& tempArr, int low, int mid, int high) 
{

	int inversionCount = 0;
	int i = low;
	int j = mid + 1;
	int k = low;

	while (i <= mid && j <= high) {
		if(arr[i] <= arr[j]) {
			tempArr[k++] = arr[i++];
		} else {
			tempArr[k++] = arr[j++];
			inversionCount = inversionCount + (mid-i);
		}
	}

	while (i <= mid) {
		tempArr[k++] = arr[i++];
	}

	while (j <= high) {
		tempArr[k++] = arr[j++];
	}
	
	return inversionCount;
}

/*
int mergesort(vector<int>& arr, vector<int>& tempArr, int low, int high) {
	int inversionCount = 0;

	if(high > low) {
		int mid = (high + low) / 2;

		inversionCount += mergesort(arr, tempArr, low, mid);
		inversionCount += mergesort(arr, tempArr, mid+1, high);

		inversionCount += merge(arr, tempArr, low, mid, high);
	}
	return inversionCount;
}

int countInversions(vector<int>& arr) {
	vector<int> tempArr(arr.size());
	return mergesort(arr, tempArr, 0, arr.size()-1);
}
*/

int countInversions(vector<int>& arr, vector<int>& tempArr, int low, int high) 
{
	if(low >= high){  
		return 0;
	}
	int inversionCount = 0;
	int mid = (low + high) / 2;
	
	countInversions(left, tempArr, 0, mid) + countInversions(right)
	return inversionCount;
}


int main() {

	vector<int> arr = {7, 5, 1, 3, 4, 6};
	vector<int> tempArr(arr.size());
	int res = countInversions(arr, tempArr, 0, arr.size()-1);
	cout << res << endl;
	//8

/*	countInversions(arr, tempArr, 0, arr.size()-1);
	for(auto e : arr) {
		cout << e <<"  ";
	}
	cout << '\n';
*/
	return 0;
}