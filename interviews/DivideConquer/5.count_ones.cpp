#include<iostream>
#include<vector>
using namespace std;

/**
* Count One's :

Given an array containing only 1's and 0's in which 0's appears before 1's,
count the number of 1's.

Ex1:
I/P: [0, 0, 0, 0, 1, 1, 1, 1, 1, 1]
O/P: 6

Logic:
	
	 0  1  2  3  4  5  6  7  8 9
	[0, 0, 0, 0, 1, 1, 1, 1, 1, 1]
					^
					mid
If we're using binary search we have to stop at first one, which is at idx4, from above array.


So mid is at idx 5, so we don't have to stop here because it is not the first one as nums[mid-1] = 1. So we'll take high to mid-1.

	 0  1  2  3  4  5  6  7  8 9
	[0, 0, 0, 0, 1, 1, 1, 1, 1, 1]
	 ^	   ^	 ^	
	 l	   m	 h	
l = 0, h = 4, mid = 2.

as at nums[mid] = 0, so low will be shifted(by mid+1), as we can get 1 further, so we're taking low ptr upwards. 

	 0  1  2  3  4  5  6  7  8 9
	[0, 0, 0, 0, 1, 1, 1, 1, 1, 1]
	 	      ^	 ^	
	 	      l	 h
	 	      m

l = 3, h = 4, mid = 3

low = mid + 1

	 0  1  2  3  4  5  6  7  8 9
	[0, 0, 0, 0, 1, 1, 1, 1, 1, 1]
	 	      	 ^	
	 	      	l h
	 	      
Now, both low and high ptr are pointing at same element. So either element would be zero or one.

And if (nums[low] == 1) {
	return n - low;
}

so this will return num of count of ones. As low is idx where we got our first one, which is index of one.






**/

int binarySearch(vector<int>& nums, int low, int high) {

	while (low <= high) {
		int mid = (low + high) / 2;

		if(nums[mid] == 1 && nums[mid-1] == 0) {
			return mid;
		} else if(nums[mid] == 1 && nums[mid-1] == 1) {
			high = mid - 1;
		}  else {
			low = mid + 1;
		}
	}
	return 0;
}

int binarySearchRecursively(vector<int>& nums, int low, int high) {

	if(high >= low) {

		int mid = (low + high) / 2;

		if(nums[mid] == 1 && nums[mid-1] == 0) {
			return mid;
		} else if(nums[mid] == 1 && nums[mid-1] == 1) {
			return binarySearchRecursively(nums, low, mid-1);
		}
		return binarySearchRecursively(nums, mid+1, high);
	}
	return 0;
}

int countOnes(vector<int>& nums) {
	//int low = binarySearchRecursively(nums, 0, nums.size()-1);
	int low = binarySearch(nums, 0, nums.size()-1);
	if(low != 0 && nums[low] == 1) {
		return (nums.size() - low);
	}
	return 0;
}

int main() {

	vector<int> nums = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
	int res = countOnes(nums);
	cout << res << endl;
	//6

	nums = {0, 0, 0, 0, 1};
	res = countOnes(nums);
	cout << res << endl;
	//1

	nums = {0, 0, 0, 0};
	res = countOnes(nums);
	cout << res << endl;
	//0

	nums = {};
	res = countOnes(nums);
	cout << res << endl;	

}