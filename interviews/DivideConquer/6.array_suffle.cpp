#include<iostream>
#include<vector>
using namespace std;

/**
* Array Shuffle: Given an array of 2n integers in the foll format a1, a2, a3,...an, b1, b2, b3...bn. Shuffle the array to a1, b1, a2, b2...an,bn.

Ex1:
I/P: [1, 2, 3, 4, 5, 6, 7, 8];
O/P: [1 5 2 6 3 7 4 8];

* Logic:

By two pointers it was O(n) for space & time.

But in interview they can say that their is no memory so try making algo memory efficient. So it is fine to increase the time complexity for compensenting the space. 


Divide and Conquer:
We'll use the mergesort's partition logic in this.

1.	a1, a2, a3, a4, | b1, b2, b3, b4
				   / \
	a1, a2, a3, a4     b1, b2, b3, b4

Get the half of both the files or subarrays before breaking the array. After getting both the half swap the values of 1 half with 2 half.


	a1, a2, a3, a4, | b1, b2, b3, b4
				   / \
	a1, a2,| a3, a4     b1, b2,| b3, b4
			 ^  		^ - this is mid + 1
			 l          t

swap nums[l] with nums[t] and vice versa. and after swapping increment both the pointers swap until it is end of their array.

	a1, a2, a3, a4, | b1, b2, b3, b4
				   / \
	a1, a2,| b1, a4     a3, b2,| b3, b4
			     ^  		^
			     l          t

	a1, a2, a3, a4, | b1, b2, b3, b4
				   / \
	a1, a2,| b1, b2     a3, a4,| b3, b4
			       ^  		  ^
			       l          t



2.	a1, a2, a3, a4, | b1, b2, b3, b4
				   / \
	a1, a2,| a3, a4     b1, b2,| b3, b4

	a1, a2,| b1, b2     a3, a4,| b3, b4
		  /	\      			  / \
a1,| a2	 /   \ b1,| b2 a3|a4 /   \ b3,| b4 

 	a1, b1,    a2,b2     a3,b3,  	a4,b4


Jabh 2 ka pair mile stop, don't go further. 
Answer is a1,b1,a2,b2,a3,b3,a4,b4

Swapping krne ke liye ekk temp variable lagega.

**/

/**
* Complexity:
* Space: O(lg n): height of tree created
* Time: O(n lg n): n is for n swapping as it is n/2 times swapping but it is considered as O(n) and O(lg n) for partition the array and making O(lg n) height of tree.
**/
void shuffleArray(vector<int> &arr, int left, int right) {

	//base boundary if there are 2 elements each, as it should recursive further (1-0 = 1 or 3-2 = 1), this means there 2 elements.
	if(right - left == 1) {
		return;
	}

	int mid = (left + right) / 2;
	//right side's first element.
	int temp = mid + 1;

	//get left side's mid
	int leftMid = (left + mid) / 2;
	for(int i = leftMid+1; i<=mid; ++i) {
		swap(arr[i], arr[temp]);
		++temp;
	}

	shuffleArray(arr, left, mid);
	shuffleArray(arr, mid+1, right);
}

int main() {

	vector<int> nums = {1, 2, 3, 4, 5, 6, 7, 8};
	shuffleArray(nums, 0, nums.size()-1);
	for(const auto& e : nums) {
		cout << e <<" ";
	}
	cout << "\n";

	return 0;
}