#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;

/**
* Course Schedule:
There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return true if you can finish all courses. Otherwise, return false.

Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: true
Explanation: There are a total of 2 courses to take. 
To take course 1 you should have finished course 0. So it is possible.

Example 2:

Input: numCourses = 2, prerequisites = [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take. 
To take course 1 you should have finished course 0, and to take course 0 you should also have finished course 1. So it is impossible.
 
Constraints:

1 <= numCourses <= 105
0 <= prerequisites.length <= 5000
prerequisites[i].length == 2
0 <= ai, bi < numCourses
All the pairs prerequisites[i] are unique.

**/

const int WHITE = 0;
const int GRAY = 1;
const int BLACK = 2;

unordered_map<int, vector<int>> adj_mat;

bool dfs(int vertex, vector<int>& visited) {
	visited[vertex] = GRAY;

	for(auto neighbour_vertex : adj_mat[vertex]) {
		if(visited[neighbour_vertex] == GRAY) {
			return true;
		}

		if(visited[neighbour_vertex] == WHITE && dfs(neighbour_vertex, visited)) {
			return true;
		}
	}
	visited[vertex] = BLACK;
	return false;
}

bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
	if(numCourses == 0) return false;

	vector<int> visited(numCourses, WHITE);

	//create graph
	for(auto edge : prerequisites) {
		int vertex1 = edge[0];
		int vertex2 = edge[1];
		adj_mat[vertex1].push_back(vertex2);
	}

	for(int vertex = 0; vertex < numCourses; ++vertex) {
		if(visited[vertex] == WHITE && dfs(vertex, visited)) {
			//if their is cycle then we cannot finish the course so return false
			return false;
		}
	}
	return true;

}

int main() {

	cout << std::boolalpha;
	int numCourses = 2;
	vector<vector<int>> prerequisites = {
		{1, 0},
		{0, 1}
	};

	bool res = canFinish(numCourses, prerequisites);
	cout << res << endl;

	numCourses = 2;
	prerequisites = {
		{1, 0}
	};

	res = canFinish(numCourses, prerequisites);
	cout << res << endl;

	return 0;
}