#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;

/**
Graph is amazon's hot favorite question in interview:


* Detect Cycle in Directed Graph

* Given n nodes labelled from 0 to n-1 and list of directed edges, write a 
* function to find whether the graph contains the cycle or not.

Ex1: 
I/P: n = 5, edges = [[0,1], [1, 2], [2, 3], [3, 4], [4, 0]]
O/P: true

Logic:

 no cycle
(0)----->(1)
 
 cycle
(0)----->(1)
  ^	 	 /
   \----/ 


In below, graph their is a twist:
0's adjacent is 1 & 2, 1's adjacent is 2 but 2 has no adjacent

we start from 0 marked it visited, then go to 0's adj i.e 1 marked that visited
& then go to 1's adjacent 2 marked it visited & then backtrack as their is no
2's adjacent but 0 has one for adjacent vertex which is 2 but it is already 
visited so their is cycle in this graph.

(0)->(2)
 |	 /
 |	/
 | /
(1)

We will not use DFS/BFS

So to solve this we'll keep three state 
WHITE - not visited
GRAY - visiting
BLACK - completed visited means uske adjacent bhi visit ho chaku hai yaha toh 
		visiting hai

By default state of whole graph's vertices is WHITE


Algorithm:

1. Create an adjacency list or adjacency matrix from the given edges
2. Create a visited variable, initially all set to WHITE
3. Create a modified DFS which excepts a 'vertex' & 'visited'
	3.1. Modify visited of current vertex to 'GRAY'
	3.2. for each neighbour of the current vertex:
		3.2.1. If neighbour's visited status is GRAY then return true
		3.2.2. If neighbour's visited status is WHITE, initiate a new DFS from
			   that vertex and return true if this DFS return true

	3.3. Modify the visited status of the current vertex to BLACK 
	3.4. return false
4. Run the DFS on all vertex with WHITE visited status, return true if any 
	DFS call returns true
5. return false.

**/

const int WHITE = 0;
const int GRAY = 1;
const int BLACK = 2;

unordered_map<int, vector<int>> adj_mat;

bool dfs(int vertex, vector<int>& visited) {
	visited[vertex] = GRAY;

	for(auto neighbour_vertex : adj_mat[vertex]) {
		if(visited[neighbour_vertex] == GRAY) {
			return true;
		}

		if(visited[neighbour_vertex] == WHITE && dfs(neighbour_vertex, visited)) {
			return true;
		}
	}
	visited[vertex] = BLACK;
	return false;
}

bool detectCycle(int n, vector<vector<int>>& edges) {
	if(n == 0) return false;

	vector<int> visited(n, WHITE);

	//create graph
	for(auto edge : edges) {
		int vertex1 = edge[0];
		int vertex2 = edge[1];
		adj_mat[vertex1].push_back(vertex2);
	}

	for(int vertex = 0; vertex < n; ++vertex) {
		if(visited[vertex] == WHITE && dfs(vertex, visited)) {
			return true;
		}
	}
	return false;
}

int main() {

	cout << std::boolalpha;
	int n = 5;
	vector<vector<int>> edges = {
		{0, 1},
		{1, 2},
		{2, 3},
		{3, 4},
		{4, 0}
	};
	
	bool res = detectCycle(n, edges);
	cout << res << endl;

	return 0;
}
