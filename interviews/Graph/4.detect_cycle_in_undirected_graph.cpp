#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;

/**
* Detect Cycle in Undirected Graph:
Given n nodes labelled from 0 to n-1 and a list of undirected edges (each edge is a pair of nodes), write a function to find whether the graph contains the cycle or not. 

Ex1:
I/P: n = 5, & edges = [[0,1], [1,2], [2, 3], [3, 1], [4, 0]]
O/P: true


LOGIC:
	
 G.        G 
(0) ------(1)

If now we see 1 node adjacent which is 0 but it is gray so, their is a cycle. But why we came to 1 node becuase 0 node is its parent. So, 0 would be acting as parent to 1.

So when i go to 1 and start dfs from 1, then keep the status that 1's parent node is 0. Because when 1 would be looking for adjacent and when 0 came as adjacent to 0, so their is a cycle in it but no 0 is my parent node, so their is no cycle for 0 as it is parent node of 1.

So we need to keep a track on parent itself.

For ex:

1. no cycle:

true		true
(0)----------(2) [2 : 0]
 |
 |
 |
 |
(1)true [1 : 0]

When we go to neighbour_vertex 2 of 0 vertex then we'll keep track of parent
by [2 : 0] as 0 vertex is parent of 2 vertex. When 2 will look for its adjacent vertex it will get 0 vertex but 0 is my parent so their is no cycle. As their is no more adjacent vertex of 2 so it will be backtrackted and go to 0 vertex which has still adjacent_vertex 1 it will go their and mark as visited. The program will be end here, becuase their is no adjacent of 1 and all vertices have been visited. So, their is no cycle in this graph.


2. cycle: start from 0 vertex, true is symboled as (T)
		
 T 			 T
(0)---------(1) [1 : 0]
 |			 |
 |			 |
 |			 |
(3)---------(2) [2 : 1]
 T			 T
[3 : 2]

As '3' vertex's parent is 2, so we will not go their/try to visit '2' but 3's adjacent is also '0' vertex which is not parent of '3' but it is already visited so their is a cycle in this graph.


Algorithm:
1. create adjList or adjMat for given graph.
2. create visited variable, and initially set all false.
3. create a modified DFS which accepts vertex (source), parent, & visited 
	3.1: Modify visited of the current vertex to 'TRUE'.
	3.2: For each neighbour_vertex of current vertex
		3.2.1: If neighbour_vertex's visited status is FALSE 
				then call dfs(neighbour, currentVertex as parent, if dfs returns TRUE then return TRUE)
			  else if neighbour_vertex visited status is TRUE 
			  	then if neighbour_vertex != parent
			  		then return true
	3.3: return false
4. Run the DFS on all vertex with status OF FALSE, return true if any DFS calls return TRUE 
5. return false;
**/

unordered_map<int, vector<int>> adj_mat;

bool dfs(int vertex, int parent, vector<bool>& visited) {
	visited[vertex] = true;

	for(auto& adj_vertex : adj_mat[vertex]) {
		if(!visited[adj_vertex] ) {
			if(dfs(adj_vertex, vertex, visited)) {
				return true;
			}
		} else if(visited[adj_vertex] == true) {
			if(adj_vertex != parent) {
				return true;
			}
		}
	}
	return false;
}


bool detectCycleInUndirectedGraph(int n, vector<vector<int>>& edges) {

	if(n == 0) return false;
	//keep a track of discovered vertices
	vector<bool> visited(n, false);

	//create adj_mat of undirected graph
	for(auto& edge : edges) {
		int vertex1 = edge[0];
		int vertex2 = edge[1];
		adj_mat[vertex1].push_back(vertex2);
		adj_mat[vertex1].push_back(vertex2);
	}

	for(int vertex = 0; vertex < n; ++vertex) {
		if(!visited[vertex]) {
			if(dfs(vertex, -1, visited)) {
				return true;
			}
		}
	}
	return false;
}

int main() {

	cout << std::boolalpha;
	vector<vector<int>> edges = {
		{0, 1},
		{1, 2},
		{2, 3},
		{3, 1},
		{4, 0}
	};

	int n = 5;

	bool res = detectCycleInUndirectedGraph(n, edges);
	cout << res << endl;
	//true

	return 0;
}
