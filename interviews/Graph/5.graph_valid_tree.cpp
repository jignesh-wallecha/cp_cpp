#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;

/**
* Graph Valid Tree:
* Given n nodes labelled from 0 to n-1 and a list of undirected edges (each edge is a pair of nodes), write a function to check whether these edges make up a valid tree.

EX1:
I/P: n = 5, edges = [[0,1], [0,2], [0,3], [1,4]]
O/P: true

Ex2:
I/P: n = 5, edges = [[0,1], [1,2], [2,3], [1, 3], [1,4]]
O/P: false


LOGIC:
All trees are graph, but all graphs are not trees.

Can we see below given graph as tree ?

(0)----------(1)
 |
 |
 |
 |
(2)
-> Yes 

But we can see this as a tree.

(0)----(1)
 |	   /
 |	  /
 |	 /
 |	/
(2)


So, jabh tak graph ke pass cycle nhi hai n toh woo tabh tak tree hai.
But above given fig, is a graph but not a tree.

So, every graph is a tree if it does not have any cycle, and it should be a one component(it should not have multiple components). 


**/

unordered_map<int, vector<int>> adj_mat;

bool dfs(int vertex, int parent, vector<bool>& visited) {
	visited[vertex] = true;

	for(auto& adj_vertex : adj_mat[vertex]) {
		if(!visited[adj_vertex] ) {
			if(dfs(adj_vertex, vertex, visited)) {
				return true;
			}
		} else {
			if(adj_vertex != parent) {
				return true;
			}
		}
	}
	return false;
}


bool validTree(int n, vector<vector<int>>& edges) {

	if(n == 0) return false;
	//keep a track of discovered vertices
	vector<bool> visited(n, false);

	//create adj_mat of undirected graph
	for(auto& edge : edges) {
		int vertex1 = edge[0];
		int vertex2 = edge[1];
		adj_mat[vertex1].push_back(vertex2);
		adj_mat[vertex1].push_back(vertex2);
	}

	if(dfs(0, -1, visited)) {
		//if dfs returns true then their is a cycle in graph & and so it is not a valid tree.
		return false;
	}

/*	for(auto visit : visited) {
		//agar koi vertex visit nhi huha toh their is false, and it is false becuase their is another components/dusra component rahega. So it is not a valid tree
		if(!visit) {
			return false;
		}
	}
*/
	for(int vertex = 0; vertex < n; ++vertex) {
		if(!visited[vertex]) {
			return false;
		}
	}
	//agar kuch nhi raha then it is a valid tree so return true
	return true;
}

int main() {

	cout << std::boolalpha;
/*	int n = 5;
	vector<vector<int>> edges = {
		{0, 1},
		{1, 2},
		{2, 3},
		{1, 3},
		{1, 4}
	};

	bool res = validTree(n, edges);
	cout << res << endl;
	//false
*/
	int n = 5;
	vector<vector<int>> edges = {
		{1, 0},
		{0, 2},
		{0, 3},
		{3, 4}
	};

	bool res = validTree(n, edges);
	cout << res << endl;
	//true

	return 0;
}