#include<iostream>
#include<vector>
#include<unordered_map>

using namespace std;
/**
* Question: All Paths From Source to Target
*
* Algorithm:
  1: We need helper function which will use Approach of DFS to get All paths from 
     that source.
  2: set source of graph as discovered/true & then push that source node to 
     path vector
  3: Check whether source isEquals to n-1 of graph ? 
     3.1: if yes, then push the path vector in allPaths vector as we 
          have reached the target.
     3.2: if no, then get the adjVertex of source node,
     	  if adjVertex is not discovered/false then start dfs from that 
     	  adjVertex.
  4. As now, source == n-1, so we'll backtrack as their is no further adjacent
    vertex of that source, and make that source as false.

  5. Now by help of this helper function we will get allPath from source to 
     target.
*/

void getAllPaths(int source, vector<bool>& discovered, vector<int>& path, vector<vector<int>>& allPaths, vector<vector<int>>& graph) {
	discovered[source] = true;
	path.push_back(source);

	if(source == graph.size()-1) {
		allPaths.push_back(path);
	} else {
		for(auto neighbourVertices : graph[source]) {
			if(discovered[neighbourVertices] == false) {
				getAllPaths(neighbourVertices, discovered, path, allPaths, graph);
			}
		}
	}

	//backtrack
	path.pop_back();
	discovered[source] = false;
}

vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph) {
    vector<int> path;
	vector<vector<int>> allPaths;
    vector<bool> discovered(graph.size(), false);

    //get all paths from source
    getAllPaths(0, discovered, path, allPaths, graph);  
    return allPaths;
}


unordered_map<int, vector<int>> adjMat;
void createGraphMap(vector<vector<int>>& graph) {
	//create graph
    for(int i =0; i<graph.size(); ++i) {
        for(int j = 0; j<graph[i].size(); ++j) {
            int vertex =graph[i][j];
            adjMat[i].push_back(vertex);
        }
    }
}

void printGraphMap(vector<vector<int>>& graph) {
	//print graph
    for(int i =0; i<adjList.size(); ++i) {
        cout << "vertex: " << i;
        vector<int> neighbourVertices = adjMat[i];
        for(int j = 0; j<neighbourVertices.size(); ++j) {
            cout << " " << adjMat[i].at(j) <<"  ";
        }
        cout << '\n';
    }
}

int main() {

	vector<vector<int>> graph = {
		{1, 2},
		{3},
		{3},
		{}
	};

	cout << "first input:\n";
	vector<vector<int>> allPaths = allPathsSourceTarget(graph);
	for(vector<int> path : allPaths) {
		cout << "[ ";
		for(int v : path) {
			cout << v <<" ";
		}
		cout << "]\n";
	}

	graph = {
		{4, 3, 1},
		{3, 2, 4},
		{3},
		{4},
		{}
	};

	cout << "Second input:\n";
	allPaths = allPathsSourceTarget(graph);
	for(vector<int> path : allPaths) {
		cout << "[ ";
		for(int v : path) {
			cout << v <<" ";
		}
		cout << "]\n";
	}
}