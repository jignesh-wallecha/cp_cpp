#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

unordered_map<int, vector<pair<int, int>>> adj_mat;
int networkDelayTime(vector<vector<int>>& times, int n, int k) {
    //create graph
	for (auto& set : times) {
		adj_mat[set[0]].push_back({set[1], set[2]});
	}
	
	cout << "printing adj_mat...\n";
	for (auto& pair : adj_mat) {
		cout << pair.first << " -> ";
		for (auto& p : pair.second) {
			cout << p.first << "time taken: " << p.second << " -> ";
		} 
		cout << endl;
	} 
    return -1;
}

int main() 
{
	vector<vector<int>> times = {
		{2, 1, 1},
		{2, 3, 1},
		{3, 4, 1}
	};

	int n = 4, k = 2;

	networkDelayTime(times, n, k);

	return 0;
}
