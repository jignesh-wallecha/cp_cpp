#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;

/**
* No. of Connected Components in an undirected Graph:
* Given an n nodes labelled from 0 to n-1 and a list of undirected edges 
* (each edge is a pair of nodes), write a function to find the no. of 
* connected components in an undirected graph.
*
Logic:
What is Component ?
edges = [[0, 1], [1, 2], [3, 4]]

(0)-(1)
	/	    (4)
   /	   /
(2)		 (3)

So 0, 1, 2 is one component and [4, 3] is another component
So their are 2 total components in whole graph.

**/

unordered_map<int, vector<int>> adj_mat;

void dfs(int vertex, vector<bool>& visited) {
	visited[vertex] = true;
	for(auto neighbour_vertex : adj_mat[vertex]) {
		if(!visited[neighbour_vertex]) {
			dfs(neighbour_vertex, visited);
		}
	}
}

int countComponents(int& n, vector<vector<int>>& edges) {

	if(n == 0) {
		//their is no components.
		return 0;
	}
	int count = 0;
	vector<bool> visited(n, false);

	//make a graph i.e adjList or adjMatrix
	for(auto edge : edges) {
		int vertex1 = edge[0];
		int vertex2 = edge[1];
		adj_mat[vertex1].push_back(vertex2);
		adj_mat[vertex2].push_back(vertex1);
	}

	for(int vertex = 0; vertex < n; ++vertex) {
		if(!visited[vertex]) {
			dfs(vertex, visited);
			++count;
		}
	}
	return count;
}

int main() {

	int n = 5;
	vector<vector<int>> edges = {
		{0, 1},
		{1, 2},
		{3, 4},
	};

	int res = countComponents(n, edges);
	cout << res << endl;

	n = 5;
	edges = {
		{0, 1},
		{1, 2},
		{2, 3},
		{3, 4},
	};	

	res = countComponents(n, edges);
	cout << res << endl;

	return 0;
}