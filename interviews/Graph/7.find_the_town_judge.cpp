#include <iostream>
#include <vector>
using namespace std;

//using in out degree logic of graph
int findJudge(int n, vector<vector<int>>& trust) {
        vector<int> degree(n+1);
        
        for (int i = 0; i < trust.size(); ++i) {
            degree[trust[i][0]]--;
            degree[trust[i][1]]++;
        }
        
        for (int i = 1; i <= n; ++i) {
            if (degree[i] == n-1) {
                return i;
            }
        }
        return -1;
    }

int main() 
{

	vector<int> trust = {
		{1, 3},
		{2, 3}
	};
	int n = 3;
	int res = findJudge(n, trust);
	cout << res << endl;
	return 0;
}