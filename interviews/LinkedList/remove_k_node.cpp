#include<iostream>
#include<iostream>
using namespace std;
struct Node {
	int data;
	struct Node *next;
};

typedef struct Node ListNode;
ListNode *head = NULL;

ListNode* createNode() {
    ListNode *pnode = (ListNode*) malloc(sizeof(ListNode));
    if(pnode == NULL) return pnode;
    cout << "Enter Data:\n";
    cin >> pnode->data;
    pnode->next = NULL;
    return pnode;
}

void insertAtEnd() {
	ListNode *pnode, *current;
	pnode = createNode();
	if(head == NULL) {
		head  = pnode;
	} else {
		current = head;
		while(current->next) {
			current = current->next;
		}
		current->next = pnode;
		current = current->next;
	}
}

void removeKthNode(int k) {
	ListNode *current;
	// int length = 0;
	// while(head) {
	// 	length++;
	// 	head = head->next;
	// }
	// current = head;
	// while(length > 0) {
	// 	length--;
	// 	current = current->next;
	// }
	// cout << current->data << endl;
	// length -= k;
	// cout << length << endl;
	while(k > 0) {
		k--;
		head = head->next;
	}
	cout << head->data << endl;
}

ListNode* find3rdLastNode() {

	ListNode *slow, *fast;
	slow = head;
	fast = head->next->next; 
	while(fast->next) {
		fast = fast->next;
		slow = slow->next;
	}
	return slow;
}

ListNode* removekthLastNode(int k) {
	ListNode *slow, *fast;
	slow = head;
	//fast will point to kth node
	fast = head;
	for(int i=0; i<k; ++i) {
		fast = fast->next;
	}
	while(fast->next) {
		fast = fast->next;
		slow = slow->next;
	}
	// cout << fast->data <<endl;
	// cout << slow->data << endl;
	head->next->next = slow->next;
	return head;
}
int main() {

	for(int i=0; i<5; ++i) {
		insertAtEnd();
	}
	// ListNode* res = find3rdLastNode();
	// cout <<"Third last Node: "<< res->data << endl;

	ListNode* res = removekthLastNode(2);
	while(res) {
		cout << res->data << endl;
		res = res->next;
	}
}