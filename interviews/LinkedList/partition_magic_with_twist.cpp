#include<iostream>
using namespace std;
struct Node {
	int data;
	struct Node *next;
};

typedef struct Node ListNode;
ListNode *head = NULL;
ListNode *tail = NULL;

ListNode* createNode() {
    ListNode *pnode = (ListNode*) malloc(sizeof(ListNode));
    if(pnode == NULL) return pnode;
    cout << "Enter Data:\n";
    cin >> pnode->data;
    pnode->next = NULL;
    return pnode;
}

void insertAtBeg() {
	ListNode *pnode = createNode();
	if(head == NULL) {
		head = pnode;
		tail = pnode;
	} else {
		pnode->next = head;
		head = pnode;
	}
}
void insertAtEnd() {
	ListNode *pnode, *current;
	pnode = createNode();
	if(head == NULL) {
		head = tail = pnode;
	} else {
		current = head;
		while(current->next) {
			current = current->next;
		}
		current->next = pnode;
		tail = current->next;
	}
}

// ListNode* partitionMagic(int x) {
// 	ListNode* current;
// 	current = first;
// 	while(current < tail) {
// 		if(current->data < x) {
// 			//less than x
// 			current->next = first;

// 		} else {
// 			//greater than x
// 			tail->next = current
// 			current->next = current->next->next;

// 		}
// 	}
// }

void traverse_list() {
        ListNode *current;
        current = head;
        while(current) {
		    cout << current->data <<"  ";
		    current = current->next;
	    }
	    cout << '\n';
    }  


int main() {

	for(int i=0; i<4; ++i) {
		insertAtBeg();
	}
	traverse_list();
	cout << "head: " << head->data << endl;
	cout << "Tail: " << tail->data << endl;

	return 0;
}