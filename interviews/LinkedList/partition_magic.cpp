#include<iostream>
using namespace std;

/**
* Que: Partition Magic:
* Write a code to partiton linked last 
**/

struct Node {
    int data;
    struct Node *next;
};

class ListNode {
     Node *first;
public:
    ListNode() : first(NULL) {};

    Node* createNode() {
        Node* pnode = new Node();
        if(pnode == NULL) return pnode;
        cout << "Enter Data:\n";
        cin >> pnode->data;
        pnode->next = NULL;
        return pnode;
    }
    void insertAtEnd(Node *pnode) {
        //pnode = createNode();
        Node *current;
        if(first == NULL) first = pnode;
        else {
            current = first;
            while(current->next) {
                current = current->next;
            }
            current->next = pnode;
            current = current->next;
            cout <<"current: " << current->data << endl;
        }
    }

    void insertAtBeg(Node *pnode) {
        pnode->next = first;
        first = pnode;
    }

    void traverseList() {
        Node *current = first;
        while(current) {
            cout << current->data <<"  ";
            current = current->next;
        }
    }

    void partionMagic(int x) {

        ListNode small_list;
        ListNode greater_list;
        Node* current = first;
        while(current != NULL) {
            //traversing original list
            if(current->data >= x) {
                //push in greater_list if it greater or equal to x
                if(current->data == x) {
                    greater_list.insertAtBeg(current);
                } else {
                    greater_list.insertAtEnd(current);
                }
            } else {
                //push in small_list if is small than x
                small_list.insertAtEnd(current);
            }
            current = current->next;
        }
        printf("small_list:\n");
        greater_list.traverseList();
    }
};




/**
3->5->8->5->10->2->1-> x = 5;
^
c

O/P: 3->2->1->5->5->8->10

small_list = 0->3
                ^
                s

if(c->data >= x) {
    move in big_list
}
big_list = 0->5->5->
           ^
           g
**/
int main() {

    ListNode list1;
    //Node* node = list1.createNode();
    for(int i=0; i<7; ++i)  {
        list1.insertAtEnd(list1.createNode());
        //list1.insertAtBeg(list1.createNode());
    }
    list1.traverseList();
    printf("\n");
    // ListNode res = list1.partionMagic(5);
    // res.traverseList();
    list1.partionMagic(5);
}