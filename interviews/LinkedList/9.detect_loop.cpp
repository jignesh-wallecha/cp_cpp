#include<iostream>
using namespace std;

struct ListNode {
	int data;
    ListNode *next;
public:
    ListNode *first;
    ListNode() : data(0), next(nullptr), first(nullptr) {}
    
    ListNode* createNode() {
    	ListNode *pnode;
    	pnode = (ListNode*) malloc(sizeof(ListNode));
    	if(pnode == NULL) {
    		return NULL;
    	} else {
    		cout << "Enter data:\n";
    		cin >> pnode->data;
    	}
    	pnode->next = NULL;
    	return pnode;
    }
    
    void insertAtEnd() {
    	ListNode *pnode, *current;
    	pnode = createNode();
    	if(first == NULL) {
    		first = pnode;
    	} else {
    		current = first;
    		while(current->next) {
    			current = current->next;
    		}
    		current->next = pnode;
    	}
    }
    
    void traverse_list() {
        ListNode *current;
        current = first;
        while(current) {
		    cout << current->data;
		    current = current->next;
	    }
    }  
    
    void insertNode(int n) {
    	for(int i=0; i<n; ++i) {
    		insertAtEnd();
    	}
    }

    bool detectLoop(ListNode *head) {
    	ListNode *slow, *fast;
    	slow = fast = head;
    	
    }