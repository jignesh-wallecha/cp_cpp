#include <iostream>
using namespace std;

struct Node {
    int data;
    struct Node *next;
};

typedef struct Node ListNode;
ListNode *head = nullptr;

ListNode* createNode() {
    ListNode *pnode = (ListNode*) malloc(sizeof(ListNode));
    if(!pnode) return pnode;
    cout << "Enter Data:\n";
    cin >> pnode->data;
    pnode->next = NULL;
    return pnode;
}

void insertAtEnd() {
    ListNode *pnode, *current;
    pnode = createNode();
    if(head == NULL) {
        head = pnode;
    } else {
        current = head;
        while(current->next) {
            current = current->next;
        }
        current->next = pnode;
    }
}

void insertNode(int n) {
    for(int i=0; i<n; ++i) {
        insertAtEnd();
    }
}

struct ListNode* removeNthFromEnd(int n){
    if (head->next and n == 1) {
        head = null;
    }
    ListNode *f, *s;
    f = s = head;
    for (int i = 0; i < n; ++i) {
        f = f->next;
    }
    while (f->next) {
        f = f->next;
        s = s->next;
    }
    s->next = s->next->next;
    return head;
}

int main() 
{
    ListNode *head = nullptr;
    int n;
    cout << "Enter the num:\n";
    cin >> n;
    insertNode(n);
    //cout << "first: "<< list1.first->data << endl;
    removeNthFromEnd(2);
    return 0;
}