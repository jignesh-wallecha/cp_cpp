#include<iostream>
#include<forward_list>
#include<stack>
#include<cstdlib>
#include<vector>
#include<algorithm>
#include<iterator>

using namespace std;

/**
* Question: Palindrome Linked List:
* Given a singly linked list, determine if it is palindrome.

Ex1: Input: 1->2			Ex2: Input: 1->2->2->1
	 Output: false				 Output: true
	
LOGIC:

Intutive Approach:

We'll reverse a string i.e duplicate of original string and then compare original string with duplicate string.

Approach 1: Using Stack

And use two pointers one would be slow would be incremented by one and other would be fast which we'll be incremented by twice. 
's'= slow & 'f' = fast.

So jabh tak 'f' pointer end tak reach nhi hota hai list, tabh tak slow ke data ko stack mai push karu. 

1. forward_list = 0 -> 1 -> 2 -> 2 -> 1 -> 0 
			  ^ ^
			  s f
2. forward_list = 0 -> 1 -> 2 -> 2 -> 1 -> 0 
			  		   ^ 	^
			  		   s 	f
3. forward_list = 0 -> 1 -> 2 -> 2 -> 1 -> 0 
			  		   		^ 		  ^
			  		   		s 	      f
4. forward_list = 0 -> 1 -> 2 -> 2 -> 1 -> 0 
			  		   			 ^ 		  	   	^
			  		   			 s 	      		f

'f' pointer has past the end of list. So 's' ptr's element would not be pushed in stack. But 's' ptr will be pointing to 2 element of list.

So now 's' ptr jabh tak end tak nhi reach hota hai list tabh tak we'll compare with stack's top element if it matches then we'll pop the element from stack.

5. forward_list = 0 -> 1 -> 2 -> 2 -> 1 -> 0 
			  		   			 ^ 		  	   	
			  		   			 s 	      
	
	so s->data i.e (2) compareto(stack.top) if s->data != s->top() return false otherwise stk.pop().

 
Stack:		Stack:1.(Poping ele:(Popped 2))	
|	   |	|	   |						
|	   |	|	   |						
|  2   |	|	   |
|  1   |	|  1   |
|  0   |	|  0   |
|------|	|------|

And so on tak 's' pointer to end of list match s->data with stack's top element.
So this Approach is for Even List.

Odd Length List:
So from above logic of even length:

list = 0 -> 1 -> 2 -> 1 -> 0
	  			 ^ 	 	   ^
	 	    	 s 	 	   f 
Now 's' is pointing 2 element and 'f' is pointing to 0 element in list
stack contains: 0 1 

But we'll not push '2' ele of list in stack because 'f' ptr is at last node not past the list. 

In above case of even length of list fast ptr use to visit second last node of list then past the last i.e (end :end of list, Hence end + 1). But in odd length list it is visiting last/end node of list. So agar fast ptr last pe rahega n toh slow ptr wale element ko skip krr dehna aur slow ptr increment krr dehne ka.
*/

struct ListNode {
	int data;
    ListNode *next;
public:
    ListNode *first;
    ListNode() : data(0), next(nullptr), first(nullptr) {}
    
    ListNode* createNode() {
    	ListNode *pnode;
    	pnode = (ListNode*) malloc(sizeof(ListNode));
    	if(pnode == NULL) {
    		return NULL;
    	} else {
    		cout << "Enter data:\n";
    		cin >> pnode->data;
    	}
    	pnode->next = NULL;
    	return pnode;
    }
    
    void insertAtEnd() {
    	ListNode *pnode, *current;
    	pnode = createNode();
    	if(first == NULL) {
    		first = pnode;
    	} else {
    		current = first;
    		while(current->next) {
    			current = current->next;
    		}
    		current->next = pnode;
    	}
    }
    
    void traverse_list() {
        ListNode *current;
        current = first;
        while(current) {
		    cout << current->data;
		    current = current->next;
	    }
    }  
    
    void insertNode(int n) {
    	for(int i=0; i<n; ++i) {
    		insertAtEnd();
    	}
    }
    
    /**
	* Approach 1: Using Vector 
	* Time : O(n) & Space : O(n)
    */
    bool isPalindrome(ListNode* first) {
        vector<int> v;
        while(first) {
            v.emplace_back(first->data);
            first = first->next;
        }
        for(int i=0; i<v.size()/2; ++i) {
            if(v[i] != v[v.size()-1-i]) {
                return false;
            }
        }
        return true;
    }

    /**
	* Approach 2: Using Stack
	* Time: O(n) & Space: O(n) 
    */
    bool isEven() {
		stack<int> stk;
		ListNode *slow, *fast;
		slow = fast = first;
		while(fast) {
			stk.push(slow->data);
			fast = fast->next->next;
			slow = slow->next;
		}
		//cout <<"current slow ptr: "<< slow->data<< endl;
		while(slow) {
			if(slow->data != stk.top()) {
				return false;
			}
			stk.pop();
			slow = slow->next;
		}
		if(!stk.empty()) {
			return false;
		}
		return true;
	}

	bool isOdd() {
		stack<int> stk;
		ListNode *slow, *fast;
		slow = fast = first;
		while(fast->next) {
			stk.push(slow->data);
			slow = slow->next;
			fast = fast->next->next;
		}
		while(slow->next) {
			if (slow->next->data != stk.top()) return false;
			else stk.pop();
			slow = slow->next;
		}	
		if(!stk.empty()) return false;
		return true;
	}

    bool isPalindrome() {
    	return isEven() || isOdd();
    }

    /**
	* Approach 3: Two pointer with O(1) space complexity.
    *

    bool isPalindrome(ListNode* first) {
    	ListNode *slow, *fast, *tmp;
    	slow = fast = first;

    	//find middle (slow)
    	while(fast || fast->next) {
    		fast = fast->next->next;
    		slow = slow->next;
    	}

    	//reverse the second half
    	ListNode *prev(nullptr);
    	while(slow) {
    		tmp = slow->next;
    		slow->next = prev;
    		prev = slow;
    		slow = tmp;
    	}

    	//check palindrome
    	ListNode *left, *right;
    	left = first, right = prev;
    	while(left <= right) {
    		if(left->data != right->data) {
    			return false;
    		}
    		left = left->next;
    		right = right->next;
    	}
    	return true;
    }
    */
}; 

/**
* Doing Program Using STL FRAMEWORK:
*
*/

bool isPalindrome(forward_list<int>& l) {
	vector<int> v;
	std::copy(l.begin(), l.end(), back_inserter(v));
	for(int i=0; i<v.size()/2; ++i) {
		if(v[i] != v[v.size()-1-i]) {
            return false;
        }
	}	
	return true;
}


int main() {
    
  cout << std::boolalpha << endl;
/*   ListNode list1;
   int n;
   cout << "Enter the num:\n";
   cin >> n;
   list1.insertNode(n);
   //cout << "first: "<< list1.first->data << endl;
   bool res = list1.isPalindrome(list1.first);
   cout << res <<endl;
   res = list1.isPalindrome();
   cout << res << endl;
*/

  forward_list<int> l{0, 1, 2, 2, 0, 1};
  bool res = isPalindrome(l);
  cout << res << endl;

  
 // cout << *next(l.begin()) << endl;

  auto f = l.begin();

  *next(l.begin()) = NULL;

  advance(f, 1);

  cout << *f << endl;
  return 0;
}
