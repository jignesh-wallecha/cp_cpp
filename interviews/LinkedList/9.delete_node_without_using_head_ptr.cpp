#include<iostream>
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

/**
* Delete Node in a Linked List:
Write a function to delete a node in a singly-linked list. You will not be given access to the head of the list, instead you will be given access to the node to be deleted directly.

It is guaranteed that the node to be deleted is not a tail node in the list.
Example 1:
Input: head = [4,5,1,9], node = 5
Output: [4,1,9]
Explanation: You are given the second node with value 5, the linked list should become 4 -> 1 -> 9 after calling your function.


Logic:
Approach: Swap with Next Node 

The usual way of deleting a node from singly linked list, is to modify the next pointer of deleting node with previous node, to point prev node to the deleting node after/next

But, we do not have access to the head of the list, so we'll intially not deleting node but like swapping:

1.

node = 5
4 -> 5 -> 1 -> 9    

node = 5, so we'll replace the value of node (deleting node) with the value in the node after it, and then delete the node after it.

2.

4 -> 1 -> 1 -> 9    

Now point node's next pointer to node->next->next, So this way we'll delete it indirectly.

3.
	 --------
	 ↑ 		  ↘︎
4 -> 1   1    9
	 ^
	node(val of next node)
4.

4 -> 1 -> 9

Because we know that the node we want to delete is not the tail of the list, we can guarantee that this approach is possible.

**/
void deleteNode(ListNode* node) {
    if(!node) return;
        
    node->val = node->next->val;
    node->next = node->next->next;
}