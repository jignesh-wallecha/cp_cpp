#include<iostream>
using namespace std;

struct Node {
	int data;
	struct Node *next;
};

struct Block {
	struct Node *head;
	struct Node *tail;
    struct Block *next;	
    int maxCapacity;
    int numOfNodes;
};

typedef struct Node ListNode;
typedef struct Block BlockList;
BlockList* head_block = NULL;
BlockList* tail_block = NULL;

ListNode* createNode() {
	ListNode *pnode = (ListNode*) malloc(sizeof(ListNode));
	if(pnode == NULL) {
		return pnode;
	}
	cout << "Enter data:\n";
	cin >> pnode->data;
	pnode->next = NULL;
	return pnode;
}

Block* createBlock(int maxElements) {
	BlockList *pBlock = (BlockList*) malloc(sizeof(BlockList));
	if(pBlock == NULL) {
		return pBlock;
	}
	pBlock->head  = pBlock->tail = NULL;
	pBlock->maxCapacity = maxElements;
	pBlock->numOfNodes = 0;
	pBlock->next = NULL;
	return pBlock;
}

bool isEmptyBlock(BlockList* block) {
	return block->numOfNodes == 0 ? true : false;  
}

int countNumOfNodes(BlockList *block) {
	ListNode *current = block->head;
	int counter = 0;
	while(current) {
		counter++;
		current = current->next;
	}
	return counter;
}

void traverse_Block_Nodes(Block* block) {
	ListNode* current = block->head;
	while(current) {
		cout << current->data <<"  "; 
		current = current->next;
 	}
 	cout << '\n';
}

void insertAtBlockBeg(BlockList *block) {
	//create a new node
	ListNode* pnode = createNode();

	//block is empty
	if(isEmptyBlock(block)) {
		block->head = pnode;
		block->tail = pnode;

	} else if(block->numOfNodes == block->maxCapacity) {
		BlockList *newBlock = createBlock(4);
		block->next = newBlock;
		ListNode *prev = NULL;
		ListNode *current = block->head;
		while(current->next) {
			prev = current;
			current = current->next;
		}
		if(isEmptyBlock(newBlock)) {
			newBlock->head = current;
			newBlock->tail = current;
		} else {
			current->next = newBlock->head;
			newBlock->head = current;
			newBlock->tail = current->next;
		}
		newBlock->numOfNodes++;
		free(block->tail);
		block->tail = prev;
		//cout << "block->tail : " << block->tail->data << endl;
		//now insert pnode
		pnode->next = block->head;
		block->head = pnode;
		printf("new block:  ");
		traverse_Block_Nodes(newBlock);
	} else {
		//nor a block is empty and nor a block is full
		pnode->next = block->head;
		block->head = pnode;
	}	
	block->numOfNodes++;
}



/**
void insertAtBlockAfter(BlockList *block) {
	int x;
	ListNode* pnode = createNode();
	cout << "Enter element after you want to insert:\n";
	cin >> x;
	ListNode* current = block->head;
	if(block->numOfNodes == block->maxCapacity) {
		//create new block and shift last node that block;
		BlockList *newBlock = createBlock(4);
		ListNode* c = block->head;
		while(c->next) {
			c = c->next;
		}
		newBlock->head = c;
		newBlock->numOfNodes++;
		c = NULL;
		// cout << "newBlock:\n";
		// traverse_Block_Nodes(newBlock);
	}
	while(current && current->data != x) {
		current = current->next;
	}
	if(current == NULL) {
		cout << x << "not found in list might be in another block!\n";
		return;
	}
	//current is not null and x is their in list so insert it.
	if(block->numOfNodes != block->maxCapacity) {
		block->numOfNodes++;
		pnode->next = current->next;
		current->next = pnode;
	}
}
**/
/**
* this function is for inserting a node from end/back at specified node 
* if this block has reached its maxCapacity then create new block and insert
* this node at that block
**/

void insertAtBlockEnd(BlockList *block) {	
	ListNode* pnode = createNode();

	if(isEmptyBlock(block)) {
		//this block is empty so insert it directly 
		block->head = pnode;
		block->tail = pnode;
		block->numOfNodes++;

	} else if(block->numOfNodes == block->maxCapacity) {
		//as this block has maxCapacity nodes so can't insert, instead
		//create new block and insert into new block
		BlockList *newBlock = createBlock(4);
		block->next = newBlock;
			if(isEmptyBlock(newBlock)) {
				newBlock->head = pnode;
				newBlock->tail = pnode;
				newBlock->numOfNodes++;
			} else {
				cout << "Not empty newBlock:\n";
				ListNode *c = newBlock->head;
				while(c->next) {
					c = c->next;
				}
				c->next = pnode;
				newBlock->tail = c->next;
				newBlock->numOfNodes++;
			}
		
			if(head_block == NULL) {
				head_block = block;
				tail_block = block;
			} else {
				head_block = block;
				tail_block = newBlock;
			}
		cout << "head_block head: " << head_block->head->data << endl;
		//cout << "head_block tail: " << head_block->tail->data << endl;
		cout << "tail_block head: " << tail_block->head->data << endl;
		cout << "tail_block tail: " << tail_block->tail->data << endl;
		//cout << "newBlock numOfNodes: " << newBlock->numOfNodes << endl;
		cout << "tail_block numOfNodes: " << tail_block->numOfNodes << endl;
		printf("traverse block nodes2:\n");
		traverse_Block_Nodes(newBlock);
	} else {
		//this block is not empty neither has reached its capacity
		ListNode* current = block->head;
		while(current->next) {
			current = current->next;
		}
		current->next = pnode;
		block->numOfNodes++;
		block->tail = current->next;
		block->tail = pnode;
	}
}

void deleteSpecificNodeFrom(BlockList* block) {
	ListNode *current, *follow;
	int x;
	cout << "Enter the element you want to delete from this block:\n";
	cin >> x;
	current = block->head;
	follow = NULL;
	while(current && current->data != x) {
		follow = current;
		current = current->next;
	}
	follow->next = current->next;
	free(current);
}



void clearAllNodesFrom(BlockList *block) {
	while(block->head) {
		ListNode *tempNode = block->head->next;
		free(block->head);
		block->head = tempNode;
	}
	block->tail = NULL;
	block->numOfNodes = 0;
}


/************************* OPERATIONS ON/OF BLOCK *******************/

void insertAtEnd() {
	BlockList *block = createBlock(4);
	for(int i=0; i<4; ++i) {
		insertAtBlockEnd(block);
	}
	if(head_block == NULL) {
		head_block = block;
	} else {
		  BlockList *currentBlock;
		  currentBlock = head_block;
		  while(currentBlock->next) {
		  		currentBlock = currentBlock->next;
		  }
		  currentBlock->next = block;
		  currentBlock = block;
	}
}

/*
void insertAtBeg() {
	if(isEmpty()) {
		//create new block and insert/push nodes in it.
		BlockList* block = createBlock(4);
		insertAtBlockBeg(block);
		head_block = block;
		tail_block = block;
	} else {
		block->next = head_block;
		head_block = block;
	}
}
*/

void traverse_Block() {
	BlockList* currentBlock = head_block;
	while(currentBlock) {
		//print the nodes data for in this block
		traverse_Block_Nodes(currentBlock);
		currentBlock = currentBlock->next;
	}
}

void deleteFromBeg() {
	BlockList *currentBlock = head_block;
	head_block = head_block->next;
	free(head_block);
}


int countNumOfBlocks() {
	BlockList* currentBlock = head_block;
	int counter = 0;
	while(currentBlock) {
		counter++;
		currentBlock = currentBlock->next;
	}
	return counter;
}


int main() {

	Block* block1 = createBlock(4);
	for(int i=0; i<6; ++i) 
		insertAtBlockBeg(block1);

	traverse_Block_Nodes(block1);

	int res = countNumOfNodes(block1);
	cout << "The count of nodes in this block is : " << res << endl;
	// int res = countNumOfBlocks();
	// printf("countNumOfBlocks:%d\n", res);
	// cout <<" block1 head: " << block1->head->data <<endl;
	// cout <<" block2 tail: " << block1->tail->data <<endl;

	// insertAtBlockAfter(block1);

	// traverse_Block_Nodes(block1);	


	// printf("\nNumOfNodes:\n");
	// cout << block1->numOfNodes << endl;

	// for(int i=0; i<3; ++i) {
	// 	insertAtEnd();
	// }
	// traverse_Block();
	// int res = countNumOfBlocks();
	// printf("countNumOfBlocks:%d\n", res);
	// cout << "The Count of total blocks is : " << countNumOfBlocks() << endl;

	//clearAllNodesFrom(block1);

// 	cout << "num of nodes in block1 is : " << block1->numOfNodes << endl;
// 	cout << "max capacity in block2 is : " << block1->maxCapacity << endl;
// 	cout << "head and tail in block1: " << block1->head <<"  " << block1->tail << endl;
	 
	// deleteSpecificNodeFrom(block1);

	// traverse_Block_Nodes(block1);
}


 