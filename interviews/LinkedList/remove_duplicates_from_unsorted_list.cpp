#include<iostream>
using namespace std;

/**
* Remove Duplicates: Write a code to remove duplicates from an unsorted array.

* Brute Force Approach:

	1 -> 2 -> 3 -> 1 -> 4 -> 5
^	^
p	c 
	
Keep a pointer c on first node and check that 'c' ptr node value from 
c->next. If it is their then link p->next with c->next->next.


**/
struct Node {
	int data;
	struct Node *next;
};

typedef struct Node ListNode;

ListNode *first = NULL;

ListNode* createNode() {
    ListNode *pnode;
    pnode = (ListNode*) malloc(sizeof(ListNode));
    if(pnode == NULL) {
    	return pnode;
   	} else {
    	cout << "Enter data:\n";
    	cin >> pnode->data;
    }
    pnode->next = NULL;
    return pnode;
}
    
void insertAtEnd() {
  	ListNode *pnode, *current;
    pnode = createNode();
    if(first == NULL) {
    	first = pnode;
    } else {
    	current = first;
    	while(current->next) {
    		current = current->next;
    	}
    	current->next = pnode;
    }
}
    
void traverse_list() {
    ListNode *current;
    current = first;
    while(current) {
	    cout << current->data <<"  ";
	    current = current->next;
	}
}  
    
void insertNode(int n) {
	for(int i=0; i<n; ++i) {
    	insertAtEnd();
    }
}

void removeDuplicates() {

	ListNode *current, *ahead;
	current = first;
	while(current) {
		ahead = current->next;
		while(ahead) {
			if(current->data == ahead->data) {
				current->next = ahead->next;
				free(ahead);
			}
			ahead = ahead->next;
		}
		current = current->next;
	}
}
int main() {

	int n;
	cout << "Enter num:\n";
	cin >> n;
	insertNode(n);
	traverse_list();
	printf("\n");

	removeDuplicates();
	traverse_list();
	printf("\n");
}