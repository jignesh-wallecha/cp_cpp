#include <iostream>
using namespace std;

struct ListNode {
	int data;
    ListNode *next;
public:
    ListNode *first;
    ListNode() : data(0), next(nullptr), first(nullptr) {}
    
    ListNode* createNode() {
    	ListNode *pnode;
    	pnode = (ListNode*) malloc(sizeof(ListNode));
    	if(pnode == NULL) {
    		return NULL;
    	} else {
    		cout << "Enter data:\n";
    		cin >> pnode->data;
    	}
    	pnode->next = NULL;
    	return pnode;
    }
    
    void insertAtEnd() {
    	ListNode *pnode, *current;
    	pnode = createNode();
    	if(first == NULL) {
    		first = pnode;
    	} else {
    		current = first;
    		while(current->next) {
    			current = current->next;
    		}
    		current->next = pnode;
    	}
    }
    
    void traverse_list() {
        ListNode *current;
        current = first;
        while(current) {
		    cout << current->data;
		    current = current->next;
	    }
    }  
    
    void insertNode(int n) {
    	for(int i=0; i<n; ++i) {
    		insertAtEnd();
    	}
    }

    ListNode* removeKthLastNode(int k) {
        ListNode *slow, *fast;
        fast = slow = first;
        for (int i = 0; i < k; ++i) {
            fast = fast->next;
        }
        while (fast->next) {
            fast = fast->next;
            slow = slow->next;
        }
        slow->next = slow->next->next;
        return first;
    }

    void traverse(ListNode* t) {
        if (!t) {
            cout << "[]";
        }
        while (t) {
            cout << t->data << endl;
            t = t->next;
        }
    }
};

int main() {

   ListNode list1;
   int n;
   cout << "Enter the num:\n";
   cin >> n;
   list1.insertNode(n);
   
   list1.traverse(list1.removeKthLastNode(1));


   return 0;
}
